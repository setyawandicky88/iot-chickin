//Include Libraries
#include <DHT.h>
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

// GANTI 3 VARIABEL INI
#define WSN_ID "th1"
#define SENSOR_COUNT 1 //tergantung jumlah sensor, sementara 1
#define NRF24_CHANNEL 124

// ======== WSN Declaration ======== //
RF24 radio(9, 10);  // CE, CSN; Set sendiri, terserah

struct sensor{
  char id[4];
  float t[3]; 
  float h[3]; 
  byte tcount;
}wsn;
//address through which two modules communicate.
const byte address = 0xF0F0F0F0AA;
// -------- end WSN Declaration -------- //

// ======== Dht Variable ======== //
#define DHTPIN 7
#define DHTTYPE DHT22 // DHT 11  (AM2302), AM2321
DHT dht(DHTPIN, DHTTYPE);
// -------- end Dht variable -------- //

void setup()
{
  strcpy (wsn.id, WSN_ID);
  Serial.begin(115200);
  dht.begin();
  radio.begin();
  radio.openWritingPipe(address);
  radio.setChannel(NRF24_CHANNEL);
  radio.setPALevel(RF24_PA_MAX);
  radio.stopListening(); //Mode Transmitter
  wsn.tcount = SENSOR_COUNT ;
}
void loop()
{
  // ======== Sensor Reading Section ======== //
  dht_reading();

  
    Serial.print("Chip Availability: ");
    Serial.println(radio.isChipConnected());
  
  // ======== Transmission Section ======== //
  if(radio.isChipConnected()){
    radio.write(&wsn, sizeof(wsn));
  }
  delay(10);
}

void dht_reading(){
  if(dht.read()) {
    float temp, hum, tempAverage = 0, humAverage = 0;
    for (uint8_t i = 0; i < 2 ; i++) { //sampling 2x
        temp = dht.readTemperature();
        hum = dht.readHumidity();
        if(temp && temp > 70 && temp < 0)
          tempAverage += temp;
        else  
          i--;
        
        if(hum && hum > 70 && hum < 0)
          humAverage += hum;
        else  
          i--;
        delay(1500);
    }
      wsn.t[0] = temp; 
      wsn.h[0] = hum;    
        Serial.print("DHT Read : ");
        Serial.print(wsn.t[0]);
        Serial.print(", ");
        Serial.print(dht.readHumidity());
        Serial.print("\n");
      
    }
  }
