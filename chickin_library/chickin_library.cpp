String intermittentChickin(int deviceOn, int deviceOff, String status)
{   
    unsigned long millisTime;
    unsigned long previousMillis;
    unsigned long intermittentOff;
    unsigned long intermittentOn;

    millisTime = millis();
    intermittentOff = deviceOff * 6000;
    intermittentOn = deviceOn * 6000;
    if (status == "off")
    {
        if (millisTime - previousMillis >= intermittentOff)
        {
            status = "on";
            // UpdateDevice(status, flockId, topicFan1Pub);
            previousMillis = millis();
        }
    }
    else if (status == "on")
    {
        if (millisTime - previousMillis >= intermittentOn)
        {
            status = "off";
            // UpdateDevice(status, flockId, topicFan1Pub);
            previousMillis = millis();
        }
    }
    return status;
}