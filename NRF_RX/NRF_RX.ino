//Include Libraries
#include <DHT.h>
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <Wire.h> 

 //GANTI VARIABEL2 ini
#define NRF24_CHANNEL 124

 //END GANTI VARIABEL2 ini

String idSensor;
char idSensorChar[4];

float rangeTemp = NULL, suhuAktual = NULL;
//debugging masalah nrf tidak masuk di mcc
#define DHTPIN 4
#define DHTTYPE DHT22 // DHT 11  (AM2302), AM2321
DHT dht(DHTPIN, DHTTYPE);


//create an RF24 object
RF24 radio(5, 2);  // CE, CSN

struct sensor{ //RX data format
  char id[4];
  float t[3]; 
  float h[3]; 
  byte tcount;
  }wsn;
  
//address through which two modules communicate.
const uint8_t address = 0xF0F0F0F0AA;

unsigned long millisNRF = 0;
float suhuSensor;

void setup()
{
  while (!Serial);
  Serial.begin(115200);
  dht.begin();

  radio.begin();

  //set the address
  radio.openReadingPipe(0, address);
  radio.setChannel(NRF24_CHANNEL);
  radio.setPALevel(RF24_PA_MAX);
  //Set module as receiver
  radio.startListening();

  millisNRF = millis();
}

void loop()
{
  if (Serial.available() > 0)
  {
    String suhuString = Serial.readString();
    int str_len = suhuString.length() + 1; 
    suhuString.toCharArray(idSensorChar, str_len);
    idSensor = suhuString;
  }
  //Read the data if available in buffer
  bool chip = radio.isChipConnected();
  bool nrf_available = radio.available();
  Serial.print("Chip Detect:");
  Serial.print(chip);
  Serial.print(", Packet Availability:");
  Serial.println(nrf_available);
  
  if(dht.read()) {
    float compareSuhu, t, h, f;
    for (uint8_t i = 0; i < 2 ; i++) { //dicoba 5x
      compareSuhu = dht.readTemperature();
      h = dht.readHumidity();
      f = dht.readTemperature(true);
      if (compareSuhu < 70 && compareSuhu > 0) {
        if (t - compareSuhu <= 10 ) { //kalau sebelum 5x pembacaan suhu sekarang dan sebelumnya tidak jauh beda, maka keluar fungsi
          t = compareSuhu;
          break;
        }
      }
      t = dht.readTemperature();
      Serial.print("DHT Read : ");
      Serial.print(compareSuhu);
      Serial.print(",");
      Serial.println(t);
      vTaskDelay(2000 / portTICK_PERIOD_MS);
      if (t < 0) t = 0;
    }
    if (!isnan(h) || !isnan(t) || !isnan(f))
    {
      suhuSensor = t;
    }
  }
  //memang sengaja ndak dikasih else if
  if (chip && nrf_available)
  {
    Serial.print("Incoming data from - ");
    Serial.println(wsn.id);
    Serial.print("Suhu Rata2 : ");
    millisNRF = millis();
    radio.read(&wsn, sizeof(wsn));
    float loop_case = 0; 
    for (uint8_t i = 0 ; i< wsn.tcount + 1; i++){
      loop_case += wsn.t[i]; 
      Serial.print("t=");
      Serial.print(i);
      Serial.print(wsn.t[i]);
      Serial.print("& h");
      Serial.print(i);
      Serial.print("=");
      Serial.print(wsn.h[i]);
      Serial.println(",");
      if (i == wsn.tcount) 
        suhuSensor = loop_case/wsn.tcount;  
    }
  delay(200);
  
  Serial.print(", Sensor Count : ");
  Serial.print(wsn.tcount);
  Serial.println(".");

  
  millisNRF = millis();
  }
  else if (millis() - millisNRF > 60000){
    Serial.println("-Wireless Sensor Timeout-");
    suhuSensor = NULL;
  }
  else if (suhuSensor == 0 || suhuSensor < 0){
    Serial.println("DHT22 error");
    //t = 0, h = 0, f = 0;
    Serial.println(F("Failed to read from DHT sensor!"));
  }
  delay(3000);
}
