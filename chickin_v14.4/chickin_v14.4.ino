#include <SimpleTimer.h>

const int ledPin =  13;      // the number of the LED pin

SimpleTimer timer;

// Variables will change:
int ledState = LOW;
int ledState2 = LOW;

long previousMillis = 0;
long previousMillis2 = 0;
long previousMillis3 = 0;

long interval = 10000;
long intervalInventer = 5000;
long interval2 = 5000;
long interval3 = 5000;

float suhu = 25;
float suhuAktual;
String blower1 = "off";
String blower2 = "off";
String blower3 = "off";
String pemanas = "off";
String pendingin = "off";
String intermittent = "";
String statusSuhu = ""; //ideal / tidak_ideal
String mode;
int inventer = 50;
float batasBawah;
float batasAtas;
int hari;
bool statusInventer = false;

void setup() {
  // set the digital pin as output:
  Serial.begin(115200);
  pinMode(ledPin, OUTPUT);

  //inventer
  timer.setTimeout(intervalInventer, hari0InventerPlus);
  timer.setTimeout(intervalInventer, hari0InventerMin);
}

void loop()
{
  timer.run();

  batasBawah = suhu - 2;
  batasAtas = suhu + 2;

  //testing
  if(Serial.available() > 0) {
    String suhuString = Serial.readString();
    suhuAktual = suhuString.toFloat();
  }
  mode = "inventer";
  hari = 12;
  if(mode == "mcc") {
    if(suhuAktual < batasBawah || suhuAktual > batasAtas) {
      statusSuhu = "tidak_ideal";  
    }
    if (hari == 0) {
      unsigned long currentMillis = millis();
      float batasBawah = suhu - 2;
      float batasAtas = suhu + 2;
      blower1 = "on";
      if (currentMillis - previousMillis > interval) {
        previousMillis = currentMillis;
        if (suhuAktual > suhu) {
          if (blower1 == "on") {
            pendingin = "on";
          } else {
            blower1 = "on";
          }
        }
        Serial.println("Blower : " + blower1);
        Serial.println("Pendingin : " + pendingin);
      }
    } else if(hari == 1) {
      unsigned long currentMillis = millis();
      float batasBawah = suhu - 2;
      float batasAtas = suhu + 2;
      blower1 = "on";
      if (currentMillis - previousMillis > interval) {
        previousMillis = currentMillis;
        if (suhuAktual > suhu) {
          if (blower1 == "on") {
            pendingin = "on";
          } else {
            blower1 = "on";
          }
        }
        Serial.println("Blower : " + blower1);
        Serial.println("Pendingin : " + pendingin);
      }
    } else if(hari == 2) {
      unsigned long currentMillis = millis();
      float batasBawah = suhu - 2;
      float batasAtas = suhu + 2;
      blower1 = "on";
      if (currentMillis - previousMillis > interval) {
        previousMillis = currentMillis;
        if (suhuAktual > suhu) {
          if (blower1 == "on") {
            pendingin = "on";
          } else {
            blower1 = "on";
          }
        }
        Serial.println("Blower : " + blower1);
        Serial.println("Pendingin : " + pendingin);
      }
    } else if(hari == 3) {
      unsigned long currentMillis = millis();
      float batasBawah = suhu - 2;
      float batasAtas = suhu + 2;
      blower1 = "on";
      if (currentMillis - previousMillis > interval) {
        previousMillis = currentMillis;
        if (suhuAktual > suhu) {
          if (blower1 == "on") {
            pendingin = "on";
          } else {
            blower1 = "on";
          }
        }
        Serial.println("Blower : " + blower1);
        Serial.println("Pendingin : " + pendingin);
      }
    } else if(hari == 4) {
      unsigned long currentMillis = millis();
      float batasBawah = suhu - 2;
      float batasAtas = suhu + 2;
      blower1 = "on";
      if (currentMillis - previousMillis > interval) {
        previousMillis = currentMillis;
        if (suhuAktual > suhu) {
          if (blower1 == "on") {
            pendingin = "on";
          } else {
            blower1 = "on";
          }
        }
        Serial.println("Blower : " + blower1);
        Serial.println("Pendingin : " + pendingin);
      }
    } else if(hari == 5) {
      unsigned long currentMillis = millis();
      unsigned long currentMillis2 = millis();
      unsigned long currentMillis3 = millis();
      float batasBawah = suhu - 2;
      float batasAtas = suhu + 2;
      if (currentMillis - previousMillis > interval) {
        previousMillis = currentMillis;
        if (suhuAktual > suhu) { //pendingin
        blower1 = "on";
          if (blower1 == "on") {
            // interval += interval2;
            if(currentMillis2 - previousMillis2 > interval2) {
              previousMillis2 = currentMillis2;
              if(suhuAktual > suhu && blower1 == "on") {
                // interval += 5000;
                blower2 = "on";

              //interval ketiga
              if(currentMillis3 - previousMillis3 > interval3) {
                previousMillis3 - currentMillis3;
                if(suhuAktual > suhu && blower1 == "on" && blower2 == "on") {
                  pendingin = "on";
                }
              }

              } else {
                // interval = 5000;
                Serial.println("intermittent");
              }
            }

          }
        } else {// nanti ditambah pemanas
          blower1 = "off";
        }
      }
    } else if(hari == 6) {
      if(suhuAktual > suhu) {
        blower1 = "on";
        delay(5000);
        if(suhuAktual > suhu) {
          if(blower1 = "on") {
            blower2 = "on";
            delay(5000);
            if(suhuAktual > suhu) {
              if(blower1 == "on" && blower2 == "on") {
                pendingin = "on";
                delay(5000);
              }
            }
          }
        }
      }
    } else if(hari == 7) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari7Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas || suhuAktual > batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari7Blower2);
          // blower1 = "off";
          // blower2 = "off";
          // pendingin = "off";
          // intermittent = "on";
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 8) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari8Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari8Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 9) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari9Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari9Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 10) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari10Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari10Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 11) {
      // if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          pemanas = "off";
          intermittent = "off";
          timer.setTimeout(interval, hari11Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
          blower1 = "off";
          blower2 = "off";
          blower3 = "off";
          pendingin = "off";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari11Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      // }
    } else if(hari == 12) {
      if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari12Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
          blower1 = "off";
          blower2 = "off";
          blower3 = "off";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari12Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
    } else if(hari == 13) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari13Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari13Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 14) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari14Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari14Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 15) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari15Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari15Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 16) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari16Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari16Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 17) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari17Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari17Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 18) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari18Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari18Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 19) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari19Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari19Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 20) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari20Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari20Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 21) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari21Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari21Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 22) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari22Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari22Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    }else if(hari == 23) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari23Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari23Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    }else if(hari == 24) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari24Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari24Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    }else if(hari == 25) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari25Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari25Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    }else if(hari == 26) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari26Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari26Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    }else if(hari == 27) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari27Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari27Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    }else if(hari == 28) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari28Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari28Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    }else if(hari == 29) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari29Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari29Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    }else if(hari == 30) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari30Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari30Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    }else if(hari == 31) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari31Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari31Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    }else if(hari == 32) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari32Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari32Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    }else if(hari == 33) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari33Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari33Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    }else if(hari == 34) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari34Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari34Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    }else if(hari == 35) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari35Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari35Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    }else if(hari == 36) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari36Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari36Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    }else if(hari == 37) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari37Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari37Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 38) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari38Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari38Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 39) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari39Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari39Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 40) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari40Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari40Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 41) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari41Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari41Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 42) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari42Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari42Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 43) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari43Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari43Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 44) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari44Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari44Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 45) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari45Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari45Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 46) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari46Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari46Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 47) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari47Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari47Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 48) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari48Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari48Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 49) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari49Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari49Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    } else if(hari == 50) {
      if(statusSuhu != "ideal") {
        if(suhuAktual > batasAtas) {
          blower1 = "on";
          intermittent = "off";
          timer.setTimeout(interval, hari50Blower2);
        } else if(suhuAktual < batasBawah) {
          pemanas = "on";
        } else if(suhuAktual == batasAtas) {
          pemanas = "off";
          blower1 = "on";
          timer.setTimeout(interval, hari50Blower2);
        } else if(suhuAktual == batasBawah) {
          blower1 = "off";
          blower2 = "off";
          pendingin = "off";
          intermittent = "on";
        }
      }
    }
    Intermittent();
    Serial.println("Blower1 : " + blower1);
    Serial.println("Blower2 : " + blower2);
    Serial.println("Blower3 : " + blower3);
    Serial.println("Pendingin : " + pendingin);
    Serial.println("Pemanas : " + pemanas);
    Serial.print("intermittent : ");
    Serial.println(intermittent);
    Serial.print("Suhu Ideal : ");
    Serial.print(suhu);
    Serial.print(" || Suhu batas bawah : ");
    Serial.print(batasBawah);
    Serial.print(" || Suhu batas atas : ");
    Serial.println(batasAtas);
    Serial.print("Suhu Aktual : ");
    Serial.println(suhuAktual);
    Serial.print("Hari ke : ");
    Serial.println(hari);
    Serial.println("==========================");
  } else if(mode == "inventer") {

    if(hari == 0) {
      if(suhuAktual > batasAtas && inventer == 100) {
        pemanas = "off";
        pendingin = "on";
      } else if(suhuAktual > batasAtas) {
        pemanas = "off";
        if(statusInventer) {
          statusInventer = false;
          timer.setTimeout(intervalInventer, hari0InventerPlus);
        }
      } else if(suhuAktual <= batasBawah) {
        pendingin = "off";
        pemanas = "on";
         if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari0InventerMin);
         }
      }
    } else if(hari == 1) {
      if(suhuAktual > batasAtas && inventer == 100) {
        pemanas = "off";
        pendingin = "on";
      } else if(suhuAktual > batasAtas) {
        pemanas = "off";
        if(statusInventer) {
          statusInventer = false;
          timer.setTimeout(intervalInventer, hari0InventerPlus);
        }
      } else if(suhuAktual <= batasBawah) {
        pendingin = "off";
        pemanas = "on";
         if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari0InventerMin);
         }
      }
    } else if(hari == 2) {
      if(suhuAktual > batasAtas && inventer == 100) {
        pemanas = "off";
        pendingin = "on";
      } else if(suhuAktual > batasAtas) {
        pemanas = "off";
        if(statusInventer) {
          statusInventer = false;
          timer.setTimeout(intervalInventer, hari0InventerPlus);
        }
      } else if(suhuAktual <= batasBawah) {
        pendingin = "off";
        pemanas = "on";
         if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari0InventerMin);
         }
      }
    } else if(hari == 3) {
      if(suhuAktual > batasAtas && inventer == 100) {
        pemanas = "off";
        pendingin = "on";
      } else if(suhuAktual > batasAtas) {
        pemanas = "off";
        if(statusInventer) {
          statusInventer = false;
          timer.setTimeout(intervalInventer, hari0InventerPlus);
        }
      } else if(suhuAktual <= batasBawah) {
        pendingin = "off";
        pemanas = "on";
         if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari0InventerMin);
         }
      }
    } else if(hari == 4) {
      if(suhuAktual > batasAtas && inventer == 100) {
        pemanas = "off";
        pendingin = "on";
      } else if(suhuAktual > batasAtas) {
        pemanas = "off";
        if(statusInventer) {
          statusInventer = false;
          timer.setTimeout(intervalInventer, hari0InventerPlus);
        }
      } else if(suhuAktual <= batasBawah) {
        pendingin = "off";
        pemanas = "on";
         if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari0InventerMin);
         }
      }
    } else if(hari == 5) {
      if(suhuAktual > batasAtas && blower1 == "on") {
        pemanas = "off"; //memastikan
        pendingin = "on";
      } else if(suhuAktual > batasAtas) {
        pemanas = "off";
        if(statusInventer) {
          statusInventer = false;
          timer.setTimeout(intervalInventer, hari5InventerPlus);
        }
      } else if(suhuAktual <= batasBawah) {
        pendingin = "off";
        blower1 = "off";
         if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari5InventerMin);
         }
      } else if(suhuAktual < batasBawah) {
        pemanas = "on";
      }
    } else if(hari == 6) {
      if(suhuAktual > batasAtas && blower1 == "on") {
        pemanas = "off"; //memastikan
        pendingin = "on";
      } else if(suhuAktual > batasAtas) {
        pemanas = "off";
        if(statusInventer) {
          statusInventer = false;
          timer.setTimeout(intervalInventer, hari6InventerPlus);
        }
      } else if(suhuAktual <= batasBawah) {
        pendingin = "off";
        blower1 = "off";
         if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari6InventerMin);
         }
      } else if(suhuAktual < batasBawah) {
        pemanas = "on";
      }
    } else if(hari == 7) {
      if(suhuAktual > batasAtas && blower1 == "on") {
        pemanas = "off"; //memastikan
        pendingin = "on";
      } else if(suhuAktual > batasAtas) {
        pemanas = "off";
        if(statusInventer) {
          statusInventer = false;
          timer.setTimeout(intervalInventer, hari7InventerPlus);
        }
      } else if(suhuAktual <= batasBawah) {
        pendingin = "off";
        blower1 = "off";
         if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari7InventerMin);
         }
      } else if(suhuAktual < batasBawah) {
        pemanas = "on";
      }
    } else if(hari == 8) {
      if(suhuAktual > batasAtas && blower1 == "on") {
        pemanas = "off"; //memastikan
        pendingin = "on";
      } else if(suhuAktual > batasAtas) {
        pemanas = "off";
        if(statusInventer) {
          statusInventer = false;
          timer.setTimeout(intervalInventer, hari8InventerPlus);
        }
      } else if(suhuAktual <= batasBawah) {
        pendingin = "off";
        blower1 = "off";
         if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari8InventerMin);
         }
      } else if(suhuAktual < batasBawah) {
        pemanas = "on";
      }
    } else if(hari == 9) {
      if(suhuAktual > batasAtas && blower1 == "on") {
        pemanas = "off"; //memastikan
        pendingin = "on";
      } else if(suhuAktual > batasAtas) {
        pemanas = "off";
        if(statusInventer) {
          statusInventer = false;
          timer.setTimeout(intervalInventer, hari9InventerPlus);
        }
      } else if(suhuAktual <= batasBawah) {
        pendingin = "off";
        blower1 = "off";
         if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari9InventerMin);
         }
      } else if(suhuAktual < batasBawah) {
        pemanas = "on";
      }
    } else if(hari == 10) {
      if(suhuAktual > batasAtas && blower1 == "on") {
        pemanas = "off"; //memastikan
        pendingin = "on";
      } else if(suhuAktual > batasAtas) {
        pemanas = "off";
        if(statusInventer) {
          statusInventer = false;
          timer.setTimeout(intervalInventer, hari10InventerPlus);
        }
      } else if(suhuAktual <= batasBawah) {
        pendingin = "off";
        blower1 = "off";
         if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari10InventerMin);
         }
      } else if(suhuAktual < batasBawah) {
        pemanas = "on";
      }
    } else if(hari == 11) {
      if(suhuAktual > batasAtas && blower1 == "on") {
        pemanas = "off"; //memastikan
        timer.setTimeout(interval, hari11InventerPendingin);
      } else if(suhuAktual > batasAtas) {
        pemanas = "off";
        if(statusInventer) {
          statusInventer = false;
          timer.setTimeout(intervalInventer, hari11InventerPlus);
        }
      } else if(suhuAktual <= batasBawah) {
        pendingin = "off";
         if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari11InventerMin);
         }
      } else if(suhuAktual < batasBawah) {
        pemanas = "on";
      }
    } else if(hari == 12) {
      if(suhuAktual > batasAtas) {
        pemanas = "off";
        if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari12InventerPlus);
         }
      } else if(suhuAktual < batasBawah) {
        pendingin = "off";
        if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari12InventerMin);
         }
      } else if(suhuAktual <= batasBawah) {
        pendingin = "off";
      } else if(suhuAktual >= batasAtas) {
        pemanas = "off";
      }
    } else if(hari == 13) {
      if(suhuAktual > batasAtas) {
        pemanas = "off";
        if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari13InventerPlus);
         }
      } else if(suhuAktual < batasBawah) {
        pendingin = "off";
        if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari13InventerMin);
         }
      } else if(suhuAktual <= batasBawah) {
        pendingin = "off";
      } else if(suhuAktual >= batasAtas) {
        pemanas = "off";
      }
    } else if(hari == 14) {
      if(suhuAktual > batasAtas) {
        pemanas = "off";
        if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari14InventerPlus);
         }
      } else if(suhuAktual < batasBawah) {
        pendingin = "off";
        if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari14InventerMin);
         }
      } else if(suhuAktual <= batasBawah) {
        pendingin = "off";
      } else if(suhuAktual >= batasAtas) {
        pemanas = "off";
      }
    } else if(hari == 15) {
      if(suhuAktual > batasAtas) {
        pemanas = "off";
        if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari15InventerPlus);
         }
      } else if(suhuAktual < batasBawah) {
        pendingin = "off";
        if(statusInventer) {
            statusInventer = false;
            timer.setTimeout(intervalInventer, hari15InventerMin);
         }
      } else if(suhuAktual <= batasBawah) {
        pendingin = "off";
      } else if(suhuAktual >= batasAtas) {
        pemanas = "off";
      }
    }

    // Intermittent();
    Serial.print("Status Inventer : ");
    Serial.println(statusInventer);
    Serial.print("Inventer : ");
    Serial.println(inventer);
    Serial.println("Blower1 : " + blower1);
    Serial.println("Blower2 : " + blower2);
    Serial.println("Blower3 : " + blower3);
    Serial.println("Pendingin : " + pendingin);
    Serial.println("Pemanas : " + pemanas);
    Serial.print("intermittent : ");
    Serial.println(intermittent);
    Serial.print("Suhu Ideal : ");
    Serial.print(suhu);
    Serial.print(" || Suhu batas bawah : ");
    Serial.print(batasBawah);
    Serial.print(" || Suhu batas atas : ");
    Serial.println(batasAtas);
    Serial.print("Suhu Aktual : ");
    Serial.println(suhuAktual);
    Serial.print("Hari ke : ");
    Serial.println(hari);
    Serial.println("==========================");
  }
  delay(500);
}

//intermittent
void Intermittent() {
  if(intermittent == "on") {
    if(hari == 0) {
      blower2 = "off";
      blower3 = "off";
      timer.setTimeout(interval, blower1On);
    }
  }
}

void blower1On() {
  blower1 = "on";
  timer.setTimeout(5000, blower1Off);
}

void blower1Off() {
  blower1 = "off";
}

//MCC
//hari ke 7
void hari7Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari7Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari7Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari7Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 7
//hari ke 8
void hari8Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari8Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari8Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari8Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 8
//hari ke 9
void hari9Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari9Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari9Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari9Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 9
//hari ke 10
void hari10Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari10Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari10Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari10Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 10
//hari ke 11
void hari11Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      // statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari11Blower3);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari11Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari11Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      intermittent = "on";
    } else {
      blower3 = "ok";
    }
}

void hari11Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
      pemanas = "off";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    }
  // }
}

void hari11Pemanas() {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
}
//end hari ke 11
//hari ke 12
void hari12Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari12Blower3);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari12Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari12Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      intermittent = "on";
    }
}

void hari12Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari12Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 12
//hari ke 13
void hari13Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari13Blower3);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari13Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari13Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      intermittent = "on";
    }
}

void hari13Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari13Pemanas() {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
}
//end hari ke 13
//hari ke 14
void hari14Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari14Blower3);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari14Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari14Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari14Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    }
  // }
}

void hari14Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 14
//hari ke 15
void hari15Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari15Blower3);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari15Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari15Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari15Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    }
  // }
}

void hari15Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 15
//hari ke 16
void hari16Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari16Blower3);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari16Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari16Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari16Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari16Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 16
//hari ke 17
void hari17Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari17Blower3);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari17Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari17Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari17Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari17Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 17
//hari ke 18
void hari18Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari18Blower3);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari18Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari18Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari18Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari18Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 18
//hari ke 19
void hari19Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari19Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari19Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari19Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari19Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari19Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 19
//hari ke 20
void hari20Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari20Blower3);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari20Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari20Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari20Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari20Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 20
//hari ke 21
void hari21Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari21Blower3);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari21Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari21Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari21Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari21Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 21
//hari ke 22
void hari22Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari21Blower3);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari22Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari22Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari22Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari22Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 22
//hari ke 23
void hari23Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari23Blower3);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari23Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari23Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari23Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari23Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 23
//hari ke 24
void hari24Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari24Blower3);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari24Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari24Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari24Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari24Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 24
//hari ke 25
void hari25Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari25Blower3);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari25Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari25Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari25Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari25Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 25
//hari ke 26
void hari26Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari26Blower3);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari26Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari26Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari26Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari26Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 26
//hari ke 27
void hari27Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari27Blower3);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari27Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari27Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari27Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari27Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 27
//hari ke 28
void hari28Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari28Blower3);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari28Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari28Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari28Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari28Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 28
//hari ke 29
void hari29Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari29Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari29Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari29Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari29Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari29Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 29
//hari ke 30
void hari30Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari30Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari30Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari30Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari30Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari30Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 30
//hari ke 31
void hari31Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari31Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari31Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari31Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari31Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari31Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 31
//hari ke 32
void hari32Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari32Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari32Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari32Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari32Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari32Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 32
//hari ke 33
void hari33Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari33Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari33Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari33Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari33Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari33Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 33
//hari ke 34
void hari34Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari34Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari34Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari34Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari34Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari34Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 34
//hari ke 35
void hari35Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari35Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari35Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari35Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari35Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari35Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 35
//hari ke 36
void hari36Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari36Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari36Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari36Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari36Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari36Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 36
//hari ke 37
void hari37Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari37Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari37Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari37Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari37Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari37Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 37
//hari ke 38
void hari38Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari38Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari38Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari38Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari38Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari38Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 38
//hari ke 39
void hari39Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari39Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari39Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari39Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari39Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari39Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 39
//hari ke 40
void hari40Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari40Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari40Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari40Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari40Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari40Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 40
//hari ke 41
void hari41Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari41Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari41Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari41Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari41Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari41Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 41
//hari ke 42
void hari42Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari42Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari42Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari42Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari42Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari42Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 42
//hari ke 43
void hari43Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari43Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari43Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari43Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari43Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari43Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 43
//hari ke 44
void hari44Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari44Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari44Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari44Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari44Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari44Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 44
//hari ke 45
void hari45Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari45Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari45Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari45Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari45Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari45Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 45
//hari ke 46
void hari46Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari46Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari46Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari46Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari46Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari46Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 46
//hari ke 47
void hari47Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari47Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari47Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari47Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari47Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari47Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 47
//hari ke 48
void hari48Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari48Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari48Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari48Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari48Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari48Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 48
//hari ke 49
void hari49Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari49Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari49Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari49Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari49Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari49Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 49
//hari ke 50
void hari50Blower2() {
    if(suhuAktual > batasAtas && blower1 == "on") {
      statusSuhu = "tidak_ideal";
      blower2 = "on";
      timer.setTimeout(interval, hari50Blower2);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari50Blower3() {
    if(suhuAktual > batasAtas && blower1 == "on" && blower2 == "on") {
      statusSuhu = "tidak_ideal";
      blower3 = "on";
      timer.setTimeout(interval, hari50Pendingin);
    } else if(suhuAktual == batasBawah) {
      statusSuhu = "ideal";
      blower1 = "off";
      blower2 = "off";
      intermittent = "on";
    }
}

void hari50Pendingin() {
  // if(statusSuhu != "ideal") {
    if(suhuAktual > batasBawah && blower1 == "on" && blower2 == "on" && blower3 == "on") {
      pendingin = "on";
    } else if(suhuAktual == batasBawah) {
      blower1 = "off";
      blower2 = "off";
      blower3 = "off";
      pendingin = "off";
      intermittent = "on";
      statusSuhu = "ideal";
    } else {
      pendingin = "lainny i";
    }
  // }
}

void hari50Pemanas() {
  if(statusSuhu != "ideal") {
    if(suhuAktual < batasBawah) {
      pemanas = "on";
    } else if(suhuAktual == batasAtas) {
      pemanas = "off";
    }
  }
}
//end hari ke 50
//end MCC

//MCC Inventer
//hari ke 0
void hari0InventerPlus() {
    if(inventer < 100) {
      inventer += 10;
      statusInventer = true;
    }
}

void hari0InventerMin() {
    if(inventer > 30) {
      inventer -= 10;
      statusInventer = true;
    }
}

// void hari0PendinginInventer() {
//   if(suhuAktual > batasBawah && inventer == 100) {
//     pendingin = "on";
//     pemanas = "off";
//   } else if(suhuAktual <= batasBawah) {
//     pendingin == "off";
//   }
// }

// void hari0PemanasInventer() {
//     if(suhuAktual < batasBawah) {
//       pendingin = "off";
//       pemanas = "on";
//     } else if(suhuAktual >= batasAtas) {
//       pemanas = "off";
//     }
// }
//end hari ke 0
//hari ke 1
void hari1InventerPlus() {
    if(inventer < 100) {
      inventer += 10;
      statusInventer = true;
    }
}

void hari1InventerMin() {
    if(inventer > 30) {
      inventer -= 10;
      statusInventer = true;
    }
}
//end hari ke 1
//hari ke 2
void hari2InventerPlus() {
    if(inventer < 100) {
      inventer += 10;
      statusInventer = true;
    }
}

void hari2InventerMin() {
    if(inventer > 30) {
      inventer -= 10;
      statusInventer = true;
    }
}
//end hari ke 2

//hari ke 5
void hari5InventerPlus() {
  if(suhuAktual > batasAtas && inventer == 100) {
    timer.setTimeout(interval, hari5InventerBlower1);
    // statusInventer = true;
  }
    if(inventer < 100) {
      inventer += 10;
      // statusInventer = true;
    }
    statusInventer = true;
}

void hari5InventerBlower1() {
    blower1 = "on";
}

void hari5InventerMin() {
  if(suhuAktual < batasBawah) {
    pemanas = "on";
    // statusInventer = true;
  }
    if(inventer > 30) {
      inventer -= 10;
      // statusInventer = true;
    }
    statusInventer = true;
}
//end hari ke 5

//hari ke 6
void hari6InventerPlus() {
  if(suhuAktual > batasAtas && inventer == 100) {
    timer.setTimeout(interval, hari6InventerBlower1);
    // statusInventer = true;
  }
    if(inventer < 100) {
      inventer += 10;
      // statusInventer = true;
    }
    statusInventer = true;
}

void hari6InventerBlower1() {
    blower1 = "on";
}

void hari6InventerMin() {
  if(suhuAktual < batasBawah) {
    pemanas = "on";
    // statusInventer = true;
  }
    if(inventer > 30) {
      inventer -= 10;
      // statusInventer = true;
    }
    statusInventer = true;
}
//end hari ke 6

//hari ke 7
void hari7InventerPlus() {
  if(suhuAktual > batasAtas && inventer == 100) {
    timer.setTimeout(interval, hari7InventerBlower1);
    // statusInventer = true;
  }
    if(inventer < 100) {
      inventer += 10;
      // statusInventer = true;
    }
    statusInventer = true;
}

void hari7InventerBlower1() {
    blower1 = "on";
}

void hari7InventerMin() {
  if(suhuAktual < batasBawah) {
    pemanas = "on";
    // statusInventer = true;
  }
    if(inventer > 30) {
      inventer -= 10;
      // statusInventer = true;
    }
    statusInventer = true;
}
//end hari ke 7

//hari ke 8
void hari8InventerPlus() {
  if(suhuAktual > batasAtas && inventer == 100) {
    timer.setTimeout(interval, hari8InventerBlower1);
    // statusInventer = true;
  }
    if(inventer < 100) {
      inventer += 10;
      // statusInventer = true;
    }
    statusInventer = true;
}

void hari8InventerBlower1() {
    blower1 = "on";
}

void hari8InventerMin() {
  if(suhuAktual < batasBawah) {
    pemanas = "on";
    // statusInventer = true;
  }
    if(inventer > 30) {
      inventer -= 10;
      // statusInventer = true;
    }
    statusInventer = true;
}
//end hari ke 8

//hari ke 9
void hari9InventerPlus() {
  if(suhuAktual > batasAtas && inventer == 100) {
    timer.setTimeout(interval, hari9InventerBlower1);
    // statusInventer = true;
  }
    if(inventer < 100) {
      inventer += 10;
      // statusInventer = true;
    }
    statusInventer = true;
}

void hari9InventerBlower1() {
    blower1 = "on";
}

void hari9InventerMin() {
  if(suhuAktual < batasBawah) {
    pemanas = "on";
    // statusInventer = true;
  }
    if(inventer > 30) {
      inventer -= 10;
      // statusInventer = true;
    }
    statusInventer = true;
}
//end hari ke 9

//hari ke 10
void hari10InventerPlus() {
  if(suhuAktual > batasAtas && inventer == 100) {
    timer.setTimeout(interval, hari10InventerBlower1);
    // statusInventer = true;
  }
    if(inventer < 100) {
      inventer += 10;
      // statusInventer = true;
    }
    statusInventer = true;
}

void hari10InventerBlower1() {
    blower1 = "on";
}

void hari10InventerMin() {
  if(suhuAktual < batasBawah) {
    pemanas = "on";
    // statusInventer = true;
  }
    if(inventer > 30) {
      inventer -= 10;
      // statusInventer = true;
    }
    statusInventer = true;
}
//end hari ke 10

//hari ke 11
void hari11InventerPlus() {
  if(suhuAktual > batasAtas && inventer == 100) {
    timer.setTimeout(interval, hari11InventerBlower1);
    // statusInventer = true;
  }
    if(inventer < 100) {
      inventer += 10;
      // statusInventer = true;
    }
    statusInventer = true;
}

void hari11InventerBlower1() {
    blower1 = "on";
    if(suhuAktual > batasAtas && blower1 == "on") {
      timer.setTimeout(interval, hari11InventerBlower2);
    } else if(suhuAktual <= batasBawah && inventer == 30);
}

void hari11InventerBlower2() {
  if(suhuAktual > batasAtas && blower1 == "on") {
    blower2 = "on";
  } else if(suhuAktual <= batasBawah && blower1 == "off") {
    blower2 = "off";
  }
}

void hari11InventerMin() {
  if(suhuAktual <= batasBawah) {
    pemanas = "on";
    timer.setTimeout(interval, hari11InventerBlower2);
    // statusInventer = true;
  }
    if(inventer > 30) {
      inventer -= 10;
      // statusInventer = true;
    }
    statusInventer = true;
}

void hari11InventerPendingin() {
  if(suhuAktual > batasAtas && inventer == 100 && blower1 == "on" && blower2 == "on") {
    pendingin = "on";
  }
}
//end hari ke 11

//hari ke 12
void hari12InventerPlus() {
  if(suhuAktual > batasAtas && inventer == 100) {
    timer.setTimeout(interval, hari12InventerBlower1);
  }
    if(inventer < 100) {
      inventer += 10;
    }
    statusInventer = true;
}

void hari12InventerMin() {
  if(suhuAktual <= batasBawah && inventer == 50) {
    pemanas = "on";
  }
  if(suhuAktual <= batasBawah) {
    timer.setTimeout(interval, hari12InventerBlower2);
  }
  if(suhuAktual < batasBawah && blower1 == "off" && blower2 == "off") {
    if(inventer > 50) {
      inventer -= 10;
    }
  }
    statusInventer = true;
}

void hari12InventerBlower1() {
    if(suhuAktual > batasAtas && inventer == 100) {
      blower1 = "on";
      timer.setTimeout(interval, hari12InventerBlower2);
    } else if(suhuAktual <= batasBawah && blower2 == "off") {
      blower1 = "off";
    }
}

void hari12InventerBlower2() {
  if(suhuAktual > batasAtas && blower1 == "on") {
    blower2 = "on";
    timer.setTimeout(interval, pendinginOn);
  } else if(suhuAktual <= batasBawah) {
    blower2 = "off";
    timer.setTimeout(interval, hari12InventerBlower1);
  }
}

void pendinginOn() {
  if(suhuAktual > batasAtas && inventer == 100 && blower1 == "on")
  pendingin = "on";
}
//end hari ke 12

//hari ke 13
void hari13InventerPlus() {
  if(suhuAktual > batasAtas && inventer == 100) {
    timer.setTimeout(interval, hari13InventerBlower1);
  }
    if(inventer < 100) {
      inventer += 10;
    }
    statusInventer = true;
}

void hari13InventerMin() {
  if(suhuAktual <= batasBawah && inventer == 50) {
    pemanas = "on";
  }
  if(suhuAktual <= batasBawah) {
    timer.setTimeout(interval, hari13InventerBlower2);
  }
  if(suhuAktual < batasBawah && blower1 == "off" && blower2 == "off") {
    if(inventer > 50) {
      inventer -= 10;
    }
  }
    statusInventer = true;
}

void hari13InventerBlower1() {
    if(suhuAktual > batasAtas && inventer == 100) {
      blower1 = "on";
      timer.setTimeout(interval, hari13InventerBlower2);
    } else if(suhuAktual <= batasBawah && blower2 == "off") {
      blower1 = "off";
    }
}

void hari13InventerBlower2() {
  if(suhuAktual > batasAtas && blower1 == "on") {
    blower2 = "on";
    timer.setTimeout(interval, pendinginOn);
  } else if(suhuAktual <= batasBawah) {
    blower2 = "off";
    timer.setTimeout(interval, hari13InventerBlower1);
  }
}

void pendinginOn() {
  if(suhuAktual > batasAtas && inventer == 100 && blower1 == "on")
  pendingin = "on";
}
//end hari ke 13

//hari ke 14
void hari14InventerPlus() {
  if(suhuAktual > batasAtas && inventer == 100) {
    timer.setTimeout(interval, hari14InventerBlower1);
  }
    if(inventer < 100) {
      inventer += 10;
    }
    statusInventer = true;
}

void hari14InventerMin() {
  if(suhuAktual <= batasBawah && inventer == 50) {
    pemanas = "on";
  }
  if(suhuAktual <= batasBawah) {
    timer.setTimeout(interval, hari14InventerBlower2);
  }
  if(suhuAktual < batasBawah && blower1 == "off" && blower2 == "off") {
    if(inventer > 50) {
      inventer -= 10;
    }
  }
    statusInventer = true;
}

void hari14InventerBlower1() {
    if(suhuAktual > batasAtas && inventer == 100) {
      blower1 = "on";
      timer.setTimeout(interval, hari14InventerBlower2);
    } else if(suhuAktual <= batasBawah && blower2 == "off") {
      blower1 = "off";
    }
}

void hari14InventerBlower2() {
  if(suhuAktual > batasAtas && blower1 == "on") {
    blower2 = "on";
    timer.setTimeout(interval, pendinginOn);
  } else if(suhuAktual <= batasBawah) {
    blower2 = "off";
    timer.setTimeout(interval, hari14InventerBlower1);
  }
}

void pendinginOn() {
  if(suhuAktual > batasAtas && inventer == 100 && blower1 == "on")
  pendingin = "on";
}
//end hari ke 14

//hari ke 15
void hari15InventerPlus() {
  if(suhuAktual > batasAtas && inventer == 100) {
    timer.setTimeout(interval, hari15InventerBlower1);
  }
    if(inventer < 100) {
      inventer += 10;
    }
    statusInventer = true;
}

void hari15InventerMin() {
  if(suhuAktual <= batasBawah && inventer == 50) {
    pemanas = "on";
  }
  if(suhuAktual <= batasBawah) {
    timer.setTimeout(interval, hari15InventerBlower2);
  }
  if(suhuAktual < batasBawah && blower1 == "off" && blower2 == "off") {
    if(inventer > 50) {
      inventer -= 10;
    }
  }
    statusInventer = true;
}

void hari15InventerBlower1() {
    if(suhuAktual > batasAtas && inventer == 100) {
      blower1 = "on";
      timer.setTimeout(interval, hari15InventerBlower2);
    } else if(suhuAktual <= batasBawah && blower2 == "off") {
      blower1 = "off";
    }
}

void hari15InventerBlower2() {
  if(suhuAktual > batasAtas && blower1 == "on") {
    blower2 = "on";
    timer.setTimeout(interval, pendinginOn);
  } else if(suhuAktual <= batasBawah) {
    blower2 = "off";
    timer.setTimeout(interval, hari15InventerBlower1);
  }
}

void pendinginOn() {
  if(suhuAktual > batasAtas && inventer == 100 && blower1 == "on")
  pendingin = "on";
}
//end hari ke 15

//end MCC Inventer
