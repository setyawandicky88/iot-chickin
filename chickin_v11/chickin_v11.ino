int id_perangkat_a;
int id_perangkat_b;
int id_perangkat_c;

#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <WiFiUdp.h>
#include <ESP8266HTTPClient.h>
#include <NTPClient.h>
#include <Ticker.h>
//#include <WiFiManager.h>
#include <ArduinoJson.h>
#include "DHT.h"
#include <Keypad_I2C.h>
#include <Keypad.h>
#include <Wire.h>
#include <SPI.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include <EEPROM.h>
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 20, 4);

WiFiClient wificlient;

//++++++++++++++++++++Menu dan keypad++++++++++++++++++++

String modePendingin = "";
String modePemanas = "";
String modeBlower = "";
String SetTimer = "";
String nama = "Chickin Panel";
String input = "";
String dataT = "36";
String dataH = "24";
String koneksi = "OK";
int setpoin;
int SetSpeed = 0;
int batas ;
String Mode = "", BatasH = "", BatasT = "";
int Cancel = 0, Up = 0, Down = 0, Ok = 0;
int lockCancel = 0, lockUp = 0, lockDown = 0, lockOk = 0 ;
int dis = 1, sub = 0, mode = 0, endsub = 0;

#define I2CADDR 0x20

const byte ROWS = 4; //four rows
const byte COLS = 4; //three columns
char keys[ROWS][COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};
// Digitran keypad, bit numbers of PCF8574 i/o port
byte rowPins[ROWS] = {0, 1, 2, 3};
byte colPins[COLS] = {4, 5, 6, 7};

TwoWire *jwire = &Wire;   //test passing pointer to keypad lib
Keypad_I2C kpd( makeKeymap(keys), rowPins, colPins, ROWS, COLS, I2CADDR, PCF8574, jwire );
//Keypad_I2C kpd( makeKeymap(keys), rowPins, colPins, ROWS, COLS, I2CADDR );

static unsigned long t_awal, t_awal1, t_awal2;
static unsigned long t_awal2_1, t_awal2_2, t_awal2_3;
static unsigned long t_setelah, t_setelah1, t_setelah2;
static unsigned long t_setelah2_1, t_setelah2_2, t_setelah2_3;
int kondisi, kondisi2;
int kondisi2_1, kondisi2_2, kondisi2_3;
//static unsigned long t = 1000;
//++++++++++++++++++++Yusuf Update Oled++++++++++++++++++++

//void configModeCallback(WiFiManager *myWiFiManager);

void button_reconnect();
void Sensor();
void tampil();
void terminal();
float h;
float t;
float f;
String SPendingin = "";
String SPemanas = "";
String SBlower = "";
int ModePanel;

const char led_indikator_pin = D4;
const char relay1_pin = D3;
const char relay2_pin = D6;
const char pinSwitch = D0;
//d1 dan d2 untuk lcd panel

#define DHTPIN D5 // Digital pin connected to the DHT sensor
#define pinButton D7
#define pinInverter 15
//const char led1_pin= D6;
//const char led1_pin= D7;
//const char led1_pin= D6;

void tick()
{
  //toggle state
  digitalWrite(led_indikator_pin, !digitalRead(led_indikator_pin));
  lcd.setCursor(0, 0); lcd.print("#Entered config mode");
  lcd.setCursor(3, 1); lcd.print("----Please----");
  lcd.setCursor(1, 2); lcd.print("Setting the Wifi");
  //lcd.clear();
  //  oled1.clearDisplay();
  //  oled1.setTextSize(1);
  //  oled1.println("Connecting");
  //  oled1.println("WiFi...");
  //  oled1.display();
}

//const char *host = "http://app.chickin.id/api_manual/api.php?id_device=97";

int buttonState;
//int data_batas;
//int *batas = &data_batas;

int suhu_batas_mati0;
int suhu_batas_nyala0;
int kelembapan_batas_mati0;
int kelembapan_batas_nyala0;

int suhu_batas_mati1;
int suhu_batas_nyala1;
int kelembapan_batas_mati1;
int kelembapan_batas_nyala1;

int suhu_batas_mati2;
int suhu_batas_nyala2;
int kelembapan_batas_mati2;
int kelembapan_batas_nyala2;

int jam0, menit0;
int jam1, menit1;
int jam2, menit2;
String mode0 = "", mode1 = "", mode2 = "";

String timerstatus0 = "", timerstatus1 = "", timerstatus2 = "";

int nilai;
int waktu_delay = 3 * 60000; //1 menit
int suhudelay;
int lembapdelay;


int cek = 0;
const long utcOffsetInSeconds = 25200;
char daysOfTheWeek[7][12] = {"Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"};

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "id.pool.ntp.org", utcOffsetInSeconds);

Ticker ticker;
//WiFiManager wm;

#define DHTTYPE DHT22 // DHT 22  (AM2302), AM2321
DHT dht(DHTPIN, DHTTYPE);


//-----------

const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial, Helvetica, sans-serif;}
* {box-sizing: border-box;}

input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=password] {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=submit] {
  background-color: #04AA6D;
  color: white;
  padding: 12px 20px;
  border: none;
  border-radius: 4px;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
}
</style>
</head>
<body>
<h3>Selamat Datang</h3>
<h3>Login Perangkat</h3>

<div class="container">
  <form action="/GetDataFrom">
    <label for="SSIDWIFI">SSID WIFI</label>
    <input type="text" id="SSIDWIFI" name="wifi" placeholder="SSID WiFi">

    <label for="PASSWIFI">Password WIFI</label>
    <input type="password" id="PASSWIFI" name="passwifi" placeholder="Password WiFi">

    <label for="USER-ID">USER-ID</label>
    <input type="text" id="USERID" name="id_user" placeholder="USER ID">
   
    <input type="submit" value="Submit">
  </form>
</div>

</body>
</html>
)rawliteral";

char *Param_SSIDWIFI = "wifi";
char *Param_PassWIFI = "passwifi";
char *Param_UserID = "id_user";

char WIFISSID[50];
char WIFIPASS[50];
char USERID[4];

char Data_ID1[4];
char Data_ID2[4];
char Data_ID3[4];
char Data_ID_Device[4];
char GET_ID_Device[4];

#define EEPROM_SIZE 512
AsyncWebServer server(80);
//#define LED4 D4 //indikator

void BacaEEPROMMemory(int mulaiMemory, int maxLength, char *Descript)
{
  EEPROM.begin(EEPROM_SIZE);
  delay(500);
  for (int i = 0; i < maxLength; i++)
  {
    Descript[i] = char(EEPROM.read(mulaiMemory + i));
  }
  delay(500);
  EEPROM.end();
  Serial.print("Membaca Memory : ");
  Serial.println(Descript);
}

void TulisEEPROMMemory(int mulaiMemory, int maxLength, char *Write)
{
  EEPROM.begin(EEPROM_SIZE);
  delay(500);
  Serial.println();
  Serial.print("Tulis Memory : ");
  for (int i = 0; i < maxLength; i++)
  {
    EEPROM.write(mulaiMemory + i, Write[i]);
    Serial.print(Write[i]);
  }
  EEPROM.commit();
  delay(500);
  EEPROM.end();
}

void ConnectWIFI()
{
  Serial.println();
  BacaEEPROMMemory(0, 50, WIFISSID);
  BacaEEPROMMemory(51, 50, WIFIPASS);
  
  Serial.println();
  Serial.print("Turn Off STA Mode");
  WiFi.softAPdisconnect (true);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(String(WIFISSID));

  WiFi.begin(WIFISSID, WIFIPASS);
        int i = 0;
        while (WiFi.status() != WL_CONNECTED) { // Wait for the Wi-Fi to connect
          delay(1000);
          Serial.print(++i); 
          Serial.print(' ');
          if (i > 30){
            ESP.restart();
          }
          buttonState = digitalRead(pinButton);
          Serial.print(buttonState);
          Serial.print(' ');
          if (buttonState == HIGH)
          {  String a = " ";
              a.toCharArray(WIFISSID, 50);
              delay(500);
              TulisEEPROMMemory(0, 50, WIFISSID);
  
              ESP.restart();
              }
          }
        

  Serial.println();
  Serial.println("WiFi connected");
  Serial.print("IP address : ");
  Serial.println(WiFi.localIP());
}

void HandleWebLoginPerangkat()
{
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request)
            { request->send_P(200, "text/html", index_html); });

  server.on("/GetDataFrom", HTTP_GET, [](AsyncWebServerRequest *request)
            {
              String wifiMessage;
              String passwifiMessage;
              String useridMessage;

              if (request->hasParam(Param_SSIDWIFI) && request->hasParam(Param_PassWIFI) && request->hasParam(Param_UserID))
              {
                wifiMessage = request->getParam(Param_SSIDWIFI)->value();
                wifiMessage.toCharArray(WIFISSID, 50);
                delay(500);
                TulisEEPROMMemory(0, 50, WIFISSID);

                passwifiMessage = request->getParam(Param_PassWIFI)->value();
                passwifiMessage.toCharArray(WIFIPASS, 50);
                delay(500);
                TulisEEPROMMemory(51, 50, WIFIPASS);

                useridMessage = request->getParam(Param_UserID)->value();
                useridMessage.toCharArray(USERID, 4);
                delay(500);
                TulisEEPROMMemory(101, 4, USERID);
                
              Serial.println("\n\nSistem Reset");
              ESP.restart();
              }
            });

  server.onNotFound(notFound);
  server.begin();
}
void notFound(AsyncWebServerRequest *request)
{
  request->send(404, "text/plain", "Not found");
}

String ID1, ID2, ID3, ID_Device;
String Data_Perangkat;

void ModeGetCredential()
{
  Serial.println();
  ConnectWIFI();
  BacaEEPROMMemory(101, 4, USERID);
  BacaEEPROMMemory(106, 4, GET_ID_Device);
  Serial.println();

  String host1 = "http://app.chickin.id/api/v2/add-device?id_user=" + String(USERID) + "&&nama_device=flock";
  HTTPClient http;

  Serial.print("Request Link:");
  Serial.println(host1);

  http.begin(wificlient, host1); //Specify request destination

  int httpCode = http.GET();         //Send the request
  String payload = http.getString(); //Get the response payload from server

  Serial.print("Response Code:"); //200 is OK
  Serial.println(httpCode);       //Print HTTP return code

  Serial.print("Returned data from Server:");
  Serial.println(payload); //Print request response payload

  if (httpCode == 200)
  {
    // Allocate JsonBuffer
    // Use arduinojson.org/assistant to compute the capacity.
    const size_t capacity = JSON_OBJECT_SIZE(3) + JSON_ARRAY_SIZE(2) + 60;
    DynamicJsonBuffer jsonBuffer(capacity);

    // Parse JSON object
    JsonObject &root = jsonBuffer.parseObject(payload);
    if (!root.success())
    {
      Serial.println(F("Parse Data Gagal!"));
      return;
    }
    JsonObject &result_0 = root["result"];

    //JSON Parse LED1
    Serial.println(F("Response:"));
    Serial.println(result_0["status"].as<char *>());

    if (result_0["status"] == "sukses")
    {
      Serial.println(result_0["msg"].as<char *>());
      Serial.println("Berhasil Mendapat Credential");
     // digitalWrite(LED4, HIGH);

      ID1 = String(result_0["perangkat1"].as<char *>());
      ID2 = String(result_0["perangkat2"].as<char *>());
      ID3 = String(result_0["perangkat3"].as<char *>());
      ID_Device = String(result_0["iddevice"].as<char *>());

      ID1.toCharArray(Data_ID1, 4);
      delay(500);
      TulisEEPROMMemory(201, 4, Data_ID1);

      ID2.toCharArray(Data_ID2, 4);
      delay(500);
      TulisEEPROMMemory(205, 4, Data_ID2);

      ID3.toCharArray(Data_ID3, 4);
      delay(500);
      TulisEEPROMMemory(209, 4, Data_ID3);

      ID_Device.toCharArray(Data_ID_Device, 4);
      delay(500);
      TulisEEPROMMemory(213, 4, Data_ID_Device);
    }
    else if (result_0["success"] == "false")
    {
      Serial.println(result_0["msg"].as<char *>());
      Serial.println("Gagal Mendapat Token");
      //digitalWrite(LED4, LOW);
    }
    //=======================================================================================
  }
  else
  {
    Serial.println("Error in response");
  }
  http.end(); //Close connection
  Serial.println("\nBerhasil Hore2");
  delay(5000);
}

const char *ssidAP = "Test-V4-2";
const char *passwordAP = "";

int PitStop, PitStop2;
//-----------


void setup()
{  
  Serial.begin(115200);
  delay(10);
  Serial.println();
  lcd.begin(20, 4);
  lcd.init();
  lcd.backlight();

  while ( !Serial ) {
    /*wait*/
  }
  //  Wire.begin( );
  jwire->begin( );
  //  kpd.begin( makeKeymap(keys) );
  kpd.begin( );
  Serial.print( "start with pinState = " );
  Serial.println( kpd.pinState_set( ), HEX );

  lcd.home();

  lcd.setCursor(0, 0);lcd.print("Welcome");
  lcd.setCursor(0, 1);lcd.print("Chickin Panel");
  lcd.setCursor(0, 2);lcd.print("V 3.1");
  delay(2000);
  lcd.clear();
  lcd.setCursor(1, 1); lcd.print("...Please Wait...");
  lcd.setCursor(5, 2); lcd.print("Starting");
  delay(1000);
  lcd.clear();
  
  //WiFi.mode(WIFI_STA);
  ticker.attach(0.2, tick);
  //wm.setConfigPortalTimeout(300);


  pinMode(led_indikator_pin, OUTPUT);
  pinMode(relay1_pin, OUTPUT);
  pinMode(relay2_pin, OUTPUT);
  pinMode(pinSwitch, INPUT);
  //  pinMode(led1_pin, OUTPUT);
  // pinMode(led2_pin, OUTPUT);
  //  pinMode(led3_pin, OUTPUT);
  pinMode(pinButton, INPUT_PULLUP);

  dht.begin();
  timeClient.begin();

  digitalWrite(led_indikator_pin, HIGH);
  delay(100);
  digitalWrite(led_indikator_pin, LOW);
  delay(100);
  digitalWrite(led_indikator_pin, HIGH);
  delay(100);
  digitalWrite(led_indikator_pin, LOW);
  delay(100);

  std::vector<const char *> menu = {"wifi", "sep", "restart", "exit"};
  /*wm.setMenu(menu);

  //wm.setClass("invert");


  WiFi.mode(WIFI_STA);
  if (!wm.autoConnect())
  {
    Serial.println("failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    ESP.restart();
    delay(1000);
  }
  ticker.detach();*/
  //++++++++++++++++++++Yusuf Update Oled++++++++++++++++++++

  kondisi = 0;
  kondisi2 = 0;
  lcd.clear();

  SPendingin = "Off";
  SPemanas = "Off";
  SBlower = "Off";

  //++++++++++++++++++++Yusuf Update Oled++++++++++++++++++++
//    timer1_attachInterrupt(tampil);
//    timer1_enable(TIM_DIV16, TIM_EDGE, TIM_SINGLE);
//    timer1_write(600000); //120000 us


//-------------
  BacaEEPROMMemory(201, 4, Data_ID1);
  BacaEEPROMMemory(205, 4, Data_ID2);
  BacaEEPROMMemory(209, 4, Data_ID3);
  String a = String(Data_ID1);
  String b = String(Data_ID2);
  String c = String(Data_ID3);
  id_perangkat_a = a.toInt();
  id_perangkat_b = b.toInt();
  id_perangkat_c = c.toInt();
  Serial.println(id_perangkat_a);
  Serial.println(id_perangkat_b);
  Serial.println(id_perangkat_c);
//-------------
//WiFi.mode(WIFI_STA);
}

int ResetCheck = 0;

void loop()
{   


//    lcd.clear();
//    lcd.setCursor(0, 0); lcd.print("==Status Aktual");
//    lcd.setCursor(0,1); lcd.print("P.dgn :" + SPendingin );
//    lcd.setCursor(0,2); lcd.print("P.mns :" + SPemanas ); 
//    lcd.setCursor(0,3); lcd.print("Blower:" + SBlower );
//    
//    //lcd.setCursor(9, 0); lcd.print("|");
//    lcd.setCursor(10, 1); lcd.print("|");
//    lcd.setCursor(10, 2); lcd.print("|");
//    lcd.setCursor(10, 3); lcd.print("|");
//
//    String dataT = String(suhudelay);
//    String dataH = String(lembapdelay);
//    lcd.setCursor(11, 1); lcd.print("Suhu:" + dataT + (char)223 + "C");
//    lcd.setCursor(11, 2); lcd.print("Lemb:" + dataH + "%");
//    lcd.setCursor(11, 3); lcd.print("Conn:" + koneksi);
    
  Sensor();

  
  
  t_setelah = millis() - t_awal;
  if (t_setelah > waktu_delay)
  {
    suhudelay = t;
    lembapdelay = h;
    t_awal = millis();
  }

  Serial.print("Suhu Delay : ");
  Serial.println(suhudelay);
  Serial.print("Kelempan Delay : ");
  Serial.println(lembapdelay);
  buttonState = digitalRead(pinButton);
  Serial.print("=========\n");
  Serial.println("Status Button : " + String(buttonState));
  Serial.print("=========\n\n");
  //buttonState == LOW;

   ModePanel=digitalRead(pinSwitch); 
  Serial.print("\nModePanel : ");
  Serial.println(ModePanel);
  
  if(ModePanel==1){ ///------------------------------------------------ flex panel
      cek++;
      
        //-------
  PitStop ? Serial.println("PitStop Fungsi 1 - CLosed") : Serial.println("PitStop Fungsi 1 - Open");
  PitStop2 ? Serial.println("PitStop Fungsi 2 - Closed") : Serial.println("PitStop Fungsi 2 - Open");

  //buttonState == LOW;
  if (buttonState == HIGH || PitStop == 0)
  {
    ResetCheck++;
    Serial.print("Counting Reset : ");
    Serial.println(ResetCheck);
    if (ResetCheck > 1)
    {
      ResetCheck = 0;
      Serial.println("Masuk Pak Eko");
      button_reconnect();
    }
    if (PitStop2 == 0)
    {
      WiFi.mode(WIFI_AP);
      WiFi.softAP(ssidAP, passwordAP);
      IPAddress IP = WiFi.softAPIP();

      Serial.println();
      Serial.print("IP Address Nodemcu : ");
      Serial.println(IP);

      //Web Login
      HandleWebLoginPerangkat(); 
     
      BacaEEPROMMemory(0, 50, WIFISSID);
      if(isAlphaNumeric(WIFISSID[0])){
        ModeGetCredential();
      }else{
        Serial.println("Waktu Tunggu Input User");
        delay(180000);
        BacaEEPROMMemory(0, 50, WIFISSID);

        int i = 0;
        while (WiFi.status() != WL_CONNECTED) { // Wait for the Wi-Fi to connect
          delay(1000);
          Serial.print(i++); 
          Serial.print(' ');
          if (i > 10){
            ESP.restart();
          }
        }
      }  
    }
    PitStop=1;
    PitStop2=1;
    Serial.println("================================== reconnect");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP()); //You can get IP address assigned to ESP
  }
  BacaEEPROMMemory(213, 4, GET_ID_Device);
  Serial.println();
  

  //-------
  String host = "http://app.chickin.id/api_manual/api.php?id_device=" + String(GET_ID_Device);
  //-------

  HTTPClient http;  //Declare object of class httpClient
  http.begin(wificlient, host); //Specify request destination

  Serial.print("Request Link:");
  Serial.println(host);

  int httpCode = http.GET();         //Send the request
  String payload = http.getString(); //Get the response payload from server

  Serial.print("Response Code:"); //200 is OK
  Serial.println(httpCode);       //Print http return code

  timeClient.update();

  Serial.print("Returned data from Server:");
  Serial.println(payload); //Print request response

  Serial.print("Cek");
  Serial.println(cek); //Print request response
    if(cek>5){

  if (httpCode == 200)
  {
    koneksi = "OK";
    //cek = 0;
    ticker.detach();
    // Allocate JsonBuffer
    // Use arduinojson.org/assistant to compute the capacity.
    const size_t capacity = JSON_OBJECT_SIZE(3) + JSON_ARRAY_SIZE(2) + 60;
    DynamicJsonBuffer jsonBuffer(capacity);

    // Parse JSON object
    JsonObject &root = jsonBuffer.parseObject(payload);
    if (!root.success())
    {
      Serial.println(F("Parsing failed!"));
      //delay(1000);
      return;
    }

    JsonObject &result_0 = root["perangkat"][0];
    JsonObject &result_1 = root["perangkat"][1];
    JsonObject &result_2 = root["perangkat"][2];
    JsonObject &result_3 = root["user"][0];
    
    Serial.println(result_3["id"].as<char *>());
    Serial.println(result_3["name"].as<char *>());
    Serial.println(result_3["email"].as<char *>());
    nama =result_3["name"].as<char *>();
    
    Serial.println(F("Response 0:"));
    Serial.println(result_0["id"].as<char *>());
    Serial.println(result_0["nama"].as<char *>());
    Serial.println(result_0["mode"].as<char *>());
    mode0=result_0["mode"].as<char *>();
    Serial.println (mode0);
    Serial.println(result_0["status"].as<char *>());
    timerstatus0=result_0["status"].as<char *>();
    Serial.println(result_0["suhu_aktual"].as<char *>());
    Serial.println(result_0["suhu_batas_nyala"].as<char *>());
    Serial.println(result_0["suhu_batas_mati"].as<char *>());
    Serial.println(result_0["kelembapan_aktual"].as<char *>());
    Serial.println(result_0["kelembapan_batas_nyala"].as<char *>());
    Serial.println(result_0["kelembapan_batas_mati"].as<char *>());
    Serial.println(result_0["jam_mati"].as<char *>());
    Serial.println(result_0["menit_mati"].as<char *>());
    //Serial.println(result_0["speed"].as<char*>());
    jam0 = result_0["jam_mati"];
    menit0 = result_0["menit_mati"];
    suhu_batas_mati0 = result_0["suhu_batas_mati"];
    suhu_batas_nyala0 = result_0["suhu_batas_nyala"];
    kelembapan_batas_mati0 = result_0["kelembapan_batas_mati"];
    kelembapan_batas_nyala0 = result_0["kelembapan_batas_nyala"];

    Serial.println(F("Response 1:"));
    Serial.println(result_1["id"].as<char *>());
    Serial.println(result_1["nama"].as<char *>());
    Serial.println(result_1["mode"].as<char *>());
    mode1=result_1["mode"].as<char *>();
    Serial.println (mode1);
    Serial.println(result_1["status"].as<char *>());
    timerstatus1=result_1["status"].as<char *>();
    Serial.println(result_1["suhu_aktual"].as<char *>());
    Serial.println(result_1["suhu_batas_nyala"].as<char *>());
    Serial.println(result_1["suhu_batas_mati"].as<char *>());
    Serial.println(result_1["kelembapan_aktual"].as<char *>());
    Serial.println(result_1["kelembapan_batas_nyala"].as<char *>());
    Serial.println(result_1["kelembapan_batas_mati"].as<char *>());
    Serial.println(result_1["jam_mati"].as<char *>());
    Serial.println(result_1["menit_mati"].as<char *>());
    //Serial.println(result_1["speed"].as<char*>());
    jam1 = result_1["jam_mati"];
    menit1 = result_1["menit_mati"];
    suhu_batas_mati1 = result_1["suhu_batas_mati"];
    suhu_batas_nyala1 = result_1["suhu_batas_nyala"];
    kelembapan_batas_mati1 = result_1["kelembapan_batas_mati"];
    kelembapan_batas_nyala1 = result_1["kelembapan_batas_nyala"];

    Serial.println(F("Response 2:"));
    Serial.println(result_2["id"].as<char *>());
    Serial.println(result_2["nama"].as<char *>());
    Serial.println(result_2["mode"].as<char *>());
    mode2=result_2["mode"].as<char *>();
    Serial.println (mode2);
    Serial.println(result_2["status"].as<char *>());
    timerstatus2=result_2["status"].as<char *>();
    Serial.println(result_2["suhu_aktual"].as<char *>());
    Serial.println(result_2["suhu_batas_nyala"].as<char *>());
    Serial.println(result_2["suhu_batas_mati"].as<char *>());
    Serial.println(result_2["kelembapan_aktual"].as<char *>());
    Serial.println(result_2["kelembapan_batas_nyala"].as<char *>());
    Serial.println(result_2["kelembapan_batas_mati"].as<char *>());
    Serial.println(result_2["jam_mati"].as<char *>());
    Serial.println(result_2["menit_mati"].as<char *>());
    Serial.println(result_2["speed"].as<char*>());
    SetSpeed = result_2["speed"];
    jam2 = result_2["jam_mati"];
    menit2 = result_2["menit_mati"];
    suhu_batas_mati2 = result_2["suhu_batas_mati"];
    suhu_batas_nyala2 = result_2["suhu_batas_nyala"];
    kelembapan_batas_mati2 = result_2["kelembapan_batas_mati"];
    kelembapan_batas_nyala2 = result_2["kelembapan_batas_nyala"];

    //Serial.println(root["data"][1].as<char*>());
    Serial.print("Hari : ");
    Serial.println(daysOfTheWeek[timeClient.getDay()]);
    Serial.print("Jam : ");
    Serial.print(timeClient.getHours());
    Serial.print(":");
    Serial.print(timeClient.getMinutes());
    Serial.print(":");
    Serial.println(timeClient.getSeconds());

    String serverName11 = "http://app.chickin.id/api_manual/api_aktual_sensor.php?id=";
    String serverPath11 = serverName11 + id_perangkat_a;
    String serverName22 = "&suhu=";
    String serverPath22 = serverPath11 + serverName22 + suhudelay;
    String serverName33 = "&lembap=";
    String serverPath33 = serverPath22 + serverName33 + lembapdelay;
    Serial.print("update nilai :");
    Serial.println(serverPath33);
    // Your Domain name with URL path or IP address with path
    http.begin(wificlient, serverPath33.c_str());
    int httpCode = http.GET();    

    Serial.print("Mode0 :");
    Serial.println(mode0);
        Serial.print("Mode1 :");
    Serial.println(mode1);
        Serial.print("Mode2 :");
    Serial.println(mode2);
    //===== PENDINGIN =====
    //=====================

    if (mode0 == "timer" && timerstatus0 == "nyala")
    {
      Serial.println("-----------------MODE Timer pendingin---------------------");

      int timer = jam0 * 60000 ;
      timer = timer + menit0;
      timer = timer * 60000;
      int timer2 = timer * 2;

      t_setelah2_1 = millis() - t_awal2_1;
      if (kondisi2_1 == 0)
      {
        if (t_setelah2_1 > timer)
        {
          Serial.print("interval nyala");
//          digitalWrite(relay1_pin, HIGH);
//          Serial.println(" | pendingin dinyalakan");
          SPendingin = "On";
          kondisi2_1 = 1;
        }
      }

      if (t_setelah2_1 > timer2)
      {
        Serial.print("interval mati");
//        digitalWrite(relay1_pin, LOW);
//        Serial.println(" | pendingin dimatikan");
        SPendingin = "Off";
        t_awal2_1 = millis();
        kondisi2_1 = 0;
      }

      //      String serverName = "http://app.chickin.id/api_manual/api_aktual.php?id=";
      //      String serverPathawal = serverName + id_perangkat_a;
      //      String serverName2 = "&status=mati";
      //      String serverPath = serverPathawal + serverName2;
      //      // Your Domain name with URL path or IP address with path
      //      http.begin(serverPath.c_str());
      //      int httpCode = http.GET();
    }

    //    if (result_0["mode"] == "timer" && result_0["status"] == "mati")
    //    {
    //      Serial.println("-----------------MODE Timer pendingin---------------------");
    //      digitalWrite(relay1_pin, LOW);
    //      Serial.println("pendingin dimatikan");
    //      SPendingin = "Off";
    //    }

    if (mode0 == "suhu")
    {
      Serial.println("-----------------MODE SUHU pendingin---------------------");

      if (suhudelay >= suhu_batas_nyala0)
      {
//        digitalWrite(relay1_pin, HIGH);
//        Serial.println("pendingin dinyalakan");
        SPendingin = "On";
      }
      else if (suhudelay <= suhu_batas_mati0)
      {
//        digitalWrite(relay1_pin, LOW);
//        Serial.println("pendingin dimatikan");
        SPendingin = "Off";
      }
    }
    if (mode0 == "kelembapan")
    {
      Serial.println("-----------------MODE KELEMBAPAN pendingin---------------------");

      if (lembapdelay <= kelembapan_batas_nyala0)
      {
//        digitalWrite(relay1_pin, HIGH);
//        Serial.println("pendingin dinyalakan");
        SPendingin = "On";
      }
      else if (lembapdelay >= kelembapan_batas_mati0)
      {
//        digitalWrite(relay1_pin, LOW);
//        Serial.println("pendingin dimatikan");
        SPendingin = "Off";
      }
    }

    if (mode0 == "off")
    {
      Serial.println("-----------------MODE off pendingin---------------------");
      Serial.println("mode Off");
//      digitalWrite(relay1_pin, LOW);
//      Serial.println("pendingin dimatikan");
      SPendingin = "Off";
    }
    if (mode0 == "on"){
      SPendingin = "On";
    }

    //===== PEMANAS =====
    //===================

    if (mode0=="timer" && timerstatus1 == "nyala")
    {
      Serial.println("-----------------MODE Timer pemanas---------------------");

      int timer_2 = jam1 * 60000 ;
      timer_2 = timer_2 + menit1;
      timer_2 = timer_2 * 60000;
      int timer2_2 = timer_2 * 2;

      t_setelah2_2 = millis() - t_awal2_2;
      if (kondisi2_2 == 0)
      {
        if (t_setelah2_2 > timer_2)
        {
          Serial.print("interval nyala");
//          digitalWrite(relay2_pin, HIGH);
//          Serial.println(" | pemanas dinyalakan");
          SPemanas = "On";
          kondisi2_2 = 1;
        }
      }

      if (t_setelah2_2 > timer2_2)
      {
        Serial.print("interval mati");
//        digitalWrite(relay2_pin, LOW);
//        Serial.println(" | pemanas dimatikan");
        SPemanas = "Off";
        t_awal2_2 = millis();
        kondisi2_2 = 0;
      }


      //        String serverName = "http://app.chickin.id/api_manual/api_aktual.php?id=";
      //        String serverPathawal = serverName + id_perangkat_b;
      //        String serverName2 = "&status=mati";s
      //        String serverPath = serverPathawal + serverName2;
      //        // Your Domain name with URL path or IP address with path
      //        http.begin(serverPath.c_str());
      //        int httpCode = http.GET();
      //    }
    }
    //    if (result_1["mode"] == "timer" && result_1["status"] == "mati")
    //    {
    //      Serial.println("-----------------MODE Timer pemanas---------------------");
    //      digitalWrite(relay2_pin, LOW);
    //      Serial.println("pemanas dimatikan");
    //      SPemanas = "Off";
    //    }

    if (mode1 == "suhu")
    {
      Serial.println("-----------------MODE SUHU pemanas---------------------");

      if (suhudelay <= suhu_batas_nyala1)
      {
//        digitalWrite(relay2_pin, HIGH);
//        Serial.println("pemanas dinyalakan");
        SPemanas = "On";
      }
      else if (suhudelay >= suhu_batas_mati1)
      {
//        digitalWrite(relay2_pin, LOW);
//        Serial.println("pemanas dimatikan");
        SPemanas = "Off";
      }
    }

    if (mode1 == "kelembapan")
    {
      Serial.println("-----------------MODE KELEMBAPAN pemanas---------------------");

      if (lembapdelay >= kelembapan_batas_nyala1)
      {
//        digitalWrite(relay2_pin, HIGH);
//        Serial.println("pemanas dinyalakan");
        SPemanas = "On";
      }
      else if (lembapdelay <= kelembapan_batas_mati1)
      {
//        digitalWrite(relay2_pin, LOW);
//        Serial.println("pemanas dimatikan");
        SPemanas = "Off";
      }
    }

    if (mode1 == "off")
    {
      Serial.println("-----------------MODE KELEMBAPAN pemanas---------------------");
      Serial.println("mode Off");
      digitalWrite(relay2_pin, LOW);
      Serial.println("pemanas dimatikan");
      SPemanas = "Off";
    }
    if (mode1 == "on"){
      SPemanas = "On";
    }

    //===== BLOWER =====
    //==================

    if (mode2 == "timer" && timerstatus2== "nyala")
    {
      Serial.println("-----------------MODE Timer Blower---------------------");


      int timer_3 = jam2 * 60000 ;
      timer_3 = timer_3 + menit2;
      timer_3 = timer_3 * 60000;
      int timer2_3 = timer_3 * 2;

      t_setelah2_3 = millis() - t_awal2_3;
      if (kondisi2_3 == 0)
      {
        if (t_setelah2_3 > timer_3)
        {
          Serial.print("interval nyala");
//          digitalWrite(relay3_pin, HIGH);
//          Serial.println(" | Blower dinyalakan");
          SBlower = "On";
          kondisi2_3 = 1;
        }
      }

      if (t_setelah2_3 > timer2_3)
      {
//        digitalWrite(relay3_pin, LOW);
//        Serial.println("Blower dimatikan");
        SBlower = "Off";
        t_awal2_3 = millis();
        kondisi2_3 = 0;
      }

      //        String serverName = "http://app.chickin.id/api_manual/api_aktual.php?id=";
      //        String serverPathawal = serverName + id_perangkat_c;
      //        String serverName2 = "&status=mati";
      //        String serverPath = serverPathawal + serverName2;
      //        // Your Domain name with URL path or IP address with path
      //        http.begin(serverPath.c_str());
      //        int httpCode = http.GET();

    }
    //      if (result_2["mode"] == "timer" && result_2["status"] == "mati")
    //      {
    //        Serial.println("-----------------MODE Timer Blower---------------------");
    //        digitalWrite(relay3_pin, LOW);
    //        Serial.println("Blower dimatikan");
    //        SBlower = "Off";
    //      }

    if (mode2 == "suhu")
    {
      Serial.println("-----------------MODE SUHU Blower---------------------");

      if (suhudelay >= suhu_batas_nyala2)
      {
//        digitalWrite(relay3_pin, HIGH);
//        Serial.println("Blower dinyalakan");
        SBlower = "On";
      }
      else if (suhudelay <= suhu_batas_mati2)
      {
//        digitalWrite(relay3_pin, LOW);
//        Serial.println("Blower dimatikan");
        SBlower = "Off";
      }
    }

    if (mode2 == "kelembapan")
    {
      Serial.println("-----------------MODE KELEMBAPAN Blower---------------------");

      if (lembapdelay >= kelembapan_batas_nyala2)
      {
//        digitalWrite(relay3_pin, HIGH);
//        Serial.println("Blower dinyalakan");
        SBlower = "On";
      }
      else if (lembapdelay <= kelembapan_batas_mati2)
      {
//        digitalWrite(relay3_pin, LOW);
//        Serial.println("Blower dimatikan");
        SBlower = "Off";
      }
    }

    if (mode2 == "off")
    {
      Serial.println("-----------------MODE KELEMBAPAN Blower---------------------");
      Serial.println("mode Off");
      //digitalWrite(relay3_pin, LOW);
      Serial.println("Blower dimatikan");
    }
    if (mode2 == "on"){
      SBlower = "On";
    }
    digitalWrite(led_indikator_pin, HIGH);
    delay(100);
    digitalWrite(led_indikator_pin, LOW);
  }
  else
  {//--------not connect
    koneksi = "X";
    Serial.println("Error in response");
    digitalWrite(led_indikator_pin, LOW);
    delay(1000);
    
   /* cek++;
    Serial.print("cek full manual : ");
    Serial.println(cek);
    if (cek == 50)
    {
      digitalWrite(relay1_pin, LOW);
      digitalWrite(relay2_pin, LOW);
      digitalWrite(relay3_pin, LOW);
      cek = 0;
      
      
    }*/
  }
  
   http.end(); //Close connection
   }

String serverName11 = "http://app.chickin.id/api_manual/api_aktual_update.php?id_device="+String(GET_ID_Device);
    String serverPath11 = serverName11;
    String serverName12 = "&mode=";
    String serverPath12 = serverPath11 + serverName12 + mode0;
//    String serverName13 = "&status=";
//    String serverPath13 = serverPath12 + serverName13 + SPendingin;
    String serverName14 = "&suhunyala=";
    String serverPath14 = serverPath12 + serverName14 + suhu_batas_mati0;
    String serverName15 = "&suhumati=";
    String serverPath15 = serverPath14 + serverName15 + suhu_batas_nyala0;
    String serverName16 = "&lembapnyala=";
    String serverPath16 = serverPath15 + serverName16 + kelembapan_batas_nyala0;
    String serverName17 = "&lembapmati=";
    String serverPath17 = serverPath16 + serverName17 + kelembapan_batas_mati0;
    String serverName18 = "&jammati=";
    String serverPath18 = serverPath17 + serverName18 + jam0;
    String serverName19 = "&menitmati=";
    String serverPath19 = serverPath18 + serverName19 + menit0;
    String serverName20 = "&speed=0";
    String serverPath20 = serverPath19 + serverName20 ;
    String serverName21 = "&nama=Pendingin";
    String serverPath21 = serverPath20 + serverName21;
    Serial.print("update :");
    Serial.println(serverPath21);
    // Your Domain name with URL path or IP address with path
    http.begin(wificlient, serverPath21.c_str());
    int httpCode10 = http.GET();

    String serverName31 = "http://app.chickin.id/api_manual/api_aktual_update.php?id_device="+String(GET_ID_Device);
    String serverPath31 = serverName31;
    String serverName32 = "&mode=";
    String serverPath32 = serverPath31 + serverName32 + mode1;
//    String serverName33 = "&status=";
//    String serverPath33 = serverPath32 + serverName33 + SPemanas;
    String serverName34 = "&suhunyala=";
    String serverPath34 = serverPath32 + serverName34 + suhu_batas_mati1;
    String serverName35 = "&suhumati=";
    String serverPath35 = serverPath34 + serverName35 + suhu_batas_nyala1;
    String serverName36 = "&lembapnyala=";
    String serverPath36 = serverPath35 + serverName36 + kelembapan_batas_nyala1;
    String serverName37 = "&lembapmati=";
    String serverPath37 = serverPath36 + serverName37 + kelembapan_batas_mati1;
    String serverName38 = "&jammati=";
    String serverPath38 = serverPath37 + serverName38 + jam1;
    String serverName39 = "&menitmati=";
    String serverPath39 = serverPath38 + serverName39 + menit1;
    String serverName40 = "&speed=0";
    String serverPath40 = serverPath39 + serverName40;
    String serverName41 = "&nama=Pemanas";
    String serverPath41 = serverPath40 + serverName41;
    Serial.print("update2 :");
    Serial.println(serverPath41);
    // Your Domain name with URL path or IP address with path
    http.begin(wificlient, serverPath41.c_str());
    int httpCode20 = http.GET();

    String serverName51 = "http://app.chickin.id/api_manual/api_aktual_update.php?id_device="+String(GET_ID_Device);
    String serverPath51 = serverName51;
    String serverName52 = "&mode=";
    String serverPath52 = serverPath51 + serverName52 + mode2;
//    String serverName53 = "&status=";
//    String serverPath53 = serverPath52 + serverName53 + SBlower;
    String serverName54 = "&suhunyala=";
    String serverPath54 = serverPath52 + serverName54 + suhu_batas_mati2;
    String serverName55 = "&suhumati=";
    String serverPath55 = serverPath54 + serverName55 + suhu_batas_nyala2;
    String serverName56 = "&lembapnyala=";
    String serverPath56 = serverPath55 + serverName56 + kelembapan_batas_nyala2;
    String serverName57 = "&lembapmati=";
    String serverPath57 = serverPath56 + serverName57 + kelembapan_batas_mati2;
    String serverName58 = "&jammati=";
    String serverPath58 = serverPath57 + serverName58 + jam2;
    String serverName59 = "&menitmati=";
    String serverPath59 = serverPath58 + serverName59 + menit2;
    String serverName60 = "&speed=";
    String serverPath60 = serverPath59 + serverName60 + SetSpeed;
    String serverName61 = "&nama=Blower";
    String serverPath61 = serverPath60 + serverName61;
    Serial.print("update2 :");
    Serial.println(serverPath61);
    // Your Domain name with URL path or IP address with path
    http.begin(wificlient, serverPath61.c_str());
    int httpCode30 = http.GET();
    
   
  }
  else if (ModePanel==0){
    
    koneksi = "X";
   // delay(500);
    cek=0;
    //////=======================================================mode offline
     Serial.print("Mode0 :");
    Serial.println(mode0);
        Serial.print("Mode1 :");
    Serial.println(mode1);
        Serial.print("Mode2 :");
    Serial.println(mode2);
    //===== PENDINGIN =====
    //=====================

    if (mode0 == "timer" && timerstatus0 == "nyala")
    {
      Serial.println("-----------------MODE Timer pendingin---------------------");

      int timer = jam0 * 60000 ;
      timer = timer + menit0;
      timer = timer * 60000;
      int timer2 = timer * 2;

      t_setelah2_1 = millis() - t_awal2_1;
      if (kondisi2_1 == 0)
      {
        if (t_setelah2_1 > timer)
        {
          Serial.print("interval nyala");
//          digitalWrite(relay1_pin, HIGH);
//          Serial.println(" | pendingin dinyalakan");
          SPendingin = "On";
          kondisi2_1 = 1;
        }
      }

      if (t_setelah2_1 > timer2)
      {
        Serial.print("interval mati");
//        digitalWrite(relay1_pin, LOW);
//        Serial.println(" | pendingin dimatikan");
        SPendingin = "Off";
        t_awal2_1 = millis();
        kondisi2_1 = 0;
      }

      //      String serverName = "http://app.chickin.id/api_manual/api_aktual.php?id=";
      //      String serverPathawal = serverName + id_perangkat_a;
      //      String serverName2 = "&status=mati";
      //      String serverPath = serverPathawal + serverName2;
      //      // Your Domain name with URL path or IP address with path
      //      http.begin(serverPath.c_str());
      //      int httpCode = http.GET();
    }

    //    if (result_0["mode"] == "timer" && result_0["status"] == "mati")
    //    {
    //      Serial.println("-----------------MODE Timer pendingin---------------------");
    //      digitalWrite(relay1_pin, LOW);
    //      Serial.println("pendingin dimatikan");
    //      SPendingin = "Off";
    //    }

    if (mode0 == "suhu")
    {
      Serial.println("-----------------MODE SUHU pendingin---------------------");

      if (suhudelay >= suhu_batas_nyala0)
      {
//        digitalWrite(relay1_pin, HIGH);
//        Serial.println("pendingin dinyalakan");
        SPendingin = "On";
      }
      else if (suhudelay <= suhu_batas_mati0)
      {
//        digitalWrite(relay1_pin, LOW);
//        Serial.println("pendingin dimatikan");
        SPendingin = "Off";
      }
    }
    if (mode0 == "kelembapan")
    {
      Serial.println("-----------------MODE KELEMBAPAN pendingin---------------------");

      if (lembapdelay <= kelembapan_batas_nyala0)
      {
//        digitalWrite(relay1_pin, HIGH);
//        Serial.println("pendingin dinyalakan");
        SPendingin = "On";
      }
      else if (lembapdelay >= kelembapan_batas_mati0)
      {
//        digitalWrite(relay1_pin, LOW);
//        Serial.println("pendingin dimatikan");
        SPendingin = "Off";
      }
    }

    if (mode0 == "Off")
    {
      Serial.println("-----------------MODE off pendingin---------------------");
      Serial.println("mode Off");
//      digitalWrite(relay1_pin, LOW);
//      Serial.println("pendingin dimatikan");
      SPendingin = "Off";
    }
    if (mode0 == "on"){
      SPendingin = "On";
    }

    //===== PEMANAS =====
    //===================

    if (mode0=="timer" && timerstatus1 == "nyala")
    {
      Serial.println("-----------------MODE Timer pemanas---------------------");

      int timer_2 = jam1 * 60000 ;
      timer_2 = timer_2 + menit1;
      timer_2 = timer_2 * 60000;
      int timer2_2 = timer_2 * 2;

      t_setelah2_2 = millis() - t_awal2_2;
      if (kondisi2_2 == 0)
      {
        if (t_setelah2_2 > timer_2)
        {
          Serial.print("interval nyala");
//          digitalWrite(relay2_pin, HIGH);
//          Serial.println(" | pemanas dinyalakan");
          SPemanas = "On";
          kondisi2_2 = 1;
        }
      }

      if (t_setelah2_2 > timer2_2)
      {
        Serial.print("interval mati");
//        digitalWrite(relay2_pin, LOW);
//        Serial.println(" | pemanas dimatikan");
        SPemanas = "Off";
        t_awal2_2 = millis();
        kondisi2_2 = 0;
      }


      //        String serverName = "http://app.chickin.id/api_manual/api_aktual.php?id=";
      //        String serverPathawal = serverName + id_perangkat_b;
      //        String serverName2 = "&status=mati";s
      //        String serverPath = serverPathawal + serverName2;
      //        // Your Domain name with URL path or IP address with path
      //        http.begin(serverPath.c_str());
      //        int httpCode = http.GET();
      //    }
    }
    //    if (result_1["mode"] == "timer" && result_1["status"] == "mati")
    //    {
    //      Serial.println("-----------------MODE Timer pemanas---------------------");
    //      digitalWrite(relay2_pin, LOW);
    //      Serial.println("pemanas dimatikan");
    //      SPemanas = "Off";
    //    }

    if (mode1 == "suhu")
    {
      Serial.println("-----------------MODE SUHU pemanas---------------------");

      if (suhudelay <= suhu_batas_nyala1)
      {
//        digitalWrite(relay2_pin, HIGH);
//        Serial.println("pemanas dinyalakan");
        SPemanas = "On";
      }
      else if (suhudelay >= suhu_batas_mati1)
      {
//        digitalWrite(relay2_pin, LOW);
//        Serial.println("pemanas dimatikan");
        SPemanas = "Off";
      }
    }

    if (mode1 == "kelembapan")
    {
      Serial.println("-----------------MODE KELEMBAPAN pemanas---------------------");

      if (lembapdelay >= kelembapan_batas_nyala1)
      {
//        digitalWrite(relay2_pin, HIGH);
//        Serial.println("pemanas dinyalakan");
        SPemanas = "On";
      }
      else if (lembapdelay <= kelembapan_batas_mati1)
      {
//        digitalWrite(relay2_pin, LOW);
//        Serial.println("pemanas dimatikan");
        SPemanas = "Off";
      }
    }

    if (mode1 == "Off")
    {
      Serial.println("-----------------MODE KELEMBAPAN pemanas---------------------");
      Serial.println("mode Off");
      digitalWrite(relay2_pin, LOW);
      Serial.println("pemanas dimatikan");
      SPemanas = "Off";
    }
    if (mode1 == "on"){
      SPemanas = "On";
    }

    //===== BLOWER =====
    //==================

    if (mode2 == "timer" && timerstatus2== "nyala")
    {
      Serial.println("-----------------MODE Timer Blower---------------------");


      int timer_3 = jam2 * 60000 ;
      timer_3 = timer_3 + menit2;
      timer_3 = timer_3 * 60000;
      int timer2_3 = timer_3 * 2;

      t_setelah2_3 = millis() - t_awal2_3;
      if (kondisi2_3 == 0)
      {
        if (t_setelah2_3 > timer_3)
        {
          Serial.print("interval nyala");
//          digitalWrite(relay3_pin, HIGH);
//          Serial.println(" | Blower dinyalakan");
          SBlower = "On";
          kondisi2_3 = 1;
        }
      }

      if (t_setelah2_3 > timer2_3)
      {
//        digitalWrite(relay3_pin, LOW);
//        Serial.println("Blower dimatikan");
        SBlower = "Off";
        t_awal2_3 = millis();
        kondisi2_3 = 0;
      }

      //        String serverName = "http://app.chickin.id/api_manual/api_aktual.php?id=";
      //        String serverPathawal = serverName + id_perangkat_c;
      //        String serverName2 = "&status=mati";
      //        String serverPath = serverPathawal + serverName2;
      //        // Your Domain name with URL path or IP address with path
      //        http.begin(serverPath.c_str());
      //        int httpCode = http.GET();

    }
    //      if (result_2["mode"] == "timer" && result_2["status"] == "mati")
    //      {
    //        Serial.println("-----------------MODE Timer Blower---------------------");
    //        digitalWrite(relay3_pin, LOW);
    //        Serial.println("Blower dimatikan");
    //        SBlower = "Off";
    //      }

    if (mode2 == "suhu")
    {
      Serial.println("-----------------MODE SUHU Blower---------------------");

      if (suhudelay >= suhu_batas_nyala2)
      {
//        digitalWrite(relay3_pin, HIGH);
//        Serial.println("Blower dinyalakan");
        SBlower = "On";
      }
      else if (suhudelay <= suhu_batas_mati2)
      {
//        digitalWrite(relay3_pin, LOW);
//        Serial.println("Blower dimatikan");
        SBlower = "Off";
      }
    }

    if (mode2 == "kelembapan")
    {
      Serial.println("-----------------MODE KELEMBAPAN Blower---------------------");

      if (lembapdelay >= kelembapan_batas_nyala2)
      {
//        digitalWrite(relay3_pin, HIGH);
//        Serial.println("Blower dinyalakan");
        SBlower = "On";
      }
      else if (lembapdelay <= kelembapan_batas_mati2)
      {
//        digitalWrite(relay3_pin, LOW);
//        Serial.println("Blower dimatikan");
        SBlower = "Off";
      }
    }

    if (mode2 == "Off")
    {
      Serial.println("-----------------MODE KELEMBAPAN Blower---------------------");
      Serial.println("mode Off");
      //digitalWrite(relay3_pin, LOW);
      Serial.println("Blower dimatikan");
    }
    if (mode2 == "on"){
      SBlower = "On";
    }
    //////=======================================================mode offline
  }
    tampil();
     Up=Down=Ok=Cancel=0;
     
    
    
  terminal();
    

  Serial.println("--------------------------------------");
  

}

void terminal(){
  Serial.println("\n============== ");
      Serial.print("Nilai input kecepatan: ");
      Serial.print (SetSpeed);
      Serial.println(" %");
      int pwm = map(SetSpeed, 0, 100, 0, 1020);
      Serial.print("pwm : ");
      Serial.println (pwm);
      float freq = map(SetSpeed, 0, 100, 0, 50);
      Serial.print("frekuensi : ");
      Serial.println (freq, 1);
      Serial.println("============== ");
      
      
if (SBlower == "Off") {
  
  //digitalWrite(relay3_pin, LOW);
  analogWrite(pinInverter, 0);
  Serial.println("Blower dimatikan");
}
if (SBlower == "On") {
  analogWrite(pinInverter, pwm);
  //digitalWrite(relay3_pin, HIGH);
  Serial.println("Blower dinyalakan");
  //delay(200);
}
if (SPemanas == "Off") {
  digitalWrite(relay2_pin, LOW);
  Serial.println("pemanas dimatikan");
}
if (SPemanas == "On") {
  digitalWrite(relay2_pin, HIGH);
  Serial.println("pemanas dinyalakan");
}
if (SPendingin == "Off") {
  digitalWrite(relay1_pin, LOW);
  Serial.println("pendingin dimatikan");
}
 if (SPendingin == "On") {
  digitalWrite(relay1_pin, HIGH);
  Serial.println("pendingin dinyalakan");
}
      
}
//gets called when WiFiManager enters configuration mode
/*void configModeCallback(WiFiManager * myWiFiManager)
{


  ticker.attach(0.2, tick);
  Serial.println("Entered config mode");
  lcd.setCursor(1, 1); lcd.print("Entered config mode");
  Serial.println(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  Serial.println(myWiFiManager->getConfigPortalSSID());
  //entered config mode, make led toggle faster
  ticker.attach(0.2, tick);
}*/

void button_reconnect()
{
  digitalWrite(led_indikator_pin, HIGH);
  delay(100);
  digitalWrite(led_indikator_pin, LOW);
  delay(100);
  digitalWrite(led_indikator_pin, HIGH);
  delay(100);
  digitalWrite(led_indikator_pin, LOW);
  delay(100);
  digitalWrite(led_indikator_pin, HIGH);
  delay(2000);
  digitalWrite(led_indikator_pin, LOW);
  delay(100);
  digitalWrite(led_indikator_pin, HIGH);
  delay(100);
  digitalWrite(led_indikator_pin, LOW);
  delay(100);
  digitalWrite(led_indikator_pin, HIGH);
  delay(100);
  digitalWrite(led_indikator_pin, LOW);
  delay(100);
  digitalWrite(led_indikator_pin, HIGH);
  Serial.println("push in/////////////////////// berhasil");
  lcd.setCursor(1, 1); lcd.print("RESET Success");

  ticker.attach(0.2, tick);
  /*WiFi.mode(WIFI_STA);
  WiFiManager wifi;
  wifi.resetSettings();
  wm.resetSettings();
  wifi.setConfigPortalTimeout(300);

  std::vector<const char *> menu = {"wifi", "sep", "restart", "exit"};
  wifi.setMenu(menu);

  wifi.setClass("invert");

  delay(1000);
  wifi.setAPCallback(configModeCallback);
  //reset and try again, or maybe put it to deep sleep
  
*/
  String a = " ";
  a.toCharArray(WIFISSID, 50);
  delay(500);
  TulisEEPROMMemory(0, 50, WIFISSID);
  
  ESP.restart();

  lcd.setCursor(1, 1); lcd.print("Koneksi Berhasil");
  Serial.println("/////////////////////connected...yeey :)");
}

void Sensor()
{

  h = dht.readHumidity();  
  // Read temperature as Celsius (the default)
  t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f))
  {
    Serial.println(F("Failed to read from DHT sensor!"));
    delay(500);
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print(F("Humidity: "));
  Serial.print(h);
  Serial.print(F("%  Temperature: "));
  Serial.print(t);
  Serial.println(F("°C "));
}


void tampil()
{

    String dataT = String(suhudelay);
    String dataH = String(lembapdelay);
    
  char key = kpd.getKey();
  
  if (key) {  
    Serial.println(key);  
    if(key=='A') Up=1;
    if(key=='B') Down=1;
    if(key=='C') Ok=1;
    if(key=='D') Cancel=1; 
  }

//  lcd.setCursor(2, 3);
//  lcd.print("input : ");
///// ------------------------------------------------------------------------------------------------------------------tombol back
  if (Cancel==0 && lockCancel==0){ lcd.clear(); lockCancel=1;} 
  if (Cancel!=0 && lockCancel==1 && sub==0){lcd.clear(); lockCancel=0; dis=1;}
  if (Cancel!=0 && lockCancel==1 && (dis==1 || dis==2) && sub>0){lcd.clear(); lockCancel=0; sub=0; endsub=0;} 
  if (Cancel!=0 && lockCancel==1 && (dis==1 || dis==2) && sub==0){lcd.clear(); lockCancel=0; dis=1; sub=0; endsub=0;}
  if (Cancel!=0 && lockCancel==1 && dis==3 && sub==1 && endsub==0){lcd.clear(); lockCancel=0; dis=3; sub=0; }
  if (Cancel!=0 && lockCancel==1 && dis==3 && sub==1 && endsub>0){lcd.clear(); lockCancel=0; dis=3; sub==1; endsub=0; BatasT=""; BatasH="";}
  if (Cancel!=0 && lockCancel==1 && dis==3 && sub==2 && endsub==0){lcd.clear(); lockCancel=0; dis=3; sub=0; }
  if (Cancel!=0 && lockCancel==1 && dis==3 && sub==2 && endsub>0){lcd.clear(); lockCancel=0; dis=3; sub==2; endsub=0; BatasT=""; BatasH="";}
  if (Cancel!=0 && lockCancel==1 && dis==3 && sub==3 && endsub==0){lcd.clear(); lockCancel=0; dis=3; sub=0; }
  if (Cancel!=0 && lockCancel==1 && dis==3 && sub==3 && endsub>0){lcd.clear(); lockCancel=0; dis=3; sub==3; endsub=0; BatasT=""; BatasH="";}
///// ------------------------------------------------------------------------------------------------------------------tombol back  
  if (dis==1)
  {
      // tampilan awal// (maks string 6 character)
    lcd.setCursor(1, 1); lcd.print("Control");
    lcd.setCursor(1, 2); lcd.print("Setting");
    lcd.setCursor(1, 3); lcd.print("Status");
    lcd.setCursor(0, 0); lcd.print("-# " + String(nama) + " #-");

    //lcd.setCursor(9, 0); lcd.print("|");
    lcd.setCursor(8, 1); lcd.print("|");
    lcd.setCursor(8, 2); lcd.print("|");
    lcd.setCursor(8, 3); lcd.print("|");

    lcd.setCursor(10, 1); lcd.print("Suhu: " + dataT + (char)223 + "C");
    lcd.setCursor(10, 2); lcd.print("Lemb: " + dataH + "%");
    lcd.setCursor(10, 3); lcd.print("Conn: " + koneksi);
      
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
       switch (mode)
      {
        case 0 : lcd.setCursor(0,1); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(8,1); lcd.print(">"); break;
      }  
      if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; dis=2; lcd.clear(); } // pilih menu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; dis=3; lcd.clear(); } // pilih menu 2
      if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; dis=4; lcd.clear(); } // pilih menu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; dis=5; lcd.clear(); } // pilih menu 4
      

      if (mode>2 || mode<0) mode=0;
     
  }
  if (dis==2)//menu 1
  {
    if (sub==0)
    {

      lcd.setCursor(0,0); lcd.print("==Menu Control");
      lcd.setCursor(1,1); lcd.print("Pendingin -> " + SPendingin );
      lcd.setCursor(1,2); lcd.print("Pemanas   -> " + SPemanas ); 
      lcd.setCursor(1,3); lcd.print("Blower    -> " + SBlower ); 
      

      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear(); sub=1; } // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear(); sub=2; } // Isi submenu 2
      if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear(); sub=3; } // Isi submenu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4

      if (mode>2 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(0,1); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
      }  
    }
  }
    if(sub==1){
      
      lcd.setCursor(0,0); lcd.print("==Control Pendingin" );
      lcd.setCursor(0,1); lcd.print("Status aktual: [" + SPendingin + "]" ); 
      lcd.setCursor(4,2); lcd.print("On");
      lcd.setCursor(4,3); lcd.print("Off");       
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  SPendingin="On"; mode0="on"; sub=0; Serial.println("pendingin dinyalakan dari panel");} // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  SPendingin="Off"; mode0="Off"; sub=0; Serial.println("pendingin dimatikan dari panel");} // Isi submenu 2
      //if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear(); endsub=3; } // Isi submenu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4

      if (mode>1 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(3,2); lcd.print(">"); break;
        case 1 : lcd.setCursor(3,3); lcd.print(">"); break;
        //case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
      }  
    }
    }
       if(sub==2){
      
      lcd.setCursor(0,0); lcd.print("==Control Pemanas" );
      lcd.setCursor(0,1); lcd.print("Status aktual: [" + SPemanas + "]" ); 
      lcd.setCursor(4,2); lcd.print("On");
      lcd.setCursor(4,3); lcd.print("Off");       
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  SPemanas="On"; mode1="on" ;sub=0; Serial.println("Pemanas dinyalakan dari panel");} // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  SPemanas="Off";mode1="Off";sub=0; Serial.println("Pemanas dimatikan dari panel");} // Isi submenu 2
      //if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear(); endsub=3; } // Isi submenu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4

      if (mode>1 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(3,2); lcd.print(">"); break;
        case 1 : lcd.setCursor(3,3); lcd.print(">"); break;
        //case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
      }  
    }
    }
        if(sub==3){
      
      lcd.setCursor(0,0); lcd.print("==Control Blower" );
      lcd.setCursor(0,1); lcd.print("Status aktual: [" + SBlower + "]" ); 
      lcd.setCursor(4,2); lcd.print("On");
      lcd.setCursor(4,3); lcd.print("Off");       
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  SBlower="On"; mode2="on"; sub=0; Serial.println("Blower dinyalakan dari panel");} // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  SBlower="Off"; mode2="Off"; sub=0; Serial.println("Blower dimatikan dari panel");} // Isi submenu 2
      //if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear(); endsub=3; } // Isi submenu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4

      if (mode>1 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(3,2); lcd.print(">"); break;
        case 1 : lcd.setCursor(3,3); lcd.print(">"); break;
        //case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
      }  
    }
    }

  }
  
  if (dis==3)
  {
     if (sub==0)
    {

      lcd.setCursor(0,0); lcd.print("==Menu Setting");
      lcd.setCursor(1,1); lcd.print("Pendingin ");
      lcd.setCursor(1,2); lcd.print("Pemanas   "); 
      lcd.setCursor(1,3); lcd.print("Blower    "); 
      

      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear(); sub=1; } // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear(); sub=2; } // Isi submenu 2
      if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear(); sub=3; } // Isi submenu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4

      if (mode>2 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(0,1); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
      }  
    }
  }
  if(sub==1){
    if(endsub==0){  
      setpoin=1;//untul looping
      lcd.setCursor(0,0); lcd.print("Setting Pendingin");//----------------------------------------------------------------PENDINGIN
      lcd.setCursor(1,2); lcd.print("Mode");
      lcd.setCursor(1,3); lcd.print("Temp"); 
      lcd.setCursor(9,2); lcd.print("Humidity"); 
      lcd.setCursor(9,3); lcd.print("Timer");   
      
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  endsub=1;} // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  endsub=2;} // Isi submenu 2
      if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear(); endsub=3; } // Isi submenu 3
      if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); endsub=4;  SetTimer="";} // Isi submenu 4

      if (mode>3 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,3); lcd.print(">"); break;
        case 2 : lcd.setCursor(8,2); lcd.print(">"); break;
        case 3 : lcd.setCursor(8,3); lcd.print(">"); break;
      }  
    }
    }
    if(endsub==1){

      lcd.setCursor(0,0); lcd.print("Mode Aktif: " +  modePendingin);
      lcd.setCursor(1,1); lcd.print("1.Suhu ");
      lcd.setCursor(1,2); lcd.print("2.Kelembapan "); 
      lcd.setCursor(1,3); lcd.print("3.Timer "); 
      
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  modePendingin="Suhu"; sub=1; endsub=0; mode0="suhu"; timerstatus0 = "mati";} // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  modePendingin="K.lembap"; sub=1; endsub=0; mode0="kelembapan"; timerstatus0 = "mati";} // Isi submenu 2
      if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear();  modePendingin="Timer"; sub=1; endsub=0; mode0="timer"; timerstatus0 = "nyala";} // Isi submenu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4

      if (mode>2 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(0,1); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
      }  
    }
    }
      if(endsub==2 && BatasT==""){
      setpoin=1;//untul looping
      lcd.setCursor(0,0); lcd.print("==Set Temp");
      lcd.setCursor(0,1); lcd.print("Aktual : " + dataT + (char)223 + "C");
      lcd.setCursor(1,2); lcd.print("Batas Nyala"); 
      lcd.setCursor(1,3); lcd.print("Batas Mati"); 
      
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  BatasT="Nyala"; } // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  BatasT="Mati"; } // Isi submenu 2
      //if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear();  Mode="Timer"; } // Isi submenu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4

      if (mode>1 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
      }  
    }
    }
      else if(endsub==3 && BatasH==""){
      setpoin=1;//untul looping
      lcd.setCursor(0,0); lcd.print("==Set Humudity");
      lcd.setCursor(0,1); lcd.print("Aktual : " + dataH + "%");
      lcd.setCursor(1,2); lcd.print("Batas Nyala"); 
      lcd.setCursor(1,3); lcd.print("Batas Mati"); 
      
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  BatasH="Nyala";} // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  BatasH="Mati"; } // Isi submenu 2
      //if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear();  Mode="Timer"; } // Isi submenu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4

      if (mode>1 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
      }  
    }
    }
       else if(endsub==4 && SetTimer==""){
      setpoin=1;//untul looping
      lcd.setCursor(0,0); lcd.print("==Set Timer");
      lcd.setCursor(0,1); lcd.print("Aktual : " + String(jam0) + " jam," + String(menit0)+ " mnt");
      lcd.setCursor(1,2); lcd.print("by Jam"); 
      lcd.setCursor(1,3); lcd.print("by menit"); 
      
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  SetTimer="Jam";} // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  SetTimer="Menit"; } // Isi submenu 2
      //if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear();  Mode="Timer"; } // Isi submenu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4

      if (mode>1 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
      }  
    }
    }
    
  }
  else if(sub==2){
      
     if(endsub==0){  
      setpoin=1;//untul looping
      lcd.setCursor(0,0); lcd.print("Setting Pemanas");//---------------------------------------------------------------------------------PEMANAS
      lcd.setCursor(1,2); lcd.print("Mode");
      lcd.setCursor(1,3); lcd.print("Temp"); 
      lcd.setCursor(9,2); lcd.print("Humidity"); 
      lcd.setCursor(9,3); lcd.print("Timer");   
      
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  endsub=1;} // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  endsub=2;} // Isi submenu 2
      if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear(); endsub=3; } // Isi submenu 3
      if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); endsub=4;  SetTimer="";} // Isi submenu 4

      if (mode>3 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,3); lcd.print(">"); break;
        case 2 : lcd.setCursor(8,2); lcd.print(">"); break;
        case 3 : lcd.setCursor(8,3); lcd.print(">"); break;
      }  
    }
    }
    if(endsub==1){

      lcd.setCursor(0,0); lcd.print("Mode Aktif: " +  modePemanas);
      lcd.setCursor(1,1); lcd.print("1.Suhu ");
      lcd.setCursor(1,2); lcd.print("2.Kelembapan "); 
      lcd.setCursor(1,3); lcd.print("3.Timer "); 
      
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  modePemanas="Suhu"; sub=2; endsub=0; mode1="suhu"; timerstatus1 = "mati";} // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  modePemanas="K.lembap"; sub=2; endsub=0; mode1="kelembapan"; timerstatus1 = "mati";} // Isi submenu 2
      if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear();  modePemanas="Timer"; sub=2; endsub=0; mode1="timer"; timerstatus1 = "nyala";} // Isi submenu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4

      if (mode>2 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(0,1); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
      }  
    }
    }
      if(endsub==2 && BatasT==""){
      setpoin=1;//untul looping
      lcd.setCursor(0,0); lcd.print("==Set Temp");
      lcd.setCursor(0,1); lcd.print("Aktual : " + dataT + (char)223 + "C");
      lcd.setCursor(1,2); lcd.print("Batas Nyala"); 
      lcd.setCursor(1,3); lcd.print("Batas Mati"); 
      
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  BatasT="Nyala"; } // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  BatasT="Mati"; } // Isi submenu 2
      //if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear();  Mode="Timer"; } // Isi submenu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4

      if (mode>1 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
      }  
    }
    }
      else if(endsub==3 && BatasH==""){
      setpoin=1;//untul looping
      lcd.setCursor(0,0); lcd.print("==Set Humudity");
      lcd.setCursor(0,1); lcd.print("Aktual : " + dataH + "%");
      lcd.setCursor(1,2); lcd.print("Batas Nyala"); 
      lcd.setCursor(1,3); lcd.print("Batas Mati"); 
      
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  BatasH="Nyala";} // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  BatasH="Mati"; } // Isi submenu 2
      //if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear();  Mode="Timer"; } // Isi submenu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4

      if (mode>1 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
      }  
    }
    }
       else if(endsub==4 && SetTimer==""){
      setpoin=1;//untul looping
      lcd.setCursor(0,0); lcd.print("==Set Timer");
      lcd.setCursor(0,1); lcd.print("Aktual : " + String(jam1) + " jam," + String(menit1)+ " mnt");
      lcd.setCursor(1,2); lcd.print("by Jam"); 
      lcd.setCursor(1,3); lcd.print("by menit"); 
      
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  SetTimer="Jam";} // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  SetTimer="Menit"; } // Isi submenu 2
      //if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear();  Mode="Timer"; } // Isi submenu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4

      if (mode>1 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
      }  
    }
    }
    }
    else if(sub==3){
     if(endsub==0 ){  
      setpoin=1;//untul looping
      lcd.setCursor(0,0); lcd.print("Setting Blower");//---------------------------------------------------------------------------------BLOWER 
      lcd.setCursor(1,1); lcd.print("Mode");
      lcd.setCursor(1,2); lcd.print("Temp"); 
      lcd.setCursor(9,1); lcd.print("Humidity"); 
      lcd.setCursor(9,2); lcd.print("Timer");  
      lcd.setCursor(9,3); lcd.print("Speed"); 
      
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  endsub=1;} // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  endsub=2;} // Isi submenu 2
      if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear(); endsub=3; } // Isi submenu 3
      if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); endsub=4;  SetTimer="";} // Isi submenu 4
      if (Ok!=0 && lockOk==1 && mode==4){lockOk=0; lcd.clear(); endsub=5;  ;} // Isi submenu 4

      if (mode>4 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(0,1); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 2 : lcd.setCursor(8,1); lcd.print(">"); break;
        case 3 : lcd.setCursor(8,2); lcd.print(">"); break;
        case 4 : lcd.setCursor(8,3); lcd.print(">"); break;
      }  
    }
    }
    if(endsub==1){

      lcd.setCursor(0,0); lcd.print("Mode Aktif: " +  modeBlower);
      lcd.setCursor(1,1); lcd.print("1.Suhu ");
      lcd.setCursor(1,2); lcd.print("2.Kelembapan "); 
      lcd.setCursor(1,3); lcd.print("3.Timer "); 
      
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  modeBlower="Suhu"; sub=3; endsub=0; mode2="suhu"; timerstatus2 = "mati";} // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  modeBlower="K.lembap"; sub=3; endsub=0; mode2="kelembapan"; timerstatus2 = "mati";} // Isi submenu 2
      if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear();  modeBlower="Timer"; sub=3; endsub=0; mode2="timer"; timerstatus2 = "nyala";} // Isi submenu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4

      if (mode>2 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(0,1); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
      }  
    }
    }
      if(endsub==2 && BatasT==""){
      setpoin=1;//untul looping
      lcd.setCursor(0,0); lcd.print("==Set Temp");
      lcd.setCursor(0,1); lcd.print("Aktual : " + dataT + (char)223 + "C");
      lcd.setCursor(1,2); lcd.print("Batas Nyala"); 
      lcd.setCursor(1,3); lcd.print("Batas Mati"); 
      
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  BatasT="Nyala"; } // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  BatasT="Mati"; } // Isi submenu 2
      //if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear();  Mode="Timer"; } // Isi submenu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4

      if (mode>1 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
      }  
    }
    }
      else if(endsub==3 && BatasH==""){
      setpoin=1;//untul looping
      lcd.setCursor(0,0); lcd.print("==Set Humudity");
      lcd.setCursor(0,1); lcd.print("Aktual : " + dataH + "%");
      lcd.setCursor(1,2); lcd.print("Batas Nyala"); 
      lcd.setCursor(1,3); lcd.print("Batas Mati"); 
      
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  BatasH="Nyala";} // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  BatasH="Mati"; } // Isi submenu 2
      //if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear();  Mode="Timer"; } // Isi submenu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4

      if (mode>1 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
      }  
    }
    }
       else if(endsub==4 && SetTimer==""){
      setpoin=1;//untul looping
      lcd.setCursor(0,0); lcd.print("==Set Timer");
      lcd.setCursor(0,1); lcd.print("Aktual : " + String(jam2) + " jam," + String(menit2)+ " mnt");
      lcd.setCursor(1,2); lcd.print("by Jam"); 
      lcd.setCursor(1,3); lcd.print("by menit"); 
      
      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
      
      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
      
      if (Ok==0 && lockOk==0){lockOk=1; mode=0;lcd.clear();}
      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear();  SetTimer="Jam";} // Isi submenu 1
      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear();  SetTimer="Menit"; } // Isi submenu 2
      //if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear();  Mode="Timer"; } // Isi submenu 3
      //if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4

      if (mode>1 || mode<0) mode=0;{
      switch(mode){
        case 0 : lcd.setCursor(0,2); lcd.print(">"); break;
        case 1 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 2 : lcd.setCursor(0,3); lcd.print(">"); break;
        //case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
      }  
    }
    }
    else if (endsub==5){
    lcd.setCursor(0,0); lcd.print("Set Motor Speed" );
    lcd.setCursor(0,1); lcd.print("Saat ini: " +String(SetSpeed) + " %");
    if(key){lcd.clear();input += key;}
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,2); lcd.print(input + " %" );
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear(); endsub=0; SetSpeed=input.toInt();

    } // Isi submenu 1
  }
     
    }
  }
    
  if (dis==4)
  {
    if (sub==0)
    {
      lcd.setCursor(0, 0); lcd.print("==Status Aktual");
      lcd.setCursor(0,1); lcd.print("P.dgn :" + SPendingin );
      lcd.setCursor(0,2); lcd.print("P.mns :" + SPemanas ); 
      lcd.setCursor(0,3); lcd.print("Blower:" + SBlower );
    
    //lcd.setCursor(9, 0); lcd.print("|");
    lcd.setCursor(10, 1); lcd.print("|");
    lcd.setCursor(10, 2); lcd.print("|");
    lcd.setCursor(10, 3); lcd.print("|");

//    String dataT = String(suhudelay);
//    String dataH = String(lembapdelay);
    lcd.setCursor(11, 1); lcd.print("Suhu:" + dataT + (char)223 + "C");
    lcd.setCursor(11, 2); lcd.print("Lemb:" + dataH + "%");
    lcd.setCursor(11, 3); lcd.print("Conn:" + koneksi);
    
//      if (Down==0 && lockDown==0){lockDown=1;}  //turunkan Cursor ">"
//      if (Down!=0 && lockDown==1){lcd.clear(); lockDown=0; mode++;} 
//      
//      if (Up==0 && lockUp==0){lockUp=1;}   //naikkan Cursor ">"
//      if (Up!=0 && lockUp==1){lcd.clear(); lockUp=0; mode--;}
//      if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
//      if (Ok!=0 && lockOk==1 && mode==0){lockOk=0; lcd.clear(); sub=1; } // Isi submenu 1
//      if (Ok!=0 && lockOk==1 && mode==1){lockOk=0; lcd.clear(); sub=2; } // Isi submenu 2
//      if (Ok!=0 && lockOk==1 && mode==2){lockOk=0; lcd.clear(); sub=3; } // Isi submenu 3
//      if (Ok!=0 && lockOk==1 && mode==3){lockOk=0; lcd.clear(); sub=4; } // Isi submenu 4
//
//      if (mode>3 || mode<0) mode=0;
//    {
//      switch(mode){
//        case 0 : lcd.setCursor(3,0); lcd.print(">"); break;
//        case 1 : lcd.setCursor(3,1); lcd.print(">"); break;
//        case 2 : lcd.setCursor(10,0); lcd.print(">"); break;
//        case 3 : lcd.setCursor(10,1); lcd.print(">"); break;
//      }  
//    }
  }
  }  
  
  
  //isi data batas | ////---------------------------------------------------------------------------------------------------------------------- PENDINGIN | PENDINGIN

  if (dis==3 && sub==1 && endsub==2 && BatasT=="Nyala" ){ 
    //endsub=8;
    lcd.setCursor(0,0); lcd.print("Suhu Nyala: " + String(suhu_batas_nyala0)+ (char)223 + "C");
    if(key){lcd.clear();input += key;}  
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,1); lcd.print(input + " " + (char)223 + "C");
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=2; sub=1; BatasT=""; suhu_batas_nyala0=input.toInt();} // Isi submenu 1
  }
  if (dis==3 && sub==1 && endsub==2 && BatasT=="Mati" ){
    lcd.setCursor(0,0); lcd.print("Suhu Mati: " + String(suhu_batas_mati0) + (char)223 + "C"); 
    if(key){lcd.clear();input += key;}
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,1); lcd.print(input + " " + (char)223 + "C");
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=2; sub=1; BatasT=""; suhu_batas_mati0=input.toInt();} // Isi submenu 1
  }
  
  if (dis==3 && sub==1 && endsub==3 && BatasH=="Nyala" ){  
    lcd.setCursor(0,0); lcd.print("K.Lembap Nyala: " + String(kelembapan_batas_nyala0) + "%");
    if(key){lcd.clear();input += key;}
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,1); lcd.print(input + "%" );
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=3; sub=1; BatasH=""; kelembapan_batas_nyala0=input.toInt();} // Isi submenu 1
  }
  if (dis==3 && sub==1 && endsub==3 && BatasH=="Mati" ){  
          lcd.setCursor(0,0); lcd.print("K.Lembap Mati: " + String(kelembapan_batas_mati0) + "%");
    if(key){lcd.clear();input += key;}
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,1); lcd.print(input + "%" );
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=3; sub=1; BatasH=""; kelembapan_batas_mati0=input.toInt();} // Isi submenu 1
  }
  //isi data Timer| ////
  if (dis==3 && sub==1 && endsub==4 && SetTimer=="Menit" ){
    lcd.setCursor(0,0); lcd.print("Set Menit Timer" );
    lcd.setCursor(0,1); lcd.print("Saat ini: " +String(menit0) + " Menit");
    if(key){lcd.clear();input += key;}
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,2); lcd.print(input + " Menit" );
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=4; sub=1; SetTimer=""; menit0=input.toInt();} // Isi submenu 1
  }
  if (dis==3 && sub==1 && endsub==4 && SetTimer=="Jam"){
    lcd.setCursor(0,0); lcd.print("Set Jam Timer" );
    lcd.setCursor(0,1); lcd.print("Saat ini: " +String(jam0) + " Jam");
    if(key){lcd.clear();input += key;}
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,2); lcd.print(input + " Jam" );
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=4; sub=1; SetTimer=""; jam0=input.toInt();} // Isi submenu 1
  }

  
    //isi data batas | ////------------------------------------------------------------------------------------------------------------------------ PEMANAS | PEMANAS
  if (dis==3 && sub==2 && endsub==2 && BatasT=="Nyala" ){ 
    //endsub=8;
    lcd.setCursor(0,0); lcd.print("Suhu Nyala: " + String(suhu_batas_nyala1)+ (char)223 + "C");
    if(key){lcd.clear();input += key;}  
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,1); lcd.print(input + " " + (char)223 + "C");
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=2; sub=2; BatasT=""; suhu_batas_nyala1=input.toInt();} // Isi submenu 1
  }
  if (dis==3 && sub==2 && endsub==2 && BatasT=="Mati" ){
    lcd.setCursor(0,0); lcd.print("Suhu Mati: " + String(suhu_batas_mati1) + (char)223 + "C"); 
    if(key){lcd.clear();input += key;}
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,1); lcd.print(input + " " + (char)223 + "C");
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=2; sub=2; BatasT=""; suhu_batas_mati1=input.toInt();} // Isi submenu 1
  }
  if (dis==3 && sub==2 && endsub==3 && BatasH=="Nyala" ){  
    lcd.setCursor(0,0); lcd.print("K.Lembap Nyala: " + String(kelembapan_batas_nyala1) + "%");
    if(key){lcd.clear();input += key;}
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,1); lcd.print(input + "%" );
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=3; sub=2; BatasH=""; kelembapan_batas_nyala1=input.toInt();} // Isi submenu 1
  }
  if (dis==3 && sub==2 && endsub==3 && BatasH=="Mati" ){  
    lcd.setCursor(0,0); lcd.print("K.Lembap Mati: " + String(kelembapan_batas_mati1) + "%");
    if(key){lcd.clear();input += key;}
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,1); lcd.print(input + "%" );
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=3; sub=2; BatasH=""; kelembapan_batas_mati1=input.toInt();} // Isi submenu 1
  }
  //isi data Timer| ////
  if (dis==3 && sub==2 && endsub==4 && SetTimer=="Menit" ){
    lcd.setCursor(0,0); lcd.print("Set Menit Timer" );
    lcd.setCursor(0,1); lcd.print("Saat ini: " +String(menit1) + " Menit");
    if(key){lcd.clear();input += key;}
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,2); lcd.print(input + " Menit" );
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=4; sub=2; SetTimer=""; menit1=input.toInt();} // Isi submenu 1
  }
  if (dis==3 && sub==2 && endsub==4 && SetTimer=="Jam"){
    lcd.setCursor(0,0); lcd.print("Set Jam Timer" );
    lcd.setCursor(0,1); lcd.print("Saat ini: " +String(jam1) + " Jam");
    if(key){lcd.clear();input += key;}
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,2); lcd.print(input + " Jam" );
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=4; sub=2; SetTimer=""; jam1=input.toInt();} // Isi submenu 1
  }

      //isi data batas | ////------------------------------------------------------------------------------------------------------------------------ BLOWER | BLOWER
  if (dis==3 && sub==3 && endsub==2 && BatasT=="Nyala" ){ 
    //endsub=8;
    lcd.setCursor(0,0); lcd.print("Suhu Nyala: " + String(suhu_batas_nyala2)+ (char)223 + "C");
    if(key){lcd.clear();input += key;}  
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,1); lcd.print(input + " " + (char)223 + "C");
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=2; sub=3; BatasT=""; suhu_batas_nyala2=input.toInt();} // Isi submenu 1
  }
  if (dis==3 && sub==3 && endsub==2 && BatasT=="Mati" ){
    lcd.setCursor(0,0); lcd.print("Suhu Mati: " + String(suhu_batas_mati2) + (char)223 + "C"); 
    if(key){lcd.clear();input += key;}
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,1); lcd.print(input + " " + (char)223 + "C");
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=2; sub=3; BatasT=""; suhu_batas_mati2=input.toInt();} // Isi submenu 1
  }
  
  if (dis==3 && sub==3 && endsub==3 && BatasH=="Nyala" ){  
    lcd.setCursor(0,0); lcd.print("K.Lembap Nyala: " + String(kelembapan_batas_nyala2) + "%");
    if(key){lcd.clear();input += key;}
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,1); lcd.print(input + "%" );
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=3; sub=3; BatasH=""; kelembapan_batas_nyala2=input.toInt();} // Isi submenu 1
  }
  if (dis==3 && sub==3 && endsub==3 && BatasH=="Mati" ){  
    lcd.setCursor(0,0); lcd.print("K.Lembap Mati: " + String(kelembapan_batas_mati2) + "%");
    if(key){lcd.clear();input += key;}
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,1); lcd.print(input + "%" );
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=3; sub=3; BatasH=""; kelembapan_batas_mati2=input.toInt();} // Isi submenu 1
  }
  //isi data Timer| ////
  if (dis==3 && sub==3 && endsub==4 && SetTimer=="Menit" ){
    lcd.setCursor(0,0); lcd.print("Set Menit Timer" );
    lcd.setCursor(0,1); lcd.print("Saat ini: " +String(menit2) + " Menit");
    if(key){lcd.clear();input += key;}
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,2); lcd.print(input + " Menit" );
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=4; sub=3; SetTimer=""; menit2=input.toInt();} // Isi submenu 1
  }
  if (dis==3 && sub==3 && endsub==4 && SetTimer=="Jam"){
    lcd.setCursor(0,0); lcd.print("Set Jam Timer" );
    lcd.setCursor(0,1); lcd.print("Saat ini: " +String(jam2) + " Jam");
    if(key){lcd.clear();input += key;}
    if(setpoin==1){lcd.clear();input="";setpoin=0;}
    lcd.setCursor(0,2); lcd.print(input + " Jam" );
    if (Ok==0 && lockOk==0){lockOk=1; mode=0; lcd.clear();}
    if (Ok!=0 && lockOk==1 && mode==0 ){lockOk=0; lcd.clear();  dis=3; endsub=4; sub=3; SetTimer=""; jam2=input.toInt();} // Isi submenu 1
  }
}
