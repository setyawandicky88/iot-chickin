// ================= chickin firmware V5_R1 ================= //
// ================= Setting Environment ================= //
String versi = "V5_R1";
// development / stagging / production
String mode_release = "development";
// ================= Setting Environment ================= //

#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <ArduinoJson.h>
#include "DHT.h"
#include <Wire.h>
#include <NTPClient.h>
#include <TimeLib.h>
// ================= alarm library ================= //
#include <time.h>
#include <TimeAlarms.h>
// ================= alarm library ================= //
#include <Preferences.h>
#include <SPI.h>
#include <HTTPUpdate.h>
#include <PubSubClient.h>
#include <Keypad_I2C.h>
#include "RTClib.h"

// ================= nrf library ================= //
#include <nRF24L01.h>
#include <RF24.h>

// BLE Libraries
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

// alarms
AlarmId idIntermittent;

// ================= init task ================= //
TaskHandle_t TaskShowMenu;
TaskHandle_t TaskReconnect;
TaskHandle_t TaskAlarm;
// ================= init task ================= //

// ================= init nrf ================= //
#define NRF24_CHANNEL 124
float rangeTemp = NULL, suhuAktual = NULL;
// debugging masalah nrf tidak masuk di mcc

// create an RF24 object
RF24 radio(5, 2); // CE, CSN

struct sensor
{ // RX data format
  char id[4];
  float t[3];
  float h[3];
  byte tcount;
} wsn;
bool checkSensor = false;
// address through which two modules communicate.
uint8_t address = 0xF0F0F0F0AA;

unsigned long millisSensor = 0;
unsigned long previousMillisSensor = 0;
float suhuSensor;
String statusSensor = "";

// ================= init nrf ================= //
// Setting waktu
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org");

// ================= init general variable ================= //
Preferences preferences;
// ================= init general variable ================= //

// ================= init flock variable ================= //
bool updateState;
String hostUpdate;
String version;

unsigned long oldtime = 0;
unsigned long previousMillisUpdateFlock = 0;
unsigned long intermitMillis = 0;

float suhu;
// float suhuAktual;
float calibrationTemp = 0.0;
bool statusCalibrationTemp = false;
// float suhuSensor;
int pwm;
float frequency;
String intermittent = "off";
unsigned long previousMillis = 0;                          // blower 1
unsigned long previousMillisCooler = 0;                    // pendingin
unsigned long previousMillisHeater = 0;                    // pemanas
unsigned long millisTime;                                  // blower 1
unsigned long millisTimeCooler;                            // pendingin
unsigned long millisTimeHeater;                            // pemanas
unsigned long intermittentOff, intermittentOn;             // intermittent blower 1
unsigned long intermittentOffCooler, intermittentOnCooler; // intermittent pendingin
unsigned long intermittentOffHeater, intermittentOnHeater; // intermittent pemanaas
int deviceOff = 5, deviceOn = 5, deviceOffHeater = 0, deviceOnHeater = 0, deviceOffCooler = 0, deviceOnCooler = 0;
int deviceOffFan2, deviceOnFan2, deviceOffFan3, deviceOnFan3, deviceOffFan4, deviceOnFan4;
String statusSuhu = ""; // ideal / tidak_ideal
String mode = "mcc";    // mcc atau inventer
float batasBawah;
float batasAtas;
int hari = 0;
bool incrementHari = true;
bool statusInventer = true;
bool statusIntermittent = true;
// float rangeTemp = 0.0;
String payload;
String payloadResponse;
String baseUrl = "https://api-iot.chickinindonesia.com/api/";
String rangeTempStatus;
bool wifiStatusBool = false;
bool connected = false;
String wifiStatus;
String warning = "";

// variable for change value
float suhuB;
float suhuAktualB;
float hB;
String modeB;
int hariB;
String warningB;
float calibrationTempB = 0.0;

// ================= init custom function ================= //
void Sensor();
String httpGET();
void ShowMenu();
void updateOTA();
void SendStatusWifi();
void TimeSync();
void SetTimeNtp();
void IncrementDay();
void digitalClockDisplay();
void Intermittent();
void IntermittentCooler();
void IntermittentHeater();
void UpdateDevice();
void ShowTimeNTP();
bool ValueChange();
void UpdateFlock();
void printDigits();
byte decToBcd();
void TimeReader();
void FirmwareUpdateMCC();
void FirmwareUpdate();
// ================= init custom function ================= //

// ================= init flock variable ================= //

// String ssid = "Workshop_Batujajar";
// String password = "Workshop2020";
String ssid = "";
String password = "";
String idUser = "";
String flockId = "";
String mqttID;

#define sensorDebug \
  if (1)            \
  Serial
// ================= init flock variable ================= //

// ================= init variable sensor ================= //
float h;
float t;
float f;
String idSensor = "";
char idSensorChar[4] = "th1";
// ================= init variable sensor ================= //

// ================= init mqtt topic and variable ================= //
String topicHeater, topicCooling, topicFan1, topicFan2, topicFan3, topicFan4, topicInverter, topicFlock, topicIntermittent, topicPing, topicSensor, topicUpdate;
String topicHeaterPub, topicCoolingPub, topicFan1Pub, topicFan2Pub, topicFan3Pub, topicFan4Pub, topicInverterPub, topicFlockPub, topicIntermittentPub, topicPingPub;
String blower1 = "off";   // perangkat 3
String blower2 = "off";   // perangkat 4
String blower3 = "off";   // perangkat 5
String blower4 = "off";   // perangkat 6
String lampu = "off";     // perangkat 7
String pemanas = "off";   // perangkat 1
String pendingin = "off"; // perangkat 2
int inventer = 30;
String inputIntermittent;
String inputFlock;
String ping = "";
// flock/{flock_id}/device_name subscribe
// flock/{flock_id}/device_name/pub publish

const char *brokerUser = "chickin";
const char *brokerPass = "chickin";
const char *broker = "103.31.39.17";
const char port = 1883;
// ================= init mqtt topic and variable ================= //

// ================= init menu variable ================= //
unsigned long millisMenu;
unsigned long PreviousMillisMenu;

#define BEFORE_LCD_OFF 20000 // waktu on display sebelum off lagi
unsigned long millis_lcd = 0;
bool lcdState = false;
#define I2CADDR 0x20 // Set the Address of the PCF8574 // keypad address

const byte ROWS = 4; // Set the number of Rows
const byte COLS = 4; // Set the number of Columns

// Set the Key at Use (4x4)
char keys[ROWS][COLS] = {
    {'1', '4', '7', '*'},
    {'2', '5', '8', '0'},
    {'3', '6', '9', '#'},
    {'A', 'B', 'C', 'D'}};

// define active Pin (4x4)
byte rowPins[ROWS] = {0, 1, 2, 3}; // Connect to Keyboard Row Pin
byte colPins[COLS] = {4, 5, 6, 7}; // Connect to Pin column of keypad.

Keypad_I2C keypad(makeKeymap(keys), rowPins, colPins, ROWS, COLS, I2CADDR, PCF8574);
// ================= init menu variable ================= //

WiFiClient espClient;
PubSubClient client(espClient);

// ================= BLE Pairing Device ================= //
// BLE Libraries for pairing
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

BLEServer *pServer = NULL;
BLECharacteristic *pTxCharacteristic;
bool bleDeviceConnected = false;
bool oldBleDeiceConnected = false;

bool reset = false;

#define SERVICE_UUID "9217b3df-6fd4-4dd5-aeeb-350908599f7f" // UART service UUID
#define CHARACTERISTIC_UUID_RX "2a201d77-0d75-415e-9c97-106e687c5206"
#define CHARACTERISTIC_UUID_TX "4ab0a9a1-1b7a-4bd5-b92e-3c0bf83bf46d"

class ChickinBLEServerCallbacks : public BLEServerCallbacks
{
  void onConnect(BLEServer *pServer)
  {
    bleDeviceConnected = true;
  }

  void onDisconnect(BLEServer *pServer)
  {
    bleDeviceConnected = false;
  }
};

class BLECallbacks : public BLECharacteristicCallbacks
{
public:
  Preferences preferences;
  BLECallbacks(Preferences &p)
  {
    preferences = p;
  }
  String getValue(String data, char separator, int index)
  {
    int found = 0;
    int strIndex[] = {0, -1};
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++)
    {
      if (data.charAt(i) == separator || i == maxIndex)
      {
        found++;
        strIndex[0] = strIndex[1] + 1;
        strIndex[1] = (i == maxIndex) ? i + 1 : i;
      }
    }

    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
  }

  void onWrite(BLECharacteristic *pCharacteristic)
  {
    std::string rxValue = pCharacteristic->getValue();
    String rxString = rxValue.c_str();
    if (rxValue.length() > 0)
    {
      String cCommand = getValue(rxString, '#', 0);
      if (cCommand == "pairing")
      {
        String cSsid = getValue(rxString, '#', 1);
        String cSsidpass = getValue(rxString, '#', 2);
        String cUserId = getValue(rxString, '#', 3);
        String cFlockId = getValue(rxString, '#', 4);
        if (cSsid != "" && cSsidpass != "" && cUserId != "" && cFlockId != "")
        {
          Serial.print(cSsid);
          Serial.print(" ");
          Serial.print(cSsidpass);
          Serial.print(" ");
          Serial.print(cUserId);
          Serial.print(" ");
          Serial.print(cFlockId);
          Serial.println();
          preferences.putString("ssid", cSsid);
          delay(10);

          preferences.putString("password", cSsidpass);
          delay(10);

          preferences.putString("user_id", cUserId);
          delay(10);

          preferences.putString("flock_id", cFlockId);
          delay(10);

          // Serial.println("\n\nSistem Reset");

          // String btResponse = "ok";

          // delay(100);
          // ESP.restart();

          // delay(1000);
        }
      }

      // if (cCommand == "reboot")
      // {
      //   delay(100);
      //   ESP.restart();
      // }
    }
  }
};
// ================= BLE Pairing Device ================= //

// END KEYPAD

// RTC
const int I2C_ADDRESS = 0x68;
// DS3231  rtc(26, 25);

const char *days[] = {"Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"};
const char *months[] = {"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "October", "November", "Desember"};

byte secondt = 0;
byte minutet = 0;
byte hourt = 0;
byte weekdayt = 0;
byte monthdayt = 0;
byte montht = 0;
byte yeart = 0;
const char *AMPM = 0;

int rtcSecond = 01, rtcMinute = 43, rtcHour = 20, rtcWeekday = 4, rtcMonthday = 8, rtcMonth = 12, rtcYear = 21;
// RTC
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27, 20, 4);

// pin LCD
#define I2C_SDA 21
#define I2C_SCL 22

// timer
//  SimpleTimer timer;
int timerId;

int modePanel;
float suhuIdeal = 0.0;

#define LED_BUILTIN 2

// ================= init pin ================= //
int buttonState;
#define pinSwitch 39
#define pinButton 34
int cekReset;
#define DHTPIN 4
#define DHTTYPE DHT22 // DHT 11  (AM2302), AM2321
DHT dht(DHTPIN, DHTTYPE);
#define pinPemanas 26   // pemanas
#define pinPendingin 27 // pendingin
#define pinBlower1 32   // blower
#define pinBlower2 33   // blower
#define pinBlower3 25   // blower
#define pinBlower4 30   // blower // belum tau pinnya
#define pinLampu 30     // Lampu // belum tau pinnya
#define pinInverter 12  // blower dengan inverter
// #define powerLedWifi 5
int powerLED = 4;
bool ledConnected;

#define BLOWER1_ACTIVE LOW
#define BLOWER2_ACTIVE LOW
#define BLOWER3_ACTIVE LOW
#define BLOWER4_ACTIVE LOW
#define HEATER_ACTIVE LOW
#define COOLER_ACTIVE LOW
#define LAMPU LOW
// ================= init pin ================= //

// ================= init variable offline mode ================= //
int nilai;
int Cancel = 0, Up = 0, Down = 0, Ok = 0;
int lockCancel = 0, lockUp = 0, lockDown = 0, lockOk = 0;
int dis = 1, sub = 0, sub1 = 0, mode1 = 0;

int rtcCancel = 0, rtcUp = 0, rtcDown = 0, rtcOk = 0;
int rtcLockCancel = 0, rtcLockUp = 0, rtcLockDown = 0, rtcLockOk = 0;
int rtcDis = 1, rtcSub = 0, rtcMode = 0;
int statusTime;
// ================= init variable offline mode ================= //

// for EEPROM
String idPerangkat1 = "", idPerangkat2 = "", idPerangkat3 = "", idPerangkat4 = "", idPerangkat5 = "", idPerangkat6 = "";

// interval
long interval = 60000 * 10;
// long interval = 10000;
long intervalInventer = 60000 * 5;
// long intervalInventer = 5000;

long interval2 = 5000;
long interval3 = 5000;

// ================= init rtc ================= //
// RTC_DS1307 rtc;
// DateTime nowRTC;
// ================= init rtc ================= //

// ================= init variable lcd ================= //
String menuIdle = "menu";
char menuPosition;
char subMenuPosition;
char subMenuPosition1;
char subMenuPosition2;
char keyPress;
String nilaiInverter;
String input;
int setPoint;
int keyPressed;
// ================= init variable lcd ================= //

// ================= callback mqtt ================= //
void callback(char *topic, byte *payload, unsigned int length)
{
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
  if (String(topic) == topicUpdate)
  {
    String updateIn = "";
    hostUpdate = "";
    for (int i = 0; i < length; i++)
    {
      updateIn += (char)payload[i];
    }
    StaticJsonDocument<125> doc;

    DeserializationError error = deserializeJson(doc, updateIn);

    if (error)
    {
      Serial.print("deserializeJson() failed: ");
      Serial.println(error.c_str());
      return;
    }

    const char *hostUpdateChar = doc["host_update"]; // "okoko"
    const char *versionChar = doc["version"];        // "1.1"
    hostUpdate = String(hostUpdateChar);
    version = String(versionChar);
    updateState = true;

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Updating Software...");
    Serial.println(hostUpdate);
    Serial.println(version);
    delay(3000);

    FirmwareUpdate();
  }
  if (String(topic) == topicSensor)
  {
    String sensorIn = "";
    for (int i = 0; i < length; i++)
    {
      Serial.print((char)payload[i]);
      sensorIn += (char)payload[i];
    }
    int str_len = sensorIn.length() + 1;
    idSensorChar[str_len];
    sensorIn.toCharArray(idSensorChar, str_len);
    Serial.print("id : ");
    Serial.println(idSensorChar);
    preferences.begin("flock", false);
    preferences.putString("id_sensor", sensorIn);
  }
  if (String(topic) == topicHeater)
  {
    pemanas = "";
    for (int i = 0; i < length; i++)
    {
      Serial.print((char)payload[i]);
      pemanas += (char)payload[i];
    }
    UpdateDevice(pemanas, flockId, topicHeaterPub);
  }
  if (String(topic) == topicCooling)
  {
    pendingin = "";
    for (int i = 0; i < length; i++)
    {
      Serial.print((char)payload[i]);
      pendingin += (char)payload[i];
    }
    UpdateDevice(pendingin, flockId, topicCoolingPub);
  }
  if (String(topic) == topicFan1)
  {
    blower1 = "";
    for (int i = 0; i < length; i++)
    {
      Serial.print((char)payload[i]);
      blower1 += (char)payload[i];
    }
    UpdateDevice(blower1, flockId, topicFan1Pub);
  }
  if (String(topic) == topicFan2)
  {
    blower2 = "";
    for (int i = 0; i < length; i++)
    {
      Serial.print((char)payload[i]);
      blower2 += (char)payload[i];
    }
    UpdateDevice(blower2, flockId, topicFan2Pub);
  }
  if (String(topic) == topicFan3)
  {
    blower3 = "";
    for (int i = 0; i < length; i++)
    {
      Serial.print((char)payload[i]);
      blower3 += (char)payload[i];
    }
    UpdateDevice(blower3, flockId, topicFan3Pub);
  }
  if (String(topic) == topicFan4)
  {
    blower4 = "";
    for (int i = 0; i < length; i++)
    {
      Serial.print((char)payload[i]);
      blower4 += (char)payload[i];
    }
    UpdateDevice(blower4, flockId, topicFan4Pub);
  }
  if (String(topic) == topicInverter)
  {
    inventer = 0;
    String stringInverter;
    for (int i = 0; i < length; i++)
    {
      Serial.print((char)payload[i]);
      stringInverter += (char)payload[i];
    }
    inventer = stringInverter.toInt();
    UpdateDevice(String(inventer), flockId, topicInverterPub);
  }
  if (String(topic) == topicIntermittent)
  {
    inputIntermittent = "";
    deviceOff = 0;
    deviceOn = 0;
    for (int i = 0; i < length; i++)
    {
      Serial.print((char)payload[i]);
      inputIntermittent += (char)payload[i];
    }

    StaticJsonDocument<1024> doc;

    DeserializationError error = deserializeJson(doc, inputIntermittent);

    if (error)
    {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.f_str());
      return;
    }

    deviceOff = doc["OffF1"];
    deviceOn = doc["OnF1"];
    deviceOffFan2 = doc["OffF2"];
    deviceOnFan2 = doc["OnF2"];
    deviceOffFan3 = doc["OffF3"];
    deviceOnFan3 = doc["OnF3"];
    deviceOffFan4 = doc["OffF4"];
    deviceOnFan4 = doc["OnF4"];
    deviceOffCooler = doc["OffCooler"];
    deviceOnCooler = doc["OnCooler"];
    deviceOffHeater = doc["OffHeater"];
    deviceOnHeater = doc["OnHeater"];

    Serial.println("off 1 : " + String(deviceOff));

    UpdateIntermittent(flockId, topicIntermittentPub);
    preferences.begin("flock", false);
    preferences.putInt("heater_off", deviceOffHeater);
    preferences.putInt("heater_on", deviceOnHeater);
    preferences.putInt("cooler_off", deviceOffCooler);
    preferences.putInt("cooler_on", deviceOnCooler);
    preferences.putInt("fan1_off", deviceOff);
    preferences.putInt("fan1_on", deviceOn);
    preferences.putInt("fan2_off", deviceOffFan2);
    preferences.putInt("fan2_on", deviceOnFan2);
    preferences.putInt("fan3_off", deviceOffFan3);
    preferences.putInt("fan3_on", deviceOnFan3);
    preferences.putInt("fan4_off", deviceOffFan4);
    preferences.putInt("fan4_on", deviceOnFan4);
  }

  if (String(topic) == topicFlock)
  {
    inputFlock = "";
    for (int i = 0; i < length; i++)
    {
      Serial.print((char)payload[i]);
      inputFlock += (char)payload[i];
    }
    StaticJsonDocument<1024> doc;

    DeserializationError error = deserializeJson(doc, inputFlock);

    if (error)
    {
      Serial.print(F("deserializeJson() failed: "));
      Serial.println(error.f_str());
      return;
    }

    calibrationTemp = doc["calibrationTemperature"];
    // const char* modeIn = doc["mode"];
    // mode = String(modeIn);
    // hari = doc["day"];

    preferences.begin("flock", false);
    preferences.putFloat("calibration_temp", calibrationTemp);

    // int actualTemperature = doc["actualTemperature"]; // 10
    // int idealTemperature = doc["idealTemperature"]; // 20
    // int humidity = doc["humidity"]; // 60
    // const char* autoMCC = doc["autoMCC"]; // "off"
    // bool connected = doc["connected"]; // false
    // bool wifiStatus = doc["wifiStatus"]; // false
    // const char* warning = doc["warning"]; // "ini warning"
    UpdateFlock();
  }
  Serial.println();
  Serial.println("-----------------------");
}

String testing;

void setup()
{
  Serial.begin(115200);
  // ================= setup keypad ================= //
  Wire.begin();                   // Call the connection Wire
  keypad.begin(makeKeymap(keys)); // Call the connection

  lcd.init();
  lcd.backlight();
  pinMode(LED_BUILTIN, OUTPUT);
  // ================= setup device pin ================= //
  pinMode(pinSwitch, INPUT);
  pinMode(pinButton, INPUT_PULLUP);
  pinMode(pinPendingin, OUTPUT);
  pinMode(pinPemanas, OUTPUT);
  pinMode(pinBlower1, OUTPUT);
  pinMode(pinBlower2, OUTPUT);
  pinMode(pinBlower3, OUTPUT);
  // pinMode(pinBlower4, OUTPUT);
  // pinMode(pinLampu, OUTPUT);
  ledcAttachPin(pinInverter, 0);
  ledcSetup(0, 1000, 8);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(powerLED, OUTPUT);

  digitalWrite(pinPendingin, !COOLER_ACTIVE);
  digitalWrite(pinPemanas, !HEATER_ACTIVE);
  digitalWrite(pinBlower1, !BLOWER1_ACTIVE);
  digitalWrite(pinBlower2, !BLOWER2_ACTIVE);
  digitalWrite(pinBlower3, !BLOWER3_ACTIVE);
  // digitalWrite(pinBlower4, !BLOWER4_ACTIVE);
  // digitalWrite(pinLampu, !LAMPU);
  // ================= setup device pin ================= //

  dht.begin();

  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // set text lcd
  lcd.setCursor(0, 0);
  lcd.print("Welcome");
  lcd.setCursor(3, 1);
  lcd.print("MCC Chickin");
  lcd.setCursor(1, 2);
  lcd.print(versi);
  delay(1000);

  // for inventer
  // inventer
  statusInventer = true;
  statusIntermittent = true; //??
                             // ================= setup pairing ================= //
  preferences.begin("flock", false);
  if (mode_release == "development")
  {
    flockId = "627483dbda270123d8b369fc";
    ssid = "realme C3";
    password = "12345678";
    Serial.println(flockId);
  }
  else
  {
    flockId = preferences.getString("flock_id", "");
    Serial.println(flockId);
    ssid = preferences.getString("ssid", "");
    password = preferences.getString("password", "");
  }
  Serial.print("ssid: ");
  Serial.println(ssid);
  Serial.print("Password: ");
  Serial.println(password);
  if (ssid != "")
  {
    Serial.println("Connecting to wifi");
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Connecting to SSID ");
    lcd.setCursor(0, 1);
    lcd.print(ssid);

    WiFi.disconnect(true);
    delay(1000);
    WiFi.mode(WIFI_STA);
    delay(1000);

    WiFi.begin(ssid.c_str(), password.c_str());
    // delay(4000);
    int cek = 0;
    while (WiFi.status() != WL_CONNECTED)
    {
      delay(1000);
      Serial.print(".");
      cek++;
      Serial.print("cek : " + String(cek));
      if (cek == 30)
      {
        break;
      }
    }
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("WiFi Connected");
    lcd.setCursor(0, 1);
    lcd.print("IP Address : ");
    lcd.setCursor(0, 3);
    lcd.print(WiFi.localIP());

    Serial.println("");
    Serial.println("WiFi connected.");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  }
  else
  {
    Serial.println("BLE Starting....");
    BLEDevice::init("Chickin BLE Service");
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("BLE Starting");
    delay(1000);
    lcd.setCursor(0, 1);
    lcd.print("Chickin BLE Service");
    delay(1000);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Connect to ");
    lcd.setCursor(0, 1);
    lcd.print("Chickin BLE Service");

    pServer = BLEDevice::createServer();
    pServer->setCallbacks(new ChickinBLEServerCallbacks());

    BLEService *pService = pServer->createService(SERVICE_UUID);

    pTxCharacteristic = pService->createCharacteristic(CHARACTERISTIC_UUID_TX, BLECharacteristic::PROPERTY_NOTIFY);
    pTxCharacteristic->addDescriptor(new BLE2902());

    BLECharacteristic *pRxCharacteristic = pService->createCharacteristic(CHARACTERISTIC_UUID_RX, BLECharacteristic::PROPERTY_WRITE);

    pRxCharacteristic->setCallbacks(new BLECallbacks(preferences));

    pService->start();
    pServer->getAdvertising()->start();

    Serial.println("BLE Started....");

    byte nowCek = 0;
    bool nowStateSsid = false;

    int pairingBLE = 100;
    while (pairingBLE > 0)
    {
      lcd.setCursor(9, 3);
      lcd.print(" " + String(pairingBLE) + " ");
      delay(1000);
      Serial.print(".");
      pairingBLE--;
      if (pairingBLE < 0)
      {
        break;
      }

      if (keypad.getKey() == 'D')
      {
        break;
      }

      if (!nowStateSsid)
      {
        nowCek = pairingBLE;
      }
      if (preferences.getString("ssid", "") != "")
      {
        nowStateSsid = true;
        if (nowCek - pairingBLE >= 4)
        {
          Serial.println("Sistem Reset");
          ESP.restart();
        }
      }
    }
    // delay(60000);
  }
  // ================= setup pairing ================= //

  // ================= setup nrf ================= //
  radio.begin();

  // set the address
  radio.openReadingPipe(0, address);
  radio.setChannel(NRF24_CHANNEL);
  radio.setPALevel(RF24_PA_MAX);
  // Set module as receiver
  radio.startListening();

  // ================= setup nrf ================= //

  // ================= syncrhonize data ================= //
  float cali = preferences.getFloat("calibration_temp", 0);
  if (cali != 0)
  {
    calibrationTemp = cali;
  }

  inventer = preferences.getInt("inverter", 30);
  deviceOff = preferences.getInt("fan1_off", 5);
  deviceOn = preferences.getInt("fan1_on", 5);
  deviceOffCooler = preferences.getInt("cooler_off", 0);
  deviceOnCooler = preferences.getInt("cooler_on", 0);
  deviceOffHeater = preferences.getInt("heater_off", 0);
  deviceOnHeater = preferences.getInt("heater_on", 0);
  hari = preferences.getInt("day", 0);
  idSensor = preferences.getString("id_sensor", "");
  int str_len = idSensor.length() + 1;
  idSensorChar[str_len];
  idSensor.toCharArray(idSensorChar, str_len);
  // ================= syncrhonize data ================= //

  if (WiFi.status() == WL_CONNECTED)
  {
    // development
    TimeSync();
    // setTime(2, 37, 0, 3, 31, 22);
    lcd.clear();
    lcd.setCursor(1, 0);
    lcd.print("Loading ...");
    delay(2000);

    // ================= setup topic and mqtt ================= //
    // topic publish
    if (mode_release == "production")
    {
      topicHeaterPub = "flock/" + flockId + "/heater/sub";
      topicCoolingPub = "flock/" + flockId + "/cooling/sub";
      topicFan1Pub = "flock/" + flockId + "/fan1/sub";
      topicFan2Pub = "flock/" + flockId + "/fan2/sub";
      topicFan3Pub = "flock/" + flockId + "/fan3/sub";
      topicFan4Pub = "flock/" + flockId + "/fan4/sub";
      topicInverterPub = "flock/" + flockId + "/inverter/sub";
      topicFlockPub = "flock/" + flockId + "/flock/sub";
      topicIntermittentPub = "flock/" + flockId + "/intermittent/sub";
      topicPingPub = "flock/" + flockId + "/ping/sub";
      // topic subscribe
      topicHeater = "flock/" + flockId + "/heater";
      topicCooling = "flock/" + flockId + "/cooling";
      topicFan1 = "flock/" + flockId + "/fan1";
      topicFan2 = "flock/" + flockId + "/fan2";
      topicFan3 = "flock/" + flockId + "/fan3";
      topicFan4 = "flock/" + flockId + "/fan4";
      topicInverter = "flock/" + flockId + "/inverter";
      topicFlock = "flock/" + flockId + "/flock";
      topicIntermittent = "flock/" + flockId + "/intermittent";
      topicPing = "flock/" + flockId + "/ping";
      topicSensor = "flock/" + flockId + "/sensor";
      topicUpdate = "flock/" + flockId + "/update";
    }
    else if (mode_release == "stagging" || mode_release == "development")
    {
      topicHeaterPub = "flock/stagging/" + flockId + "/heater/sub";
      topicCoolingPub = "flock/stagging/" + flockId + "/cooling/sub";
      topicFan1Pub = "flock/stagging/" + flockId + "/fan1/sub";
      topicFan2Pub = "flock/stagging/" + flockId + "/fan2/sub";
      topicFan3Pub = "flock/stagging/" + flockId + "/fan3/sub";
      topicFan4Pub = "flock/stagging/" + flockId + "/fan4/sub";
      topicInverterPub = "flock/stagging/" + flockId + "/inverter/sub";
      topicFlockPub = "flock/stagging/" + flockId + "/flock/sub";
      topicIntermittentPub = "flock/stagging/" + flockId + "/intermittent/sub";
      topicPingPub = "flock/stagging/" + flockId + "/ping/sub";
      // topic subscribe
      topicHeater = "flock/stagging/" + flockId + "/heater";
      topicCooling = "flock/stagging/" + flockId + "/cooling";
      topicFan1 = "flock/stagging/" + flockId + "/fan1";
      topicFan2 = "flock/stagging/" + flockId + "/fan2";
      topicFan3 = "flock/stagging/" + flockId + "/fan3";
      topicFan4 = "flock/stagging/" + flockId + "/fan4";
      topicInverter = "flock/stagging/" + flockId + "/inverter";
      topicFlock = "flock/stagging/" + flockId + "/flock";
      topicIntermittent = "flock/stagging/" + flockId + "/intermittent";
      topicPing = "flock/stagging/" + flockId + "/ping";
      topicSensor = "flock/stagging/" + flockId + "/sensor";
      topicUpdate = "flock/stagging/" + flockId + "/update";
    }
    mqttID = "mqtt_client_" + flockId;
    client.setServer(broker, 1883);
    client.setCallback(callback);

    int checkMQTT = 0;
    while (!client.connected())
    {
      Serial.println("Connecting to MQTT...");

      if (client.connect(mqttID.c_str(), "chickin", "chickin"))
      {

        Serial.println("connected");
      }
      else
      {

        Serial.print("failed with state ");
        Serial.print(client.state());
        delay(2000);
        checkMQTT++;
      }
      if (checkMQTT == 5)
      {
        break;
      }
    }

    client.subscribe(topicHeater.c_str());
    client.subscribe(topicCooling.c_str());
    client.subscribe(topicFan1.c_str());
    client.subscribe(topicFan2.c_str());
    client.subscribe(topicFan3.c_str());
    client.subscribe(topicFan4.c_str());
    client.subscribe(topicInverter.c_str());
    client.subscribe(topicFlock.c_str());
    client.subscribe(topicIntermittent.c_str());
    client.subscribe(topicSensor.c_str());
    client.subscribe(topicUpdate.c_str());
    // ================= setup topic and mqtt ================= //
  }

  // ================= alarm for add day ================= //
  // nowRTC = rtc.now();
  // rtcMonth = nowRTC.month();
  // rtcHour = nowRTC.hour();
  // rtcMinute = nowRTC.minute();
  // rtcSecond = nowRTC.second();
  setTime(rtcHour, rtcMinute, rtcSecond, rtcMonth, rtcMonthday, rtcYear);
  // Alarm.alarmRepeat(3, 50, 20, IncrementDay);

  // ================= setup task ================= //
  // xTaskCreatePinnedToCore(
  //     TaskShowMenuCode, /* Task function. */
  //     "TaskShowMenu",   /* name of task. */
  //     20000,            /* Stack size of task */
  //     NULL,             /* parameter of the task */
  //     1,                /* priority of the task */
  //     &TaskShowMenu,    /* Task handle to keep track of created task */
  //     1);               /* pin task to core 0 */
  xTaskCreatePinnedToCore(
      TaskReconnectCode, /* Task function. */
      "TaskReconnect",   /* name of task. */
      10000,             /* Stack size of task */
      NULL,              /* parameter of the task */
      1,                 /* priority of the task */
      &TaskReconnect,    /* Task handle to keep track of created task */
      1);                /* pin task to core 0 */
  xTaskCreatePinnedToCore(
      TaskAlarmCode, /* Task function. */
      "TaskAlarm",   /* name of task. */
      10000,         /* Stack size of task */
      NULL,          /* parameter of the task */
      1,             /* priority of the task */
      &TaskAlarm,    /* Task handle to keep track of created task */
      1);            /* pin task to core 0 */

  Serial.print("environment mode : ");
  Serial.println(mode_release);
}

void loop()
{
  Sensor();
  millisMenu = millis();
  if (millisMenu - PreviousMillisMenu >= 10)
  {
    ShowMenu();
    PreviousMillisMenu = millisMenu;
  }

  if (deviceOff != 0 && deviceOn != 0)
  {
    Intermittent();
  }

  if (deviceOffCooler != 0 && deviceOnCooler != 0)
  {
    IntermittentCooler();
  }

  if (deviceOffHeater != 0 && deviceOnHeater != 0)
  {
    IntermittentHeater();
  }

  client.loop();

  // ================= calibration temperature ================= // nanti dibuatkan fungsi
  if (calibrationTemp != 0.00)
  {
    if (calibrationTemp > suhuSensor)
    {
      rangeTemp = calibrationTemp - suhuSensor;
      suhuAktual = suhuSensor + rangeTemp;
      rangeTempStatus = "+" + String(rangeTemp);
      statusCalibrationTemp = true;
    }
    else if (calibrationTemp < suhuSensor)
    {
      rangeTemp = suhuSensor - calibrationTemp;
      suhuAktual = suhuSensor - rangeTemp;
      rangeTempStatus = "-" + String(rangeTemp);
      statusCalibrationTemp = true;
    }
    calibrationTemp = 0.00;
  }
  else
  {
    if (calibrationTemp > suhuSensor)
    {
      suhuAktual = suhuSensor + rangeTemp;
      rangeTempStatus = "+" + String(rangeTemp);
    }
    else if (calibrationTemp < suhuSensor)
    {
      suhuAktual = suhuSensor - rangeTemp;
      rangeTempStatus = "-" + String(rangeTemp);
    }
  }
  // ================= calibration temperature ================= //

  if (WiFi.status() != WL_CONNECTED)
  {
    wifiStatus = "Not Connect";
  }
  else
  {
    wifiStatus = "Connected  ";
  }

  // ================= used for testing ================= //
  int hm;
  if (Serial.available() > 0)
  {
    String suhuString = Serial.readString();
    // testing = suhuString;
    // Serial.println("Input Serial : " + suhuString);
    // suhuAktual = suhuString.toFloat();
    // suhuSensor = suhuString.toFloat();
    // hm = suhuString.toInt();
    // suhu = suhuString.toFloat();

    // int str_len = suhuString.length() + 1;
    // idSensorChar[str_len];
    // suhuString.toCharArray(idSensorChar, str_len);
    // Serial.print("id : ");
    // Serial.println(idSensorChar);
    // preferences.begin("flock", false);
    // preferences.putString("id_sensor", suhuString);
    // delay(3000);
    // if (hm == 1)
    // {
    //   pemanas = "on";
    //   UpdateDevice(pemanas, flockId, topicHeaterPub);
    // }
    // else if (hm == 11)
    // {
    //   pemanas = "off";
    //   UpdateDevice(pemanas, flockId, topicHeaterPub);
    // }
    if (suhuString == "reset" || suhuString.toInt() == 16)
    {
      Serial.println("Reseting preference data...");
      preferences.begin("flock", false);

      ssid = preferences.putString("ssid", "");
      password = preferences.putString("password", "");
      flockId = preferences.putString("flock_id", "");
      idUser = preferences.putString("user_id", "");
      Serial.println("Preference data reseted...");
      delay(2000);
      ESP.restart();
    }

    if (suhuString.toInt() == 1)
    {
      preferences.begin("flock", false);
      preferences.putInt("heater_off", 0);
      preferences.putInt("heater_on", 0);
      preferences.putInt("cooler_off", 0);
      preferences.putInt("cooler_on", 0);
      preferences.putInt("fan1_off", 0);
      preferences.putInt("fan1_on", 0);
      preferences.putInt("fan2_off", 0);
      preferences.putInt("fan2_on", 0);
      preferences.putInt("fan3_off", 0);
      preferences.putInt("fan3_on", 0);
      preferences.putInt("fan4_off", 0);
      preferences.putInt("fan4_on", 0);
    }

    if(suhuString.toInt() == 3) {
      Alarm.timerRepeat(5, timerLampu1);
      Serial.println("set alarm");
    }
    if(suhuString.toInt() == 4) {
      Alarm.timerRepeat(10, timerLampu1);
       Serial.println("set alarm");
    }

    // if (suhuString.toInt() == 20)
    // {
    //   if (WiFi.status() == WL_CONNECTED)
    //   {
    //     TimeSync();
    //     delay(500);
    //     Serial.println("mengatur waktu ....");
    //     delay(1000);
    //   }
    // }
  }
  // ================= used for testing ================= //

  // ================= read dht sensor ================= //

  // ================= loop change state device ================= //
  if (blower1 == "on")
  {
    digitalWrite(pinBlower1, BLOWER1_ACTIVE);
  }
  else if (blower1 == "off")
  {
    digitalWrite(pinBlower1, !BLOWER1_ACTIVE);
  }
  if (blower2 == "on")
  {
    digitalWrite(pinBlower2, BLOWER2_ACTIVE);
  }
  else if (blower2 == "off")
  {
    digitalWrite(pinBlower2, !BLOWER2_ACTIVE);
  }
  if (blower3 == "on")
  {
    digitalWrite(pinBlower3, BLOWER3_ACTIVE);
  }
  else if (blower3 == "off")
  {
    digitalWrite(pinBlower3, !BLOWER3_ACTIVE);
  }
  // if (blower4 == "on")
  // {
  //   digitalWrite(pinBlower4, BLOWER4_ACTIVE);
  // }
  // else if (blower4 == "off")
  // {
  //   digitalWrite(pinBlower4, !BLOWER4_ACTIVE);
  // }
  if (pendingin == "on")
  {
    digitalWrite(pinPendingin, COOLER_ACTIVE);
  }
  else if (pendingin == "off")
  {
    digitalWrite(pinPendingin, !COOLER_ACTIVE);
  }
  if (pemanas == "on")
  {
    digitalWrite(pinPemanas, HEATER_ACTIVE);
  }
  else if (pemanas == "off")
  {
    digitalWrite(pinPemanas, !HEATER_ACTIVE);
  }
  if (lampu == "on")
  {
    digitalWrite(pinLampu, LAMPU);
  }
  else if (lampu == "off")
  {
    digitalWrite(pinLampu, !LAMPU);
  }
  // ================= convert inverter value ================= //
  pwm = map(inventer, 0, 100, 0, 200);
  frequency = map(inventer, 0, 100, 0, 50);
  ledcWrite(0, pwm);

  // printTime();
  // ShowTimeNTP();

  // ================= print to serial for debuging ================= //

  // ================ update flock ================ //
  if (WiFi.status() == WL_CONNECTED)
  {
    bool suhuAktualBool, hBool, calibrationTempBool, suhuBool, modeBool, hariBool, warningBool;
    suhuAktualBool = ValueChange(String(suhuAktual), String(suhuAktualB));
    hBool = ValueChange(String(h), String(hB));
    calibrationTempBool = ValueChange(String(calibrationTemp), String(calibrationTempB));
    suhuBool = ValueChange(String(suhu), String(suhuB));
    modeBool = ValueChange(String(mode), String(modeB));
    hariBool = ValueChange(String(hari), String(hariB));
    warningBool = ValueChange(String(warning), String(warningB));
    if (suhuAktualBool == true || hBool == true || calibrationTempBool == true || suhuBool == true || modeBool == true || hariBool == true || warningBool == true)
    {
      UpdateFlock();
      suhuAktualB = suhuAktual;
      hB = h;
      calibrationTempB = calibrationTemp;
      suhuB = suhu;
      modeB = mode;
      hariB = hari;
      warningB = warning;
    }
  }
  if (mode_release == "development")
  {
    // delay(1000);
    digitalClockDisplay();
    Alarm.delay(1000);
    // ================= print to serial for debuging ================= //
    // Serial.println("Blower1 : " + blower1);
    // Serial.println("Blower2 : " + blower2);
    // Serial.println("Blower3 : " + blower3);
    // Serial.println("Blower4 : " + blower4);
    // Serial.println("Pendingin : " + pendingin);
    // Serial.println("Pemanas : " + pemanas);
    // Serial.println("Inverter : " + String(inventer));
    // Serial.print("PWM : ");
    // Serial.println(pwm);
    // Serial.print("Frequency : ");
    // Serial.println(frequency);
    // Serial.print("intermittent : ");
    // Serial.print(intermittent);
    // Serial.println();

    // Serial.print("Status Wifi : ");
    // Serial.println(wifiStatus);

    // Serial.print("blower 1 Off : ");
    // Serial.print(deviceOff);
    // Serial.print(" - blower 1 On : ");
    // Serial.print(deviceOn);
    // Serial.print("- pendingin Off : ");
    // Serial.print(deviceOffCooler);
    // Serial.print(" - pendingin On : ");
    // Serial.print(deviceOnCooler);
    // Serial.print("- pemanas Off : ");
    // Serial.print(deviceOffHeater);
    // Serial.print(" - pemanas On : ");
    // Serial.print(deviceOnHeater);
    // Serial.println();

    // Serial.print("blower 2 Off : ");
    // Serial.print(deviceOffFan2);
    // Serial.print(" - blower 2 On : ");
    // Serial.print(deviceOnFan2);
    // Serial.print("- blower 3 Off : ");
    // Serial.print(deviceOffFan3);
    // Serial.print(" - blower 3 On : ");
    // Serial.print(deviceOnFan3);
    // Serial.print("- blower 4 Off : ");
    // Serial.print(deviceOffFan4);
    // Serial.print(" - blower 4 On : ");
    // Serial.print(deviceOnFan4);
    // Serial.println();

    // Serial.print("Suhu Ideal : ");
    // Serial.print(suhu);
    // Serial.print(" || Suhu batas bawah : ");
    // Serial.print(batasBawah);
    // Serial.print(" || Suhu batas atas : ");
    // Serial.println(batasAtas);
    // Serial.print("Suhu Aktual : ");
    // Serial.println(suhuAktual);
    // Serial.print("Hari ke : ");
    // Serial.println(hari);
    // Serial.println("==========================");
    // Serial.print("Mode Panel : ");
    // Serial.println(modePanel);
    // Serial.print("Mode Hardware : ");
    // Serial.println(mode);

    // Serial.print("Calibration Temp ");
    // Serial.println(calibrationTemp);
    // Serial.print("Range Temp ");
    // Serial.println(rangeTemp);
    // Serial.print("Suhu Sensor ");
    // Serial.println(suhuSensor);
  }
}
// end loop

// =================== custom task function =================== //
void TaskReconnectCode(void *parameter)
{
  Serial.print("Task reconnnect running on core ");
  Serial.println(xPortGetCoreID());
  while (1)
  {
    ReconnectWIFI();
    // delay(3000);
    ReconnectMQTT();
    delay(3000);
  }
}

void TaskShowMenuCode(void *pvParameters)
{
  Serial.print("Task Show Menu running on core ");
  Serial.println(xPortGetCoreID());

  while (1)
  {
    ShowMenu();
  }
}

void TaskAlarmCode(void *parameter)
{
  while (1)
  {
    client.publish(topicPingPub.c_str(), "connected");
    delay(10000);
  }
}
// =================== custom task function =================== //

// =================== custom function =================== //
void ReconnectMQTT()
{
  if (WiFi.status() == WL_CONNECTED)
  {
    while (!client.connected())
    {
      Serial.println("Connecting to MQTT...");

      if (client.connect(mqttID.c_str(), "chickin", "chickin"))
      {
        Serial.println("connected");
        delay(1000);
        client.subscribe(topicHeater.c_str());
        client.subscribe(topicCooling.c_str());
        client.subscribe(topicFan1.c_str());
        client.subscribe(topicFan2.c_str());
        client.subscribe(topicFan3.c_str());
        client.subscribe(topicFan4.c_str());
        client.subscribe(topicInverter.c_str());
        client.subscribe(topicIntermittent.c_str());
        client.subscribe(topicUpdate.c_str());
        UpdateFlock();
        UpdateDevice(blower1, flockId, topicFan1Pub);
        UpdateDevice(blower2, flockId, topicFan2Pub);
        UpdateDevice(blower3, flockId, topicFan3Pub);
        UpdateDevice(blower4, flockId, topicFan4Pub);
        UpdateDevice(pendingin, flockId, topicCoolingPub);
        UpdateDevice(pemanas, flockId, topicHeaterPub);
      }
      else
      {
        Serial.print("failed with state ");
        Serial.print(client.state());
        delay(2000);
      }
    }
  }
}

void ReconnectWIFI()
{
  if (ssid != "")
  {
    if (WiFi.status() != WL_CONNECTED)
    {
      WiFi.disconnect();
      WiFi.begin(ssid.c_str(), password.c_str());
      int cek = 0;
      while (WiFi.status() != WL_CONNECTED)
      {
        delay(1000);
        Serial.print(".");
        cek++;
        Serial.print("cek : " + String(cek));
        if (cek == 30)
        {
          break;
        }
      }
    }
  }
}

// void PutPreferences(String type, String name, String data)
// {
//   preferences.begin("flock", false);
//   if(type == "string") {
//     preferences.putString(name, data);
//   } else if(type == "int") {
//     preferences.putInt(name, data.toInt());
//   }
// }

void timerLampu1()
{
  Serial.println("alarm 1 jalan");
}

void timerLampu2()
{
  Serial.println("alarm 2 jalan");
}

void IncrementDay()
{
  Serial.println("Day added ");
  delay(6000);
  preferences.begin("flock", false);
  hari = preferences.getInt("day", 0);
  hari += 1;
  incrementHari = false;
  preferences.putInt("day", hari);
  UpdateFlock();
}

void digitalClockDisplay()
{
  // digital clock display of the time
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.println();
}

void printDigits(int digits)
{
  Serial.print(":");
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

void TimeSetting()
{
  Wire.beginTransmission(I2C_ADDRESS);
  Wire.write(byte(0));
  Wire.write(decToBcd(rtcSecond));   // second
  Wire.write(decToBcd(rtcMinute));   // minute
  Wire.write(decToBcd(rtcHour));     // hour
  Wire.write(decToBcd(rtcWeekday));  // weekday
  Wire.write(decToBcd(rtcMonthday)); // date
  Wire.write(decToBcd(rtcMonth));    // month
  Wire.write(decToBcd(rtcYear));     // year
  Wire.write(byte(0));
  Wire.endTransmission();
}
byte decToBcd(byte val)
{
  return ((val / 10 * 16) + (val % 10));
}
byte bcdToDec(byte val)
{
  return ((val / 16 * 10) + (val % 16));
}

void ShowTimeNTP()
{
  time_t now = time(nullptr);
  struct tm *p_timeinfo = localtime(&now);
  weekdayt = p_timeinfo->tm_wday;
  monthdayt = p_timeinfo->tm_mday;
  montht = p_timeinfo->tm_mon + 1;
  yeart = p_timeinfo->tm_year + 1900;
  hourt = p_timeinfo->tm_hour;
  minutet = p_timeinfo->tm_min;
  secondt = p_timeinfo->tm_sec;
  // Serial.print("Nilai aslie : ");
  // Serial.println(String(p_timeinfo->tm_year));
  // Serial.print("Nilai aslie tambah : ");
  // Serial.println(String(p_timeinfo->tm_year + 1900));
  char buffer[3];
  Serial.print(days[weekdayt]);
  Serial.print(" ");
  Serial.print(monthdayt);
  Serial.print(" ");
  Serial.print(months[montht - 1]);
  Serial.print(" ");
  Serial.print(String(p_timeinfo->tm_year + 1900));
  Serial.print(" ");
  Serial.print(hourt);
  Serial.print(":");
  sprintf(buffer, "%02d", minutet);
  Serial.print(buffer);
  Serial.print(":");
  // sprintf(buffer, "%02d", secondt);
  Serial.print(String(p_timeinfo->tm_sec));
  Serial.println();
}

void ShowTime()
{
  char buffer[3];
  TimeReader();
  Serial.print(days[weekdayt - 1]);
  Serial.print(" ");
  Serial.print(monthdayt);
  Serial.print(" ");
  Serial.print(months[montht - 1]);
  Serial.print(" 20");
  Serial.print(yeart);
  Serial.print(" ");
  Serial.print(hourt);
  Serial.print(":");
  sprintf(buffer, "%02d", minutet);
  Serial.print(buffer);
  Serial.print(":");
  sprintf(buffer, "%02d", secondt);
  Serial.print(buffer);
  Serial.println(AMPM);
}

void TimeReader()
{

  Wire.beginTransmission(I2C_ADDRESS);
  Wire.write(byte(0));
  Wire.endTransmission();
  Wire.requestFrom(I2C_ADDRESS, 7);
  secondt = bcdToDec(Wire.read());
  minutet = bcdToDec(Wire.read());
  hourt = bcdToDec(Wire.read());
  weekdayt = bcdToDec(Wire.read());
  monthdayt = bcdToDec(Wire.read());
  montht = bcdToDec(Wire.read());
  yeart = bcdToDec(Wire.read());
}

void UpdateFlock()
{
  StaticJsonDocument<512> doc;
  doc["actualTemperature"] = suhuAktual;
  doc["humidity"] = h;
  doc["calibrationTemperature"] = calibrationTemp;
  doc["idealTemperature"] = suhu;
  doc["mode"] = "manual";
  doc["day"] = hari;
  doc["connected"] = true;
  doc["wifiStatus"] = wifiStatusBool;
  doc["warning"] = warning;
  doc["_id"] = flockId;

  char out[512];
  int b = serializeJson(doc, out);
  Serial.print("bytes = ");
  Serial.println(b, DEC);
  Serial.println(out);
  boolean rc = client.publish(topicFlockPub.c_str(), out);
  Serial.println(rc);
}

bool ValueChange(String a, String b)
{
  if (a != b)
  {
    return true;
    a = b;
  }
  else
  {
    return false;
  }
}

boolean UpdateDevice(String status, String id, String topic)
{
  StaticJsonDocument<512> doc;
  doc["device_id"] = id;
  doc["status"] = status;

  char out[512];
  int b = serializeJson(doc, out);
  boolean rc = client.publish(topic.c_str(), status.c_str());
  return rc;
}

boolean UpdateIntermittent(String id, String topic)
{
  StaticJsonDocument<256> doc;

  doc["deviceOffFan1"] = deviceOff;
  doc["deviceOnFan1"] = deviceOn;
  doc["deviceOffCooler"] = deviceOffCooler;
  doc["deviceOnCooler"] = deviceOnCooler;
  doc["deviceOffHeater"] = deviceOffHeater;
  doc["deviceOnHeater"] = deviceOnHeater;

  char out[512];
  int b = serializeJson(doc, out);
  boolean rc = client.publish(topic.c_str(), out);
  return rc;
}

void Intermittent()
{
  millisTime = millis();
  intermittentOff = deviceOff * 60000;
  intermittentOn = deviceOn * 60000;
  if (blower1 == "off")
  {
    if (millisTime - previousMillis >= intermittentOff)
    {
      blower1 = "on";
      UpdateDevice(blower1, flockId, topicFan1Pub);
      previousMillis = millis();
    }
  }
  else if (blower1 == "on")
  {
    if (millisTime - previousMillis >= intermittentOn)
    {
      blower1 = "off";
      UpdateDevice(blower1, flockId, topicFan1Pub);
      previousMillis = millis();
    }
  }
}

void IntermittentCooler()
{
  millisTimeCooler = millis();
  intermittentOffCooler = deviceOffCooler * 60000;
  intermittentOnCooler = deviceOnCooler * 60000;
  if (pendingin == "off")
  {
    if (millisTimeCooler - previousMillisCooler >= intermittentOffCooler)
    {
      pendingin = "on";
      UpdateDevice(pendingin, flockId, topicCoolingPub);
      previousMillisCooler = millis();
    }
  }
  else if (pendingin == "on")
  {
    if (millisTimeCooler - previousMillisCooler >= intermittentOnCooler)
    {
      pendingin = "off";
      UpdateDevice(pendingin, flockId, topicCoolingPub);
      previousMillisCooler = millis();
    }
  }
}

void IntermittentHeater()
{
  millisTimeHeater = millis();
  intermittentOffHeater = deviceOffHeater * 60000;
  intermittentOnHeater = deviceOnHeater * 60000;
  if (pemanas == "off")
  {
    if (millisTimeHeater - previousMillisHeater >= intermittentOffHeater)
    {
      pemanas = "on";
      UpdateDevice(pemanas, flockId, topicHeaterPub);
      previousMillisHeater = millis();
    }
  }
  else if (pemanas == "on")
  {
    if (millisTimeHeater - previousMillisHeater >= intermittentOnHeater)
    {
      pemanas = "off";
      UpdateDevice(pemanas, flockId, topicHeaterPub);
      previousMillisHeater = millis();
    }
  }
}

void Sensor()
{
  if (recvData())
  {
    Serial.print("Incoming data from - ");
    Serial.println(wsn.id);
    String idSensorIN;
    String idSensorMCC;
    for (int i = 0; i <= 3; i++)
    {
      idSensorIN += wsn.id[i];
    }
    for (int i = 0; i <= 3; i++)
    {
      idSensorMCC += idSensorChar[i];
    }
    checkSensor = idSensorIN == idSensorMCC;
    Serial.print("in : ");
    Serial.println(checkSensor);
    if (checkSensor)
    {
      Serial.print("Incoming data t - ");
      Serial.println(wsn.t[0]);
      suhuSensor = wsn.t[0];
      h = wsn.h[0];
      // for (uint8_t i = 0; i < wsn.tcount + 1; i++)
      // {
      //   loop_case += wsn.t[i];
      //   Serial.print("t=");
      //   Serial.print(i);
      //   Serial.print(wsn.t[i]);
      //   Serial.print("& h");
      //   Serial.print(i);
      //   Serial.print("=");
      //   Serial.print(wsn.h[i]);
      //   Serial.println(",");
      //   if (i == wsn.tcount)
      //     suhuSensor = loop_case / wsn.tcount;
      // }
      Serial.print("Suhu - ");
      Serial.println(suhuSensor);
      checkSensor = false;
    }
  }
  if (!checkSensor)
  {
    millisSensor = millis();
    if (millisSensor - previousMillisSensor >= 60000 * 5)
    {
      suhuSensor = 0;
      suhuAktual = 0;
      UpdateFlock();
      previousMillisSensor = millis();
    }
  }
}

int recvData()
{
  if (radio.available() && radio.isChipConnected())
  {
    radio.read(&wsn, sizeof(wsn));
    return 1;
  }
  return 0;
}

void ShowMenu()
{
  int cekLCD;
  // Serial.print("Task LCD running on core ");
  // Serial.println(xPortGetCoreID());

  if (cekLCD > 25)
  {
    lcd.clear();
    cekLCD = 0;
  }
  cekLCD++;
  char key = keypad.getKey();
  if (key)
  { // if the key variable contains
    Serial.print("press: ");
    Serial.println(key); // output characters from Serial Monitor
    keyPress = key;
    if (key == 'A')
      Up = 1;
    if (key == 'B')
      Down = 1;
    if (key == 'C')
      Ok = 1;
    if (key == 'D')
      Cancel = 1;
  }
  if (Cancel == 0 && lockCancel == 0)
  {
    lcd.clear();
    lockCancel = 1;
  }
  // if (Cancel!=0 && lockCancel==1 && sub==0){lcd.clear(); lockCancel=0; dis=1;}
  if (Cancel != 0 && lockCancel == 1 && dis > 1 && sub > 0)
  {
    lcd.clear();
    lockCancel = 0;
    sub = 0;
  }
  if (Cancel != 0 && lockCancel == 1 && dis > 1 && sub == 0)
  {
    lcd.clear();
    lockCancel = 0;
    dis = 1;
    sub = 0;
  }

  // Serial.println("Dis : " + String(dis));
  // Serial.println("Sub : " + String(sub));
  if (dis == 1)
  {
    // tampilan awal// (maks string 6 character)
    lcd.setCursor(1, 0);
    lcd.print("Ctrl"); // menu 1
    lcd.setCursor(1, 1);
    lcd.print("Sett"); // menu 2
    lcd.setCursor(9, 0);
    lcd.print("Kall"); // menu 3
    lcd.setCursor(9, 1);
    lcd.print("Status"); // menu 4

    if (Down == 0 && lockDown == 0)
    {
      lockDown = 1;
    } // turunkan Cursor ">"
    if (Down != 0 && lockDown == 1)
    {
      lcd.clear();
      lockDown = 0;
      mode1++;
    }

    if (Up == 0 && lockUp == 0)
    {
      lockUp = 1;
    } // naikkan Cursor ">"
    if (Up != 0 && lockUp == 1)
    {
      lcd.clear();
      lockUp = 0;
      mode1--;
    }
    switch (mode1)
    {
    case 0:
      lcd.setCursor(0, 0);
      lcd.print(">");
      break;
    case 1:
      lcd.setCursor(0, 1);
      lcd.print(">");
      break;
    case 2:
      lcd.setCursor(8, 0);
      lcd.print(">");
      break;
    case 3:
      lcd.setCursor(8, 1);
      lcd.print(">");
      break;
    }
    if (Ok == 0 && lockOk == 0)
    {
      lockOk = 1;
      mode1 = 0;
      lcd.clear();
    }
    if (Ok != 0 && lockOk == 1 && mode1 == 0)
    {
      lockOk = 0;
      dis = 2;
      lcd.clear();
    } // pilih menu 1
    if (Ok != 0 && lockOk == 1 && mode1 == 1)
    {
      lockOk = 0;
      dis = 3;
      lcd.clear();
    } // pilih menu 2
    if (Ok != 0 && lockOk == 1 && mode1 == 2)
    {
      lockOk = 0;
      dis = 4;
      lcd.clear();
    } // pilih menu 3
    if (Ok != 0 && lockOk == 1 && mode1 == 3)
    {
      lockOk = 0;
      dis = 5;
      lcd.clear();
    } // pilih menu 4

    if (mode1 > 3)
      mode1 = 0;
    if (mode1 < 0)
      mode1 = 3;
  }
  // ================= menu controll ================= //
  if (dis == 2)
  {
    if (sub == 0)
    {
      lcd.setCursor(0, 0);
      lcd.print("C");
      lcd.setCursor(0, 1);
      lcd.print("t");
      lcd.setCursor(0, 2);
      lcd.print("r");
      lcd.setCursor(0, 3);
      lcd.print("l");
      lcd.setCursor(3, 0);
      lcd.print("Fan1");
      lcd.setCursor(3, 1);
      lcd.print("Fan2");
      lcd.setCursor(3, 2);
      lcd.print("Fan3");
      lcd.setCursor(3, 3);
      lcd.print("Fan4");
      lcd.setCursor(11, 0);
      lcd.print("Cool");
      lcd.setCursor(11, 1);
      lcd.print("Heater");
      lcd.setCursor(11, 2);
      lcd.print("Inverter");
      lcd.setCursor(11, 3);
      lcd.print("Intrmtr");
      if (Down == 0 && lockDown == 0)
      {
        lockDown = 1;
      } // turunkan Cursor ">"
      if (Down != 0 && lockDown == 1)
      {
        lcd.clear();
        lockDown = 0;
        mode1++;
      }

      if (Up == 0 && lockUp == 0)
      {
        lockUp = 1;
      } // naikkan Cursor ">"
      if (Up != 0 && lockUp == 1)
      {
        lcd.clear();
        lockUp = 0;
        mode1--;
      }

      if (Ok == 0 && lockOk == 0)
      {
        lockOk = 1;
        mode1 = 0;
        lcd.clear();
      }
      if (Ok != 0 && lockOk == 1 && mode1 == 0)
      {
        lockOk = 0;
        lcd.clear();
        sub = 1;
      } // Isi submenu fan 1
      if (Ok != 0 && lockOk == 1 && mode1 == 1)
      {
        lockOk = 0;
        lcd.clear();
        sub = 2;
      } // Isi submenu fan 2
      if (Ok != 0 && lockOk == 1 && mode1 == 2)
      {
        lockOk = 0;
        lcd.clear();
        sub = 3;
      } // Isi submenu fan 3
      if (Ok != 0 && lockOk == 1 && mode1 == 3)
      {
        lockOk = 0;
        lcd.clear();
        sub = 4;
      } // Isi submenu fan 4
      if (Ok != 0 && lockOk == 1 && mode1 == 4)
      {
        lockOk = 0;
        lcd.clear();
        sub = 5;
      } // Isi submenu cooling
      if (Ok != 0 && lockOk == 1 && mode1 == 5)
      {
        lockOk = 0;
        lcd.clear();
        sub = 6;
      } // Isi submenu heater
      if (Ok != 0 && lockOk == 1 && mode1 == 6)
      {
        lockOk = 0;
        lcd.clear();
        sub = 7;
      } // Isi submenu inverter
      if (Ok != 0 && lockOk == 1 && mode1 == 7)
      {
        lockOk = 0;
        lcd.clear();
        sub = 8;
      } // Isi submenu intermittent

      // if (mode1 > 6 || mode1 < 0)
      //   mode1 = 0;
      if (mode1 > 6)
        mode1 = 0;
      if (mode1 < 0)
        mode1 = 6;
      {
        switch (mode1)
        {
        case 0:
          lcd.setCursor(2, 0);
          lcd.print(">");
          break;
        case 1:
          lcd.setCursor(2, 1);
          lcd.print(">");
          break;
        case 2:
          lcd.setCursor(2, 2);
          lcd.print(">");
          break;
        case 3:
          lcd.setCursor(2, 3);
          lcd.print(">");
          break;
        case 4:
          lcd.setCursor(10, 0);
          lcd.print(">");
          break;
        case 5:
          lcd.setCursor(10, 1);
          lcd.print(">");
          break;
        case 6:
          lcd.setCursor(10, 2);
          lcd.print(">");
          break;
        }
      }
    }
  }
  // ================= menu setting ================= //
  if (dis == 3)
  {
    if (sub == 0)
    {
      lcd.setCursor(0, 0);
      lcd.print("S");
      lcd.setCursor(0, 1);
      lcd.print("e");
      lcd.setCursor(0, 2);
      lcd.print("t");
      lcd.setCursor(0, 3);
      lcd.print("t");
      lcd.setCursor(3, 0);
      lcd.print("WiFi");
      lcd.setCursor(3, 1);
      lcd.print("Mode");
      lcd.setCursor(3, 2);
      lcd.print("Sensor");
      lcd.setCursor(3, 3);
      lcd.print("Update");
      lcd.setCursor(13, 0);
      lcd.print("Day");
      lcd.setCursor(13, 1);
      lcd.print("Time");
      lcd.setCursor(13, 2);
      lcd.print("Rst Per");
      if (Down == 0 && lockDown == 0)
      {
        lockDown = 1;
      } // turunkan Cursor ">"
      if (Down != 0 && lockDown == 1)
      {
        lcd.clear();
        lockDown = 0;
        mode1++;
      }

      if (Up == 0 && lockUp == 0)
      {
        lockUp = 1;
      } // naikkan Cursor ">"
      if (Up != 0 && lockUp == 1)
      {
        lcd.clear();
        lockUp = 0;
        mode1--;
      }
      if (Ok == 0 && lockOk == 0)
      {
        lockOk = 1;
        mode1 = 0;
        lcd.clear();
      }
      if (Ok != 0 && lockOk == 1 && mode1 == 0)
      {
        lockOk = 0;
        lcd.clear();
        sub = 1;
      } // Isi submenu wifi
      if (Ok != 0 && lockOk == 1 && mode1 == 1)
      {
        lockOk = 0;
        lcd.clear();
        sub = 2;
      } // Isi submenu mode
      if (Ok != 0 && lockOk == 1 && mode1 == 2)
      {
        lockOk = 0;
        lcd.clear();
        sub = 3;
      } // Isi submenu suhu
      if (Ok != 0 && lockOk == 1 && mode1 == 3)
      {
        lockOk = 0;
        lcd.clear();
        sub = 4;
      } // Isi submenu update
      if (Ok != 0 && lockOk == 1 && mode1 == 4)
      {
        lockOk = 0;
        lcd.clear();
        sub = 5;
      } // Isi submenu day
      if (Ok != 0 && lockOk == 1 && mode1 == 5)
      {
        lockOk = 0;
        lcd.clear();
        sub = 6;
      } // Isi submenu time
      if (Ok != 0 && lockOk == 1 && mode1 == 6)
      {
        lockOk = 0;
        lcd.clear();
        sub = 7;
      } // Isi submenu reset periode / hari

      if (mode1 > 6)
        mode1 = 0;
      if (mode1 < 0)
        mode1 = 6;
      {
        switch (mode1)
        {
        case 0:
          lcd.setCursor(2, 0);
          lcd.print(">");
          break;
        case 1:
          lcd.setCursor(2, 1);
          lcd.print(">");
          break;
        case 2:
          lcd.setCursor(2, 2);
          lcd.print(">");
          break;
        case 3:
          lcd.setCursor(2, 3);
          lcd.print(">");
          break;
        case 4:
          lcd.setCursor(12, 0);
          lcd.print(">");
          break;
        case 5:
          lcd.setCursor(12, 1);
          lcd.print(">");
          break;
        case 6:
          lcd.setCursor(12, 2);
          lcd.print(">");
          break;
        }
      }
    }
  }
  // ================= menu calibration ================= //
  if (dis == 4)
  {
    if (sub == 0)
    {
      lcd.setCursor(0, 0);
      lcd.print("K");
      lcd.setCursor(0, 1);
      lcd.print("a");
      lcd.setCursor(0, 2);
      lcd.print("l");
      lcd.setCursor(0, 3);
      lcd.print("l");
      lcd.setCursor(3, 0);
      lcd.print("Suhu");
      if (Down == 0 && lockDown == 0)
      {
        lockDown = 1;
      } // turunkan Cursor ">"
      if (Down != 0 && lockDown == 1)
      {
        lcd.clear();
        lockDown = 0;
        mode1++;
      }

      if (Up == 0 && lockUp == 0)
      {
        lockUp = 1;
      } // naikkan Cursor ">"
      if (Up != 0 && lockUp == 1)
      {
        lcd.clear();
        lockUp = 0;
        mode1--;
      }
      if (Ok == 0 && lockOk == 0)
      {
        lockOk = 1;
        mode1 = 0;
        lcd.clear();
      }
      if (Ok != 0 && lockOk == 1 && mode1 == 0)
      {
        lockOk = 0;
        lcd.clear();
        sub = 1;
      } // Isi submenu 1

      if (mode1 > 1 || mode1 < 0)
        mode1 = 0;
      {
        switch (mode1)
        {
        case 0:
          lcd.setCursor(2, 0);
          lcd.print(">");
          break;
        }
      }
    }
  }
  // ================= menu status ================= //
  if (dis == 5)
  {
    if (sub == 0)
    {
      String statusPanel;
      if (modePanel == 1)
      {
        statusPanel = "Online";
      }
      else
      {
        statusPanel = "Offline";
      }
      lcd.setCursor(0, 0);
      lcd.print("S");
      lcd.setCursor(0, 1);
      lcd.print("t");
      lcd.setCursor(0, 2);
      lcd.print("t");
      lcd.setCursor(0, 3);
      lcd.print("s");
      lcd.setCursor(3, 0);
      lcd.print("WiFi");
      lcd.setCursor(9, 0);
      lcd.print(wifiStatus);
      lcd.setCursor(3, 1);
      lcd.print("Hum");
      lcd.setCursor(9, 1);
      lcd.print(h);
      lcd.setCursor(3, 2);
      lcd.print("Temp");
      lcd.setCursor(9, 2);
      lcd.print(suhuAktual);
      // lcd.setCursor(15, 2);
      // lcd.print(rangeTempStatus);
    }
  }

  // ================= controll menu content ================= //
  if (dis == 2 && sub == 1)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Fan 1");
    lcd.setCursor(10, 0);
    lcd.print("   ");
    lcd.setCursor(10, 0);
    lcd.print(blower1);
    lcd.setCursor(0, 1);
    lcd.print(" 1 ON");
    lcd.setCursor(0, 2);
    lcd.print(" 2 OFF");
    if (keyPress == '1')
    {
      blower1 = "on";
      client.publish(topicFan1Pub.c_str(), blower1.c_str());
      lcd.clear();
    }
    else if (keyPress == '2')
    {
      blower1 = "off";
      client.publish(topicFan1Pub.c_str(), blower1.c_str());
      lcd.clear();
    }
  }
  if (dis == 2 && sub == 2)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Fan 2");
    lcd.setCursor(10, 0);
    lcd.print("   ");
    lcd.setCursor(10, 0);
    lcd.print(blower2);
    lcd.setCursor(0, 1);
    lcd.print(" 1 ON");
    lcd.setCursor(0, 2);
    lcd.print(" 2 OFF");
    if (keyPress == '1')
    {
      blower2 = "on";
      UpdateDevice(blower2, flockId, topicFan2Pub);
      lcd.clear();
    }
    else if (keyPress == '2')
    {
      blower2 = "off";
      UpdateDevice(blower2, flockId, topicFan2Pub);
      lcd.clear();
    }
  }
  if (dis == 2 && sub == 3)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Fan 3");
    lcd.setCursor(10, 0);
    lcd.print("   ");
    lcd.setCursor(10, 0);
    lcd.print(blower3);
    lcd.setCursor(0, 1);
    lcd.print(" 1 ON");
    lcd.setCursor(0, 2);
    lcd.print(" 2 OFF");
    if (keyPress == '1')
    {
      blower3 = "on";
      UpdateDevice(blower3, flockId, topicFan3Pub);
      lcd.clear();
    }
    else if (keyPress == '2')
    {
      blower3 = "off";
      UpdateDevice(blower3, flockId, topicFan3Pub);
      lcd.clear();
    }
  }
  // menu set fan 4
  if (dis == 2 && sub == 4)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Fan 4");
    lcd.setCursor(10, 0);
    lcd.print("   ");
    lcd.setCursor(10, 0);
    lcd.print(blower4);
    lcd.setCursor(0, 1);
    lcd.print(" 1 ON");
    lcd.setCursor(0, 2);
    lcd.print(" 2 OFF");
    if (keyPress == '1')
    {
      blower4 = "on";
      UpdateDevice(blower4, flockId, topicFan4Pub);
      lcd.clear();
    }
    else if (keyPress == '2')
    {
      blower4 = "off";
      UpdateDevice(blower4, flockId, topicFan4Pub);
      lcd.clear();
    }
  }
  // menu set cooler
  if (dis == 2 && sub == 5)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Cooling ");
    lcd.setCursor(10, 0);
    lcd.print("   ");
    lcd.setCursor(10, 0);
    lcd.print(pendingin);
    lcd.setCursor(0, 1);
    lcd.print(" 1 ON");
    lcd.setCursor(0, 2);
    lcd.print(" 2 OFF");
    if (keyPress == '1')
    {
      pendingin = "on";
      UpdateDevice(pendingin, flockId, topicCoolingPub);
      lcd.clear();
    }
    else if (keyPress == '2')
    {
      pendingin = "off";
      UpdateDevice(pendingin, flockId, topicCoolingPub);
      lcd.clear();
    }
  }
  // menu set heater
  if (dis == 2 && sub == 6)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Heater ");
    lcd.setCursor(10, 0);
    lcd.print("   ");
    lcd.setCursor(10, 0);
    lcd.print(pemanas);
    lcd.setCursor(0, 1);
    lcd.print(" 1 ON");
    lcd.setCursor(0, 2);
    lcd.print(" 2 OFF");
    if (keyPress == '1')
    {
      pemanas = "on";
      UpdateDevice(pemanas, flockId, topicHeaterPub);
      lcd.clear();
    }
    else if (keyPress == '2')
    {
      pemanas = "off";
      UpdateDevice(pemanas, flockId, topicHeaterPub);
      lcd.clear();
    }
  }
  // menu set inverter
  if (dis == 2 && sub == 7)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Inverter");
    lcd.setCursor(10, 0);
    lcd.print("   ");
    lcd.setCursor(10, 0);
    lcd.print(inventer);
    // input = inventer;
    lcd.setCursor(1, 1);
    lcd.print(input);
    switch (keyPress)
    {
    case '1':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "1";
      lcd.print(input);
      break;
    case '2':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "2";
      lcd.print(input);
      break;
    case '3':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "3";
      lcd.print(input);
      break;
    case '4':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "4";
      lcd.print(input);
      break;
    case '5':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "5";
      lcd.print(input);
      break;
    case '6':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "6";
      lcd.print(input);
      break;
    case '7':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "7";
      lcd.print(input);
      break;
    case '8':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "8";
      lcd.print(input);
      break;
    case '9':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "9";
      lcd.print(input);
      break;
    case '0':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "0";
      lcd.print(input);
      break;
    case '#':
      inventer = input.toInt();
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Inverter");
      lcd.setCursor(10, 0);
      lcd.print(inventer);
      UpdateDevice(String(inventer), flockId, topicInverterPub);
      preferences.begin("flock", false);
      preferences.putInt("inverter", inventer);
      input = "";
      break;
    case '*':
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Inverter");
      lcd.setCursor(10, 0);
      lcd.print(inventer);
      input = "";
      break;
    default:
      break;
    }
  }
  // menu set intermittent
  if (dis == 2 && sub == 8)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Intermittent ");
    // lcd.setCursor(14, 0);
    // lcd.print(intermittent);
    lcd.setCursor(0, 1);
    lcd.print(" 1 Blower 1");
    lcd.setCursor(0, 2);
    lcd.print(" 2 Pendingin ");
    lcd.setCursor(0, 3);
    lcd.print(" 3 Pemanas");

    lcd.setCursor(14, 1);
    lcd.print(String(deviceOn) + "-" + String(deviceOff));
    lcd.setCursor(14, 2);
    lcd.print(String(deviceOnCooler) + "-" + String(deviceOffCooler));
    lcd.setCursor(14, 3);
    lcd.print(String(deviceOnHeater) + "-" + String(deviceOffHeater));

    if (keyPress == '1')
    {
      sub = 8;
    }
    else if (keyPress == '2')
    {
      sub = 11;
    }
    else if (keyPress == '3')
    {
      sub = 14;
    }
  }

  // ================= menu intermittent ================= //
  if (dis == 2) // menu intermittent / menu 5
  {
    if (sub == 8)
    {
      lcd.setCursor(0, 0);
      lcd.print("I");
      lcd.setCursor(0, 1);
      lcd.print("n");
      lcd.setCursor(0, 2);
      lcd.print("t");
      lcd.setCursor(0, 3);
      lcd.print("r");
      lcd.setCursor(3, 0);
      lcd.print("Waktu Nyala");
      lcd.setCursor(15, 0);
      lcd.print(deviceOn);
      lcd.setCursor(3, 1);
      lcd.print("Waktu Mati");
      lcd.setCursor(15, 1);
      lcd.print(deviceOff);

      if (Down == 0 && lockDown == 0)
      {
        lockDown = 1;
      } // turunkan Cursor ">"
      if (Down != 0 && lockDown == 1)
      {
        lcd.clear();
        lockDown = 0;
        mode1++;
      }

      if (Up == 0 && lockUp == 0)
      {
        lockUp = 1;
      } // naikkan Cursor ">"
      if (Up != 0 && lockUp == 1)
      {
        lcd.clear();
        lockUp = 0;
        mode1--;
      }
      if (Ok == 0 && lockOk == 0)
      {
        lockOk = 1;
        mode1 = 0;
        lcd.clear();
      }
      if (Ok != 0 && lockOk == 1 && mode1 == 0)
      {
        lockOk = 0;
        lcd.clear();
        sub = 9;
      } // Isi submenu 1
      if (Ok != 0 && lockOk == 1 && mode1 == 1)
      {
        lockOk = 0;
        lcd.clear();
        sub = 10;
      } // Isi submenu 2

      if (mode1 > 1 || mode1 < 0)
        mode1 = 0;
      {
        switch (mode1)
        {
        case 0:
          lcd.setCursor(2, 0);
          lcd.print(">");
          break;
        case 1:
          lcd.setCursor(2, 1);
          lcd.print(">");
          break;
        }
      }
    }
    else if (sub == 11)
    {
      lcd.setCursor(0, 0);
      lcd.print("I");
      lcd.setCursor(0, 1);
      lcd.print("n");
      lcd.setCursor(0, 2);
      lcd.print("t");
      lcd.setCursor(0, 3);
      lcd.print("r");
      lcd.setCursor(3, 0);
      lcd.print("Waktu Nyala");
      lcd.setCursor(15, 0);
      lcd.print(deviceOnCooler);
      lcd.setCursor(3, 1);
      lcd.print("Waktu Mati");
      lcd.setCursor(15, 1);
      lcd.print(deviceOffCooler);

      if (Down == 0 && lockDown == 0)
      {
        lockDown = 1;
      } // turunkan Cursor ">"
      if (Down != 0 && lockDown == 1)
      {
        lcd.clear();
        lockDown = 0;
        mode1++;
      }

      if (Up == 0 && lockUp == 0)
      {
        lockUp = 1;
      } // naikkan Cursor ">"
      if (Up != 0 && lockUp == 1)
      {
        lcd.clear();
        lockUp = 0;
        mode1--;
      }
      if (Ok == 0 && lockOk == 0)
      {
        lockOk = 1;
        mode1 = 0;
        lcd.clear();
      }
      if (Ok != 0 && lockOk == 1 && mode1 == 0)
      {
        lockOk = 0;
        lcd.clear();
        sub = 12;
      } // Isi submenu 1
      if (Ok != 0 && lockOk == 1 && mode1 == 1)
      {
        lockOk = 0;
        lcd.clear();
        sub = 13;
      } // Isi submenu 2

      if (mode1 > 1 || mode1 < 0)
        mode1 = 0;
      {
        switch (mode1)
        {
        case 0:
          lcd.setCursor(2, 0);
          lcd.print(">");
          break;
        case 1:
          lcd.setCursor(2, 1);
          lcd.print(">");
          break;
        }
      }
    }
    else if (sub == 14)
    {
      lcd.setCursor(0, 0);
      lcd.print("I");
      lcd.setCursor(0, 1);
      lcd.print("n");
      lcd.setCursor(0, 2);
      lcd.print("t");
      lcd.setCursor(0, 3);
      lcd.print("r");
      lcd.setCursor(3, 0);
      lcd.print("Waktu Nyala");
      lcd.setCursor(15, 0);
      lcd.print(deviceOnHeater);
      lcd.setCursor(3, 1);
      lcd.print("Waktu Mati");
      lcd.setCursor(15, 1);
      lcd.print(deviceOffHeater);

      if (Down == 0 && lockDown == 0)
      {
        lockDown = 1;
      } // turunkan Cursor ">"
      if (Down != 0 && lockDown == 1)
      {
        lcd.clear();
        lockDown = 0;
        mode1++;
      }

      if (Up == 0 && lockUp == 0)
      {
        lockUp = 1;
      } // naikkan Cursor ">"
      if (Up != 0 && lockUp == 1)
      {
        lcd.clear();
        lockUp = 0;
        mode1--;
      }
      if (Ok == 0 && lockOk == 0)
      {
        lockOk = 1;
        mode1 = 0;
        lcd.clear();
      }
      if (Ok != 0 && lockOk == 1 && mode1 == 0)
      {
        lockOk = 0;
        lcd.clear();
        sub = 15;
      } // Isi submenu 1
      if (Ok != 0 && lockOk == 1 && mode1 == 1)
      {
        lockOk = 0;
        lcd.clear();
        sub = 16;
      } // Isi submenu 2

      if (mode1 > 2 || mode1 < 0)
        mode1 = 0;
      {
        switch (mode1)
        {
        case 0:
          lcd.setCursor(2, 0);
          lcd.print(">");
          break;
        case 1:
          lcd.setCursor(2, 1);
          lcd.print(">");
          break;
        }
      }
    }
  }

  // menu set intermittent fan 1 nyala
  if (dis == 2 && sub == 9)
  {
    setPoint = 1;
    lcd.setCursor(0, 0);
    lcd.print(" Enter Value");
    lcd.setCursor(1, 1);
    switch (keyPress)
    {
    case '1':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "1";
      lcd.print(input);
      break;
    case '2':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "2";
      lcd.print(input);
      break;
    case '3':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "3";
      lcd.print(input);
      break;
    case '4':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "4";
      lcd.print(input);
      break;
    case '5':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "5";
      lcd.print(input);
      break;
    case '6':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "6";
      lcd.print(input);
      break;
    case '7':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "7";
      lcd.print(input);
      break;
    case '8':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "8";
      lcd.print(input);
      break;
    case '9':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "9";
      lcd.print(input);
      break;
    case '0':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "0";
      lcd.print(input);
      break;
    case '#':
      deviceOn = input.toFloat();
      input = "";
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Value");
      lcd.setCursor(1, 1);
      lcd.print(deviceOn);
      UpdateIntermittent(flockId, topicIntermittentPub);
      preferences.begin("flock", false);
      preferences.putInt("fan1_on", deviceOn);
      break;
    case '*':
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Enter Value");
      input = "0";
      break;
    default:
      break;
    }

    lcd.setCursor(5, 1);
    lcd.print("Menit");
  }

  // menu set intermittent fan 1 mati
  if (dis == 2 && sub == 10)
  {
    setPoint = 1;
    lcd.setCursor(0, 0);
    lcd.print(" Enter Value");
    lcd.setCursor(1, 1);
    switch (keyPress)
    {
    case '1':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "1";
      lcd.print(input);
      break;
    case '2':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "2";
      lcd.print(input);
      break;
    case '3':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "3";
      lcd.print(input);
      break;
    case '4':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "4";
      lcd.print(input);
      break;
    case '5':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "5";
      lcd.print(input);
      break;
    case '6':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "6";
      lcd.print(input);
      break;
    case '7':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "7";
      lcd.print(input);
      break;
    case '8':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "8";
      lcd.print(input);
      break;
    case '9':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "9";
      lcd.print(input);
      break;
    case '0':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "0";
      lcd.print(input);
      break;
    case '#':
      deviceOff = input.toFloat();
      input = "";
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Value");
      lcd.setCursor(1, 1);
      lcd.print(deviceOff);
      UpdateIntermittent(flockId, topicIntermittentPub);
      preferences.begin("flock", false);
      preferences.putInt("fan1_off", deviceOff);
      break;
    case '*':
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Enter Value");
      input = "0";
      break;
    default:
      break;
    }

    lcd.setCursor(5, 1);
    lcd.print("Menit");
  }

  // menu set intermittent pendingin nyala
  if (dis == 2 && sub == 12)
  {
    setPoint = 1;
    lcd.setCursor(0, 0);
    lcd.print(" Enter Value");
    lcd.setCursor(1, 1);
    switch (keyPress)
    {
    case '1':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "1";
      lcd.print(input);
      break;
    case '2':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "2";
      lcd.print(input);
      break;
    case '3':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "3";
      lcd.print(input);
      break;
    case '4':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "4";
      lcd.print(input);
      break;
    case '5':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "5";
      lcd.print(input);
      break;
    case '6':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "6";
      lcd.print(input);
      break;
    case '7':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "7";
      lcd.print(input);
      break;
    case '8':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "8";
      lcd.print(input);
      break;
    case '9':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "9";
      lcd.print(input);
      break;
    case '0':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "0";
      lcd.print(input);
      break;
    case '#':
      deviceOnCooler = input.toFloat();
      input = "";
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Value");
      lcd.setCursor(1, 1);
      lcd.print(deviceOnCooler);

      UpdateIntermittent(flockId, topicIntermittentPub);
      preferences.begin("flock", false);
      preferences.putInt("cooler_on", deviceOnCooler);
      break;
    case '*':
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Enter Value");
      input = "0";
      break;
    default:
      break;
    }

    lcd.setCursor(5, 1);
    lcd.print("Menit");
  }

  // menu set intermittent pendingin mati
  if (dis == 2 && sub == 13)
  {
    setPoint = 1;
    lcd.setCursor(0, 0);
    lcd.print(" Enter Value");
    lcd.setCursor(1, 1);
    switch (keyPress)
    {
    case '1':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "1";
      lcd.print(input);
      break;
    case '2':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "2";
      lcd.print(input);
      break;
    case '3':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "3";
      lcd.print(input);
      break;
    case '4':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "4";
      lcd.print(input);
      break;
    case '5':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "5";
      lcd.print(input);
      break;
    case '6':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "6";
      lcd.print(input);
      break;
    case '7':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "7";
      lcd.print(input);
      break;
    case '8':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "8";
      lcd.print(input);
      break;
    case '9':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "9";
      lcd.print(input);
      break;
    case '0':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "0";
      lcd.print(input);
      break;
    case '#':
      deviceOffCooler = input.toFloat();
      input = "";
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Value");
      lcd.setCursor(1, 1);
      lcd.print(deviceOffCooler);

      UpdateIntermittent(flockId, topicIntermittentPub);
      preferences.begin("flock", false);
      preferences.putInt("cooler_off", deviceOffCooler);
      break;
    case '*':
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Enter Value");
      input = "0";
      break;
    default:
      break;
    }

    lcd.setCursor(5, 1);
    lcd.print("Menit");
  }

  // menu set intermittent pendingin nyala
  if (dis == 2 && sub == 15)
  {
    setPoint = 1;
    lcd.setCursor(0, 0);
    lcd.print(" Enter Value");
    lcd.setCursor(1, 1);
    switch (keyPress)
    {
    case '1':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "1";
      lcd.print(input);
      break;
    case '2':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "2";
      lcd.print(input);
      break;
    case '3':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "3";
      lcd.print(input);
      break;
    case '4':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "4";
      lcd.print(input);
      break;
    case '5':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "5";
      lcd.print(input);
      break;
    case '6':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "6";
      lcd.print(input);
      break;
    case '7':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "7";
      lcd.print(input);
      break;
    case '8':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "8";
      lcd.print(input);
      break;
    case '9':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "9";
      lcd.print(input);
      break;
    case '0':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "0";
      lcd.print(input);
      break;
    case '#':
      deviceOnHeater = input.toFloat();
      input = "";
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Value");
      lcd.setCursor(1, 1);
      lcd.print(deviceOnHeater);

      UpdateIntermittent(flockId, topicIntermittentPub);
      preferences.begin("flock", false);
      preferences.putInt("heater_on", deviceOnHeater);
      break;
    case '*':
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Enter Value");
      input = "0";
      break;
    default:
      break;
    }

    lcd.setCursor(5, 1);
    lcd.print("Menit");
  }

  // menu set intermittent pendingin mati
  if (dis == 2 && sub == 16)
  {
    setPoint = 1;
    lcd.setCursor(0, 0);
    lcd.print(" Enter Value");
    lcd.setCursor(1, 1);
    switch (keyPress)
    {
    case '1':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "1";
      lcd.print(input);
      break;
    case '2':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "2";
      lcd.print(input);
      break;
    case '3':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "3";
      lcd.print(input);
      break;
    case '4':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "4";
      lcd.print(input);
      break;
    case '5':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "5";
      lcd.print(input);
      break;
    case '6':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "6";
      lcd.print(input);
      break;
    case '7':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "7";
      lcd.print(input);
      break;
    case '8':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "8";
      lcd.print(input);
      break;
    case '9':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "9";
      lcd.print(input);
      break;
    case '0':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "0";
      lcd.print(input);
      break;
    case '#':
      deviceOffHeater = input.toFloat();
      input = "";
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Value");
      lcd.setCursor(1, 1);
      lcd.print(deviceOffHeater);

      UpdateIntermittent(flockId, topicIntermittentPub);
      preferences.begin("flock", false);
      preferences.putInt("heater_off", deviceOffHeater);
      break;
    case '*':
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Enter Value");
      input = "0";
      break;
    default:
      break;
    }

    lcd.setCursor(5, 1);
    lcd.print("Menit");
  }

  // isi menu 2 | SETTING ////
  if (dis == 3 && sub == 1)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Setting WiFi");
    lcd.setCursor(0, 1);
    lcd.print(" 1 Reset WiFi");
    if (keyPress == '1')
    {
      lcd.setCursor(1, 2);
      lcd.print("Reset Wifi");
      delay(5000);
      preferences.putString("ssid", "");
      preferences.putString("password", "");
      preferences.putString("flock_id", "");
      preferences.putString("user_id", "");
      // delay(5000);
      ESP.restart();
    }
  }
  // setting mode
  if (dis == 3 && sub == 2)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Mode");
    lcd.setCursor(8, 0);
    lcd.print(mode);

    lcd.setCursor(0, 1);
    lcd.print(" 1 MCC");
    lcd.setCursor(0, 2);
    lcd.print(" 2 MANUAL");
    lcd.setCursor(9, 1);
    lcd.print(" 3 INVERTER");

    switch (keyPress)
    {
    case '1':
      mode = "mcc";
      break;
    case '2':
      mode = "manual";
      break;
    case '3':
      mode = "inverter";
      break;
    }
  }

  // menu setting sub sensor
  if (dis == 3 && sub == 3)
  {
    int panjang;
    setPoint = 1;
    lcd.setCursor(0, 0);
    lcd.print(" Sensor");
    lcd.setCursor(8, 0);
    lcd.print(statusSensor);

    lcd.setCursor(0, 1);
    lcd.print(" ID Sensor : ");
    lcd.setCursor(13, 1);
    lcd.print(String(idSensorChar));
    switch (keyPress)
    {
    case '1':
      lcd.clear();
      lcd.setCursor(13, 2);
      input = input + "1";
      lcd.print(input);
      break;
    case '2':
      lcd.clear();
      lcd.setCursor(13, 2);
      input = input + "2";
      lcd.print(input);
      break;
    case '3':
      lcd.clear();
      lcd.setCursor(13, 2);
      input = input + "3";
      lcd.print(input);
      break;
    case '4':
      lcd.clear();
      lcd.setCursor(13, 2);
      input = input + "4";
      lcd.print(input);
      break;
    case '5':
      lcd.clear();
      lcd.setCursor(13, 2);
      input = input + "5";
      lcd.print(input);
      break;
    case '6':
      lcd.clear();
      lcd.setCursor(13, 2);
      input = input + "6";
      lcd.print(input);
      break;
    case '7':
      lcd.clear();
      lcd.setCursor(13, 2);
      input = input + "7";
      lcd.print(input);
      break;
    case '8':
      lcd.clear();
      lcd.setCursor(13, 2);
      input = input + "8";
      lcd.print(input);
      break;
    case '9':
      lcd.clear();
      lcd.setCursor(13, 2);
      input = input + "9";
      lcd.print(input);
      break;
    case '0':
      lcd.clear();
      lcd.setCursor(13, 2);
      input = input + "0";
      lcd.print(input);
      break;
    case '#':
      idSensor = input;
      input = "";
      lcd.clear();
      lcd.setCursor(13, 1);
      lcd.print(idSensor);

      panjang = idSensor.length() + 1;
      idSensorChar[4];
      idSensor.toCharArray(idSensorChar, panjang);

      preferences.begin("flock", false);
      preferences.putString("id_sensor", idSensor);

      break;
    case '*':
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Enter Value");
      input = "";
      break;
    default:
      break;
    }
  }

  if (dis == 3 && sub == 4)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Update Firmware");
    lcd.setCursor(0, 1);
    lcd.print(" 1 Update");
    if (keyPress == '1')
    {
      lcd.setCursor(0, 3);
      lcd.print("Updating ...");
      FirmwareUpdateMCC();
    }
  }
  // menu setting sub day
  if (dis == 3 && sub == 5)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Day");
    lcd.setCursor(6, 0);
    lcd.print(hari);
    switch (keyPress)
    {
    case '1':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "1";
      lcd.print(input);
      break;
    case '2':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "2";
      lcd.print(input);
      break;
    case '3':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "3";
      lcd.print(input);
      break;
    case '4':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "4";
      lcd.print(input);
      break;
    case '5':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "5";
      lcd.print(input);
      break;
    case '6':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "6";
      lcd.print(input);
      break;
    case '7':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "7";
      lcd.print(input);
      break;
    case '8':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "8";
      lcd.print(input);
      break;
    case '9':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "9";
      lcd.print(input);
      break;
    case '0':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "0";
      lcd.print(input);
      break;
    case '#': // oke
      lcd.clear();
      hari = input.toInt();
      lcd.setCursor(0, 0);
      lcd.print(" Day");
      lcd.setCursor(6, 0);
      lcd.print(hari);
      preferences.begin("flock", false);
      preferences.putInt("day", hari);
      input = "";
      break;
    case '*': // clear
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Day");
      lcd.setCursor(6, 0);
      lcd.print(hari);
      input = "";
      break;
    default:
      break;
    }
  }

  // menu setting sub time
  if (dis == 3 && sub == 6)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Setting Time");
    lcd.setCursor(0, 1);
    String jam = String(hourt) + ":" + String(minutet) + ":" + String(secondt);
    Serial.println("Time = " + jam);
    lcd.print(jam);
    lcd.setCursor(0, 2);
    lcd.print(" 1 Auto Setting");
    lcd.setCursor(0, 3);
    lcd.print(" 2 Manual Setting");
    if (keyPress == '1')
    {
      if (WiFi.status() == WL_CONNECTED)
      {
        lcd.setCursor(0, 2);
        lcd.print(" Setting Time ...");
        delay(2000);
        TimeSync();
        delay(500);
        Serial.println("mengatur waktu ....");
        delay(1000);
      }
      else
      {
        lcd.setCursor(0, 2);
        lcd.print(" Setting Time ...");
        delay(2000);
        lcd.clear();
        lcd.setCursor(0, 2);
        lcd.print(" Setting Waktu gagal ");
        lcd.setCursor(0, 3);
        lcd.print(" Pastikan WiFi terhubung ");
        lcd.setCursor(0, 3);
        lcd.print(" Atau setting manual ");
        delay(2000);
      }
    }
    // else if(keyPress == '2') {
    //   sub1 = 1;
    // }
  }
  // menu setting sub time->manual setting
  //  if (dis == 3 && sub == 8)
  //  {
  //    lcd.clear();
  //    int posisi;
  //    if(posisi > 3 || posisi < 1) {
  //      posisi = 1;
  //    }
  //    Serial.print("Posisi ");
  //    Serial.println(posisi);
  //    if(keyPress == 'A') {
  //      posisi -= 1;
  //      statusTime = 1;
  //    } else if(keyPress == 'B') {
  //      posisi += 1;
  //      statusTime = 1;
  //    }

  //   switch (posisi) {
  //     //get value from keypad
  //       switch (keyPress)
  //       {
  //       case '1':
  //         lcd.clear();
  //         lcd.setCursor(1, 2);
  //         input = input + "1";
  //         lcd.print(input);
  //         break;
  //       case '2':
  //         lcd.clear();
  //         lcd.setCursor(1, 2);
  //         input = input + "2";
  //         lcd.print(input);
  //         break;
  //       case '3':
  //         lcd.clear();
  //         lcd.setCursor(1, 2);
  //         input = input + "3";
  //         lcd.print(input);
  //         break;
  //       case '4':
  //         lcd.clear();
  //         lcd.setCursor(1, 2);
  //         input = input + "4";
  //         lcd.print(input);
  //         break;
  //       case '5':
  //         lcd.clear();
  //         lcd.setCursor(1, 2);
  //         input = input + "5";
  //         lcd.print(input);
  //         break;
  //       case '6':
  //         lcd.clear();
  //         lcd.setCursor(1, 2);
  //         input = input + "6";
  //         lcd.print(input);
  //         break;
  //       case '7':
  //         lcd.clear();
  //         lcd.setCursor(1, 2);
  //         input = input + "7";
  //         lcd.print(input);
  //         break;
  //       case '8':
  //         lcd.clear();
  //         lcd.setCursor(1, 2);
  //         input = input + "8";
  //         lcd.print(input);
  //         break;
  //       case '9':
  //         lcd.clear();
  //         lcd.setCursor(1, 2);
  //         input = input + "9";
  //         lcd.print(input);
  //         break;
  //       case '0':
  //         lcd.clear();
  //         lcd.setCursor(1, 2);
  //         input = input + "0";
  //         lcd.print(input);
  //         break;
  //       case 'C':
  //         sub1 = 0;
  //         break;
  //       default:
  //         break;
  //       }
  //     case 1: //setting jam
  //       if(statusTime == 1) {
  //         input = String(rtcHour);
  //         statusTime = 0;
  //       }
  //   	  lcd.setCursor(1, 2);
  //       lcd.print(input);
  //       delay(100);
  //       lcd.setCursor(1, 2);
  //       lcd.print("  ");
  //       delay(100);
  //   	  lcd.setCursor(3, 2);
  //       lcd.print(":");
  //       lcd.setCursor(4, 2);
  //       lcd.print(rtcMinute);
  //       lcd.setCursor(6, 2);
  //       lcd.print(":");
  //       lcd.setCursor(7, 2);
  //       lcd.print(rtcSecond);
  //       rtcHour = input.toInt();
  //   	  break;
  //     case 2: //setting menit
  //       if(statusTime == 1) {
  //         input = rtcMinute;
  //         statusTime = 0;
  //       }
  //   	  lcd.setCursor(1, 1);
  //       lcd.print(rtcHour);
  //   	  lcd.setCursor(3, 1);
  //       lcd.print(":");
  //       lcd.setCursor(4, 1);
  //   	  lcd.print(input);
  //   	  delay(100);
  //       lcd.setCursor(4, 1);
  //       lcd.print("  ");
  //       delay(100);
  //       lcd.setCursor(5, 1);
  //       lcd.print(rtcSecond);
  //   	  break;
  //     case 3:
  //       if(statusTime == 1) {
  //         input = rtcSecond;
  //         statusTime = 0;
  //       }
  //   	  lcd.setCursor(1, 1);
  //       lcd.print(rtcHour);
  //   	  lcd.setCursor(3, 1);
  //       lcd.print(":");
  //       lcd.setCursor(4, 1);
  //   	  lcd.print(rtcMinute);
  //       lcd.setCursor(6, 1);
  //       lcd.print(":");
  //       lcd.setCursor(7, 1);
  //   	  lcd.print(input);
  //       delay(100);
  //       lcd.setCursor(7, 1);
  //       lcd.print("  ");
  //       delay(100);
  //   	  break;
  //     default:
  //   	  lcd.setCursor(1, 0);
  //       lcd.print("12");
  //   	  lcd.setCursor(3, 0);
  //       lcd.print(":");
  //       lcd.setCursor(4, 0);
  //       lcd.print("10");
  //   	  break;
  //   }
  // }

  // menu setting sub reset periode
  if (dis == 3 && sub == 7)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Reset Periode");
    lcd.setCursor(0, 1);
    lcd.print(" 1 Mulai Reset");
    if (keyPress == '1')
    {
      preferences.begin("flock", false);
      preferences.putInt("day", 0);
      hari = 0;
      delay(1000);
      lcd.clear();
    }
    lcd.setCursor(0, 1);
    lcd.print(" Finish Reset");
  }

  // isi menu 3 || KALL ////
  if (dis == 4 && sub == 1)
  {
    setPoint = 1;
    lcd.setCursor(0, 0);
    lcd.print(" Enter Temperature");
    lcd.setCursor(1, 1);
    // input = calibrationTemp;
    // lcd.print(input);
    switch (keyPress)
    {
    case '1':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "1";
      lcd.print(input);
      break;
    case '2':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "2";
      lcd.print(input);
      break;
    case '3':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "3";
      lcd.print(input);
      break;
    case '4':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "4";
      lcd.print(input);
      break;
    case '5':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "5";
      lcd.print(input);
      break;
    case '6':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "6";
      lcd.print(input);
      break;
    case '7':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "7";
      lcd.print(input);
      break;
    case '8':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "8";
      lcd.print(input);
      break;
    case '9':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "9";
      lcd.print(input);
      break;
    case '0':
      lcd.clear();
      lcd.setCursor(1, 1);
      input = input + "0";
      lcd.print(input);
      break;
    case '#':
      calibrationTemp = input.toFloat();
      input = "";
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Enter Temperature");
      lcd.setCursor(1, 1);
      lcd.print(calibrationTemp);

      preferences.begin("flock", false);
      preferences.putFloat("calibration_temp", calibrationTemp);

      break;
    case '*':
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print(" Enter Temperature");
      input = "0";
      break;
    default:
      break;
    }

    lcd.setCursor(5, 1);
    lcd.print("C");
  }

  // isi menu 4 || STATUS ////
  if (dis == 5 && sub == 1)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Menu 4 | Sub 1");
  }
  if (dis == 5 && sub == 2)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Menu 4 | Sub 2");
  }
  if (dis == 5 && sub == 3)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Menu 4 | Sub 3");
  }
  if (dis == 5 && sub == 4)
  {
    lcd.setCursor(0, 0);
    lcd.print(" Menu 4 | Sub 4");
  }
  Down = 0;
  Up = 0;
  Cancel = 0;
  Ok = 0;
  keyPress = '-';
  // delay(1);
}

// time sync from ntp
void TimeSync()
{
  SetTimeNtp(7);
  delay(200);
  time_t now = time(nullptr);
  struct tm *p_timeinfo = localtime(&now);
  rtcWeekday = p_timeinfo->tm_wday;
  rtcMonthday = p_timeinfo->tm_mday;
  rtcMonth = p_timeinfo->tm_mon + 1;
  int tahun = p_timeinfo->tm_year + 1900;
  String tahun1 = String(tahun);
  String tahun2 = String(tahun1.charAt(2)) + String(tahun1.charAt(3));
  rtcYear = tahun2.toInt();
  rtcHour = p_timeinfo->tm_hour;
  rtcMinute = p_timeinfo->tm_min;
  rtcSecond = p_timeinfo->tm_sec;
  delay(200);
  TimeSetting();
}

void SetTimeNtp(int timezone)
{
  Serial.print("[TIME] : Setting time using SNTP \n");
  configTime(timezone * 3600, 0, "id.pool.ntp.org");
  time_t now = time(nullptr);
  while (now < 1000)
  {
    delay(500);
    Serial.print(".");
    now = time(nullptr);
  }
  struct tm timeinfo;
  gmtime_r(&now, &timeinfo);
  Serial.printf("\n[TIME] : Current time: %s \n", asctime(&timeinfo));
}

void FirmwareUpdate()
{
  updateState = false;
  WiFiClient client;
  t_httpUpdate_return ret = httpUpdate.update(client, hostUpdate);

  switch (ret)
  {
  case HTTP_UPDATE_FAILED:
    Serial.printf("HTTP_UPDATE_FAILED Error (%d): %s\n", httpUpdate.getLastError(), httpUpdate.getLastErrorString().c_str());
    lcd.clear();
    lcd.setCursor(1, 0);
    lcd.print("Update Failed");
    delay(2000);
    break;

  case HTTP_UPDATE_NO_UPDATES:
    Serial.println("HTTP_UPDATE_NO_UPDATES");
    lcd.clear();
    lcd.setCursor(1, 0);
    lcd.print("No Update");
    delay(2000);
    break;

  case HTTP_UPDATE_OK:
    Serial.println("HTTP_UPDATE_OK");
    lcd.clear();
    lcd.setCursor(1, 0);
    lcd.print("Update Success");
    delay(2000);
    break;
  }
}

void FirmwareUpdateMCC()
{
  WiFiClient client;
  String hostUpdate = "http://dev.protim-tech.com/chickin/firmware_chickin.bin";
  t_httpUpdate_return ret = httpUpdate.update(client, hostUpdate);
  // Or:
  // t_httpUpdate_return ret = httpUpdate.update(client, "http://dev.protim-tech.com", 80, "/chickin/chickin_v4.15.bin");

  switch (ret)
  {
  case HTTP_UPDATE_FAILED:
    Serial.printf("HTTP_UPDATE_FAILED Error (%d): %s\n", httpUpdate.getLastError(), httpUpdate.getLastErrorString().c_str());
    break;

  case HTTP_UPDATE_NO_UPDATES:
    Serial.println("HTTP_UPDATE_NO_UPDATES");
    break;

  case HTTP_UPDATE_OK:
    Serial.println("HTTP_UPDATE_OK");
    break;
  }
}
