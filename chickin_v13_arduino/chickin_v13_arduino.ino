#include <SoftwareSerial.h>
#include <StringSplitter.h>

SoftwareSerial serialArduino(2, 3);

String dataESP[4];
//index 0 = relay1
//index 1 = relay2
//index 2 = relay3
//index 3 = relay4
//index 4 = relay5
String incomingByte;
int relay1, relay2, relay3, relay4, relay5;
//deklarasi pin relay
int pinRelay1 = 4;
int pinRelay2 = 5;
int pinRelay3 = 6;
int pinRelay4 = 7;
int pinRelay5 = 8;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  serialArduino.begin(115200);
  pinMode(pinRelay1, OUTPUT);
  pinMode(pinRelay2, OUTPUT);
  pinMode(pinRelay3, OUTPUT);
  pinMode(pinRelay4, OUTPUT);
  pinMode(pinRelay5, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  //  serialArduino.print("10.2");
  //  serialArduino.print(',');
  //  serialArduino.print("20.4");
  //  serialArduino.print(',');
  //  serialArduino.print("30");
  //  serialArduino.print("\n");
  //  Serial.println("SEND");
  ReadData();
  Serial.print("Relay 1: ");
  Serial.println(relay1);
  Serial.print("Relay 2: ");
  Serial.println(relay2);
  Serial.print("Relay 3: ");
  Serial.println(relay3);
  Serial.print("Relay 4: ");
  Serial.println(relay4);
  Serial.print("Relay 5: ");
  Serial.println(relay5);
  Serial.println();
  //set relay
  if (relay1 == 1) {
    digitalWrite(pinRelay1, HIGH);
  } else {
    digitalWrite(pinRelay1, LOW);
  }
  if (relay2 == 1) {
    digitalWrite(pinRelay2, HIGH);
  } else {
    digitalWrite(pinRelay2, LOW);
  }
  if (relay3 == 1) {
    digitalWrite(pinRelay3, HIGH);
  } else {
    digitalWrite(pinRelay3, LOW);
  }
  if (relay4 == 1) {
    digitalWrite(pinRelay4, HIGH);
  } else {
    digitalWrite(pinRelay4, LOW);
  }
  if (relay5 == 1) {
    digitalWrite(pinRelay5, HIGH);
  } else {
    digitalWrite(pinRelay5, LOW);
  }

  delay(1000);
}

void ReadData() {
  if (serialArduino.available() > 1) {
    incomingByte = serialArduino.readStringUntil('\n');
    StringSplitter *splitter = new StringSplitter(incomingByte, ',', 5);  // new StringSplitter(string_to_split, delimiter, limit)
    int itemCount = splitter->getItemCount();

    for (int i = 0; i < itemCount; i++) {
      //String item = splitter->getItemAtIndex(i);
      dataESP[i] = splitter->getItemAtIndex(i);
    }
    Serial.println(dataESP[0]);
    //cek status relay
    if (dataESP[0] == "1") {
      relay1 = 1;
    } else {
      relay1 = 0;
    }
    if (dataESP[1] == "1") {
      relay2 = 1;
    } else {
      relay2 = 0;
    }
    if (dataESP[2] == "1") {
      relay3 = 1;
    } else {
      relay3 = 0;
    }
    if (dataESP[3] == "1") {
      relay4 = 1;
    } else {
      relay4 = 0;
    }
    if (dataESP[4] == "1") {
      relay5 = 1;
    } else {
      relay5 = 0;
    }
  }
}
