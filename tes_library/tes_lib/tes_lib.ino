#include <my_lib.h>
#include <chickin_library.h>

String blower1 = "off", blower2 = "off";

void setup() {
    Serial.begin(115200);
    int result = add(4,5);
    Serial.println(result);
}

void loop() {
    String rBlower1 = intermittentChickin(5,3,blower1);
    blower1 = rBlower1;

    String rBlower2 = intermittentChickin(5,3,blower2);
    Serial.print("blower 1 : ");
    Serial.print(rBlower1);
    Serial.print(" - blower 2 : ");
    Serial.print(rBlower2);
    Serial.println();
    blower2 = rBlower2;

    delay(1000);
}